//Module to get iOS and Android push configuration from environment variables

function getPushConfig(){
  var config = {};
  var android = getAndroidPushConfig();
  if(android){
    config.android = android;
  }
  var ios = getIOSPushConfig()
  if(ios){
    config.ios = ios;
  }
  return config;
};

function getAndroidPushConfig(){
  if(process.env.ANDROID_GCM_SENDER_ID && process.env.ANDROID_GCM_API_KEY){
    return {
      senderId: process.env.ANDROID_GCM_SENDER_ID,
      apiKey: process.env.ANDROID_GCM_API_KEY
    }
  }
  return null;
}

function getIOSPushConfig(){
  if(process.env.IOS_P12_CERT_PATH && process.env.IOS_BUNDLEID){
    return {
      pfx: process.env.P12_CERT_PATH,
      bundleId: process.env.IOS_BUNDLEID,
      production: process.env.ENVIRONMENT_NAME === "production"
    }
  }
  return null;
}

module.exports = {
  getPushConfig: getPushConfig,
  getAndroidPushConfig: getAndroidPushConfig,
  getIOSPushConfig: getIOSPushConfig
}
