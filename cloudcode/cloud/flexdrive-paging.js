/**
* Flexdrive Paging
* Parse Cloud Module
*/

// PagingColumnInfo is information about column that will be used to sort the Paging objects.
// Should be [columnName, lastObjectValue].
// The lastObjectValue is optional.  If provided all objects before or after the value
// (depending on the ascending value) will be returned.  If lastObjectValue
// is not provided, paging will start with the first value.
function Paging(className, ascending, pageSize, pagingColumnInfo) {
  if (className == null || pageSize == null || pagingColumnInfo == null) {
    var error = new Error();
    error.name = "Paging error";
    error.message = "All properties are required - className: " + className +  ", pageSize: " + pageSize + ", pagingColumnInfo: " + pagingColumnInfo;
    throw(error);
  }

  if (pagingColumnInfo[0] == null) {
    var error = new Error();
    error.name = "Paging error";
    error.message = "The pagingColumnInfo must have a column name at index 0";
    throw(error);
  }

  this.className = className;
  this.ascending = (ascending === true) ? true : false;
  this.pageSize = pageSize;
  this.pagingColumnInfo = pagingColumnInfo;
}

Paging.prototype.query = function() {
  var columnName = this.pagingColumnInfo[0];
  var value = this.pagingColumnInfo[1];

  var query = new Parse.Query(this.className);

  if (this.ascending) {
    query.ascending(columnName);

    if (value) {
      query.greaterThan(columnName, value);
    }
  } else {
    query.descending(columnName);

    if (value) {
      query.lessThan(columnName, value);
    }
  }

  query.limit(this.pageSize);

  return query;
}

module.exports = Paging
