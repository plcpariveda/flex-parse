/**
* FlexDrive Notifications
* Parse Cloud Module
* 1.0.0
*/

// Imports:

var Data = require('./flexdrive-data.js');
var Utils = require('./flexdrive-utils.js');
var Error = require('./flexdrive-error.js');
var Paging = require('./flexdrive-paging.js');

var _ = require('underscore');

// Cloud Module Public API:

module.exports = {
  /**
  * Get the version of the module.
  * @return {String}
  */
  version: '1.0.0',

  V2: {
    retrieve: function(user, pagingLastNotificationCreatedAt, pageSize) {
      var lastCreatedAt = null;
      if (pagingLastNotificationCreatedAt) {
        lastCreatedAt = new Date(pagingLastNotificationCreatedAt);
      }

      var paging = new Paging("Notification", false, pageSize, ["createdDate", lastCreatedAt]);
      var query = paging.query();
      query.equalTo("user", user);
      query.select(["objectId", "alert", "type", "rentalId", "reservationId", "subject", "createdDate"]);

      return query.find().then(function(notifications) {
        try {
          var returnNotifications = [];
          var lastNotificationAccessDate = user.get("lastNotificationAccessDate");

          _.each(notifications, function(notification) {
            var newNotification = notification.toJSON();
            newNotification.updatedAt = undefined;

            addReadFieldToNotification(newNotification, lastNotificationAccessDate);

            var date = new Date(notification.get("createdDate"));
            var epochDate = date.getTime();
            newNotification.createdDate = epochDate.toString();
            newNotification.createdAt = undefined;

            returnNotifications.push(newNotification);
          });

          // Only update value if this is the initial call to get notifications.
          // Should not be updated if the user is paging through existing notifications.
          if (pagingLastNotificationCreatedAt == null) {
            user.set("lastNotificationAccessDate", new Date());

            return user.save().then(function() {
              return Parse.Promise.as(returnNotifications);
            });
          } else {
            return Parse.Promise.as(returnNotifications);
          }

        } catch (err) {
          var error = Error.systemError();
          Error.addObjectToError(error, err);
          return Parse.Promise.error(error);
        }
      });
    },
    delete: function(user, notificationId) {
      var query = new Parse.Query("Notification");
      query.equalTo("user", user);

      return query.get(notificationId, null).then(function(notification) {
        return notification.destroy();
      }).then(function() {
        return Parse.Promise.as();
      }, function(err) {
        var error = Error.objectNotFoundError();
        Error.addObjectToError(error, err);
        return Parse.Promise.error(error);
      });
    },
    getUnreadCount: function(user) {
      var query = new Parse.Query("Notification");
      query.descending("createdDate");
      query.equalTo("user", user);

      var lastAccessDate = user.get("lastNotificationAccessDate");
      if (lastAccessDate) {
        query.greaterThan('createdDate', lastAccessDate);
      }

      query.limit(1000);
      return query.count().then(function(count) {
        return Parse.Promise.as({ "count" : count });
      }, function(error) {
        var error = Error.systemError();
        Error.addObjectToError(error);
        return Parse.Promise.error(error);
      });
    }
  },
  Push: {
    sendSwapNotification: function(notification, options) {
      Parse.Cloud.useMasterKey();
      var promise = new Parse.Promise();
      var pushPayload = pushPayloadForSwapNotification(notification);
      sendPushToUser(notification.get(Data.Notifications.keys.user), pushPayload).then(function() {
        Utils.reportSuccess(pushPayload, options, promise);
      }, function(error) {
        Utils.reportFailure(error, options, promise);
      });
      return promise;
    },
    V2: {
      sendStandardPushNotification: function(notification, user) {
        Parse.Cloud.useMasterKey();

        var pushPayload = pushPayloadForStandardNotification(notification);
        return sendPushToUser(user, pushPayload);
      },
    }
  }
}

// Private API:

function addReadFieldToNotification(notification, lastNotificationAccessDate) {
  var createdAt = notification.createdAt;
  var createdAtDate = new Date(createdAt);

  if (lastNotificationAccessDate && createdAtDate && createdAtDate < lastNotificationAccessDate) {
    notification.read = true;
  } else {
    notification.read = false;
  }
}

function sendPushToUser(user, data, options) {
  return Parse.Push.send({where: pushQueryForUser(user), data: data});
}

function pushQueryForUser(user) {
  var channelId = 'user_' + user.id;
  var query = new Parse.Query(Parse.Installation);
  query.equalTo(Data.Installations.keys.channels, channelId);
  return query;
}

function pushPayloadForStandardNotification(notification) {
  var keys = Data.Notifications.keys;

  var notificationPayload = {}
  notificationPayload["alert"] = notification.get(keys.alert);
  notificationPayload["badge"] = "Increment";
  notificationPayload["type"] = notification.get(keys.type);
  notificationPayload["subject"] = notification.get(keys.subject);
  notificationPayload["objectId"] = notification.id
  notificationPayload["read"] = false
  notificationPayload["createdDate"] = notification.get("createdDate")

  var date = new Date(notification.createdAt);
  var epochDate = date.getTime();
  notificationPayload.createdDate = epochDate.toString();

  if (notification.get(keys.reservationId)) {
    notificationPayload.reservationId = notification.get(keys.reservationId)
  }

  if (notification.get(keys.rentalId)) {
    notificationPayload.rentalId = notification.get(keys.rentalId)
  }

  return notificationPayload;
}

function pushPayloadForSwapNotification(notification) {
  var k = Data.Notifications.keys;

  return {
    alert: notification.get(k.alert),
    badge: increment,
    type: notification.get(k.type),
    vehicle: notification.get(k.vehicle),
    token: notification.get(k.token),
    message: notification.get(k.message),
    confirmation: notification.get(k.confirmation),
    notificationIdentifier: notification.id
  }
}
