
var Billing = require('./flexdrive-billing.js');
var Environment = require('./flexdrive-environment.js');
var FlexDrive = require('./flexdrive.js');
var Utils = require('./flexdrive-utils.js');
var Error = require('./flexdrive-error.js');
var Log = require('./flexdrive-log.js');
var Notifications = require('./flexdrive-notifications.js');
var Mail = require('./flexdrive-email.js');
var Stripe = require('./flexdrive-stripe.js');
var Data = require('./flexdrive-data.js');

var RentCentric = require("./rentcentric.js");
var RentCentricEnvironment = Environment.RentCentric.getEnvironment();
RentCentric.initialize(RentCentricEnvironment.url, RentCentricEnvironment.clientID, RentCentricEnvironment.serviceUserName, RentCentricEnvironment.servicePassword);

var Slack = require('./slack.js');
var slack = new Slack('https://hooks.slack.com/services/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');
var _ = require('underscore');

var twelveMinutesMilliseconds = 720000;

//Wrap the job function like a Parse.Cloud.job function
//Requires using the master key
function jobHandler(jobName, job){
  return function(request, response){
    console.log("Job execution request: "+JSON.stringify(request));
    if (request.master !== true){
      response.success({
        data: null,
        error: Error.unauthorizedUserError()
      });
      return;
    }
    var status = {
      success: function(message){
        response.success({
          data: message || "done",
          error: null
        });
      },
      error: function(message){
        response.success({
          data: null,
          error: message || "error"
        });
      }
    }
    console.log("Running job "+jobName);
    job(request, status).then(function(){
      console.log(jobName+" finished");
    });
  }
}

module.exports = {
  "Majordomo": majordomo,
  "SayHighToSlackTeam": sayHiToSlackTeam,
  "PrintEnvironments": printEnvironments,
  "Update_2_3": update2_3
}

var jobs = module.exports;
console.log("Creating job endpoints");
//Define a cloud function for each job so it can be executed via http
for(var key in jobs){
  Parse.Cloud.define(key, jobHandler(key, jobs[key]));
  console.log("Created job /"+key);
}

//Parse.Cloud.job("Majordomo", majordomo);

function majordomo(request, status) {
  console.log("Starting job 'majordomo'");
  var start = new Date();
  var done = false;

  var JobStats = Parse.Object.extend("JobStats");
  var jobStats = new JobStats();
  jobStats.set('name','majordomo');
  jobStats.set('lastRunAt',start);
  return jobStats.save().always(function(jobStats) {
    return pingRentCentric();
  }).always(function() {
    var elapsedTime = Utils.elapsedTime(start,new Date());
    jobStats.set('afterPing',Utils.elapsedTime(start,new Date()).minutes);
    jobStats.set('elapsedTime',Utils.elapsedTime(start,new Date()).minutes);
    return jobStats.save();
  }).always(function(results) {
    return billing();
  }).always(function() {
    jobStats.set('afterBilling',Utils.elapsedTime(start,new Date()).minutes);
    jobStats.set('elapsedTime',Utils.elapsedTime(start,new Date()).minutes);
    return jobStats.save();
  }).then(function() {
    if (timeToGo(start)) {
      done = true;
      return Parse.Promise.error('afterBilling');
    }
    return billPending()
  }).always(function() {
    jobStats.set('afterBillPending',Utils.elapsedTime(start,new Date()).minutes);
    jobStats.set('elapsedTime',Utils.elapsedTime(start,new Date()).minutes);
    return jobStats.save();
  }).then(function() {
    if (timeToGo(start)) {
      done = true;
      return Parse.Promise.error('afterBillPending');
    }
    return prune();
  }).always(function() {
    jobStats.set('afterPrune',Utils.elapsedTime(start,new Date()).minutes);
    jobStats.set('elapsedTime',Utils.elapsedTime(start,new Date()).minutes);
    return jobStats.save();
  }).then(function() {
    if (timeToGo(start)) {
      done = true;
      return Parse.Promise.error('afterPrune');
    }
    return notifications();
  }).always(function() {
    jobStats.set('afterNotifications',Utils.elapsedTime(start,new Date()).minutes);
    jobStats.set('elapsedTime',Utils.elapsedTime(start,new Date()).minutes);
    return jobStats.save();
  }).then(function() {
    if (timeToGo(start)) {
      done = true;
      return Parse.Promise.error('afterNotifications');
    }
    return odrNotifications();
  }).always(function() {
    jobStats.set('afterOdrNotifications',Utils.elapsedTime(start,new Date()).minutes);
    jobStats.set('elapsedTime',Utils.elapsedTime(start,new Date()).minutes);
    return jobStats.save();
  }).then(function() {
    if (timeToGo(start)) {
      done = true;
      return Parse.Promise.error('afterOdrNotifications');
    }
    return stripeReporting(15 - Utils.elapsedTime(start,new Date()).minutes);
  }).always(function() {
    jobStats.set('afterStripeReporting',Utils.elapsedTime(start,new Date()).minutes);
    jobStats.set('elapsedTime',Utils.elapsedTime(start,new Date()).minutes);
    return jobStats.save();
  }).then(function() {
    if (timeToGo(start)) {
      done = true;
      return Parse.Promise.error('afterStripeReporting');
    }
    return clean();
  }).always(function() {
    jobStats.set('afterClean',Utils.elapsedTime(start,new Date()).minutes);
    jobStats.set('elapsedTime',Utils.elapsedTime(start,new Date()).minutes);
    return jobStats.save();
  }).then(function() {
    status.success("done");
  }, function(error) {
    jobStats.set('exiting',JSON.stringify(error));
    return jobStats.save().always(function() {
      status.success("done");
    });
  });
}


function prune() {
  Parse.Cloud.useMasterKey();
  var now = new Date();

  // Check if a never run or a new day
  var query = new Parse.Query('JobStats');
  query.equalTo('name', 'pruneDaily');
  query.descending("createdAt");
  return query.first().then(function(jobStats) {
    var run = false;
    if (jobStats) {
      if (jobStats.get('lastRunAt').getDate() != now.getDate()) {
        run = true;
      }
    } else {
      run = true;
    }

    if (run) {
      if (!jobStats) {
        var JobStats = Parse.Object.extend("JobStats");
        jobStats = new JobStats();
        jobStats.set('name','pruneDaily');
      }
      jobStats.set('lastRunAt',now);
      return jobStats.save().always(function() {
        return executeJob("PruneLog", FlexDrive.Logs.V2.prune, Error.systemError, "Prune Log Ran successfully", ['Log']);
      }).always(function() {
        return executeJob("PruneBillingStats", FlexDrive.Logs.V2.prune, Error.systemError, "Prune Billing Stats Ran successfully", ['BillingStats']);
      }).always(function() {
        return executeJob("PruneJobStats", FlexDrive.Logs.V2.prune, Error.systemError, "Prune Job Stats Ran successfully", ['JobStats']);
      });
    } else {
      return Parse.Promise.as();
    }
  });
}

function notifications() {
  // Check if waking hours EST
  var now = new Date();
  var nowEST = Utils.getDateForTimezone(now, Utils.easternTimezone);
  var startHour = 8;
  var endHour = 22;

  var query = new Parse.Query('Config');
  query.equalTo('key', 'notificationStartHour');
  return query.first().then(function(config) {
    if (config) {
      startHour = config.get('value');
    }
    query.equalTo('key', 'notificationEndHour');
    return query.first();
  }).then(function(config) {
    if (config) {
      endHour = config.get('value');
    }

    if (nowEST.getHours() >= startHour && nowEST.getHours() < endHour) {
      var query = new Parse.Query('JobStats');
      query.equalTo('name', 'notifications');
      return query.first().then(function(jobStats) {
        if (!jobStats) {
          var JobStats = Parse.Object.extend("JobStats");
          var jobStats = new JobStats();
          jobStats.set('name','notifications');
        }
        jobStats.set('lastRunAt',now);
        return jobStats.save();
      }).then(function() {
        return executeJob("NotifyUsersOfPayment", FlexDrive.Reservations.notifyUsersOfPayment, Error.systemError, "Successfully notified recipients");
      });
    } else {
      return Parse.Promise.as();
    }
  });
}

function odrNotifications() {
  // Get the market managers
  var query = new Parse.Query(Parse.User);
  query.equalTo('notifyODR', true);
  return query.find().then(function(managers) {
    // Send odr notification starting yesterday
    var now = new Date();
    var yesterday = new Date(now.getFullYear(), now.getMonth(), (now.getDate() - 1))
    var query = new Parse.Query('Billing');
    query.equalTo('odrNotify', true);
    query.greaterThanOrEqualTo('createdAt',yesterday)
    return query.find().then(function(billings) {
      var chainedPromise = Parse.Promise.as();
      _.each(billings, function(billing) {
        chainedPromise = chainedPromise.then(function() {
          var query = new Parse.Query(Parse.User);
          query.equalTo('customerIDRentCentric', billing.get('customerIDRentCentric'));
          return query.first().then(function(user) {
            var reservation = JSON.parse(billing.get('reservation'))
            return sendODRNotifcation(managers, user, reservation).then(function() {
              billing.set('odrNotify',false);
              return billing.save();
            }, function(error) {
              return Log.warn('Problem sending ODR notification email - ' + JSON.stringify(error))
            })
          })
        });
      });
      return chainedPromise;
    });
  });
}

function stripeReporting(minutesRemaining) {
  if (minutesRemaining < 2) {
    // Do nothing
    return Parse.Promise.as();
  }

  var query = new Parse.Query('StripeReport');
  query.equalTo('status', "Processing");
  return query.first().then(function(stripeReport) {
    if (!stripeReport) {
      var query = new Parse.Query('StripeReport');
      query.equalTo('status', "New");
      return query.first();
    } else {
      return Parse.Promise.as(stripeReport);
    }
  }).then(function(stripeReport) {
    if (!stripeReport) {
      // Nothing to do
      return Parse.Promise.as();
    } else {
      var now = new Date();
      if (stripeReport.get('status') == "New") {
        stripeReport.set('status', 'Processing')
        stripeReport.set('lineItems', [])
        stripeReport.set('error',false)
        stripeReport.set('billingProcessingDate', stripeReport.get('startDate'))
        stripeReport.set('stripeProcessingDate', stripeReport.get('startDate'))
        return stripeReport.save()
      } else if (stripeReport.get('status') == "Processing") {
        stripeReport.set('error',false)
        return stripeReport.save()
      } else {
        return Parse.Promise.as(stripeReport)
      }
    }
  }).then(function(stripeReport) {
    if (!stripeReport) {
      return Parse.Promise.as();
    }
    var now = new Date();
    return processStripeReport(stripeReport, minutesRemaining)
  }).then(function(stripeReport) {
    if (!stripeReport) {
      return Parse.Promise.as();
    }
    if (stripeReport.get('stripeProcessingDate') && stripeReport.get('stripeProcessingDate').getTime() > stripeReport.get('endDate').getTime()) {
      stripeReport.set('status', 'Finished')
      return stripeReport.save()
    } else {
      return Parse.Promise.as()
    }
  });
}

function billing() {
  Parse.Cloud.useMasterKey();
  console.log("Starting Billing")
  return executeJob("Billing", Billing.run, Error.billingSystemError, "Billing ran successfully");
};

function billPending() {
  Parse.Cloud.useMasterKey();
  console.log("Starting Bill Pending")
  return executeJob("BillPending", FlexDrive.Billings.pending, Error.billingSystemError, "Bill Pending ran successfully");
}

function clean() {
  Parse.Cloud.useMasterKey();
  console.log("Cleaning")
  return executeJob("Clean", FlexDrive.Users.V2.clean, Error.systemError, "Clean ran successfully");
};

function pingRentCentric() {
  return FlexDrive.pingRentCentric().then(function(buildNumber) {
    console.log("Successfully pinged Rent Centric");
    return Parse.Promise.as();
  }, function(error) {
    return slack.send({text: "RentCentric Down (" + Environment.getEnvName() + ")", channel: '#server-issues'}).always(function() {
      console.log(JSON.stringify(error));
      return Parse.Promise.as();
    });
  });
}

//Parse.Cloud.job("SayHiToSlackTeam", sayHiToSlackTeam);

function sayHiToSlackTeam(request, status) {
  slack.send({text: 'Hi Team! - From Parse Cloud (' + Environment.getEnvName() + ').'}).then(function(){
    // success
    status.success('');
  }, function(error){
    // error
    status.error('');
  });
}

//Parse.Cloud.job("PrintEnvironments",  printEnvironments);

function printEnvironments(request, status) {
  var RentCentricEnvironment = Environment.RentCentric.getEnvironment();
  console.log('<---- RentCentric Env:');
  console.log('clientID:' + RentCentricEnvironment.clientID);
  console.log('serviceUserName:' + RentCentricEnvironment.serviceUserName);
  console.log('servicePassword:' + RentCentricEnvironment.servicePassword);
  console.log('url:' + RentCentricEnvironment.url);
  console.log('----> RentCentric Env');

  console.log('<---- Mongo Env:');
  console.log(process.env.DATABASE_URI);
  console.log('----> Mongo Env');

  var StripeEnvironment = Environment.Stripe.getEnvironment();
  console.log('<---- Stripe Env:');
  console.log('secretKey:' + StripeEnvironment.secretKey);
  console.log('----> Stripe Env');

  var MailgunEnvironment = Environment.Mailgun.getEnvironment();
  console.log('<---- Mailgun Env:');
  console.log('domainName:' + MailgunEnvironment.domainName);
  console.log('apiKey:' + MailgunEnvironment.apiKey);
  console.log('flexDriveAdminEmailAddress:' + MailgunEnvironment.flexDriveAdminEmailAddress);
  console.log('----> Mailgun Env');

  console.log('<---- System Info:');
  var now = new Date();
  console.log('Current system time: ' + now.toString());
  console.log('Current system timezone offset: ' + now.getTimezoneOffset());
  console.log('Eastern time: ' + Utils.getDateTimezoneAsText(now, Utils.easternTimezone));
  console.log('Central time: ' + Utils.getDateTimezoneAsText(now, Utils.centralTimezone));
  console.log('----> System Info');

  status.success('');
}

//Parse.Cloud.job("Update_2_3", update2_3);

function update2_3(request, status) {
  console.log("Starting update for 2.3")
  Parse.Cloud.useMasterKey();

  var query = new Parse.Query('Config');
  query.equalTo('key', 'lockedOutMinutes');
  return query.first().then(function(config) {
    if (!config) {
      var Config = Parse.Object.extend("Config");
      config = new Config();
      config.set('key','lockedOutMinutes');
      config.set('value','5');
      console.log('Adding Config.lockedOutMinutes');
      return config.save();
    } else {
      config.set('key','lockedOutMinutes');
      config.set('value','5');
      console.log('Setting Config.lockedOutMinutes');
      return config.save();
    }
  }).then(function() {
    var query = new Parse.Query('Config');
    query.equalTo('key', 'lockedOutAttempts');
    return query.first().then(function(config) {
      if (!config) {
        var Config = Parse.Object.extend("Config");
        config = new Config();
        config.set('key','lockedOutAttempts');
        config.set('value','20');
        console.log('Adding Config.lockedOutAttempts');
        return config.save();
      } else {
        config.set('key','lockedOutAttempts');
        config.set('value','20');
        console.log('Setting Config.lockedOutAttempts');
        return config.save();
      }
    });
  }).then(function() {
    var query = new Parse.Query('Config');
    query.equalTo('key', 'lineItemColumns');
    return query.first().then(function(config) {
      if (!config) {
        var Config = Parse.Object.extend("Config");
        config = new Config();
        config.set('key','lineItemColumns');
        config.set('value','7');
        console.log('Adding Config.lineItemColumns');
        return config.save();
      } else {
        config.set('key','lineItemColumns');
        config.set('value','7');
        console.log('Setting Config.lineItemColumns');
        return config.save();
      }
    });
  }).then(function() {
    var query = new Parse.Query('Config');
    query.equalTo('key', 'refundColumns');
    return query.first().then(function(config) {
      if (!config) {
        var Config = Parse.Object.extend("Config");
        config = new Config();
        config.set('key','refundColumns');
        config.set('value','4');
        console.log('Adding Config.refundColumns');
        return config.save();
      } else {
        config.set('key','refundColumns');
        config.set('value','4');
        console.log('Setting Config.refundColumns');
        return config.save();
      }
    });
  }).then(function() {
    console.log('Clearing failed login attempts')
    var query = new Parse.Query(Parse.User);
    query.exists('failedLoginAttempts');
    return query.each(function(user) {
      var failedAttempts = user.get('failedLoginAttempts')
      if (failedAttempts && failedAttempts.length > 0) {
        user.set('failedLoginAttempts',[])
        return user.save();
      } else {
        return Parse.Promise.as();
      }
    })
  }).then(function() {
    console.log('Configuring gil.pratte')
    var query = new Parse.Query(Parse.User);
    query.equalTo('username', 'gil.pratte@mutualmobile.com');
    return query.first();
  }).then(function(user) {
    if (user) {
      user.set('allowBilling',true);
      user.set('allowODR',true);
      user.set('allowStripeReporting',true);
      user.set('managedMarket',["116","119","120","121"])
      user.set('notifyODR',true)
      return user.save();
    } else {
      var user = new Parse.User();
      user.set('username','gil.pratte@mutualmobile.com');
      user.set('email','gil.pratte@mutualmobile.com');
      if (Environment.isProductionEnv()) {
        user.set('password','dantlszz');
      } else {
        user.set('password','123456');
      }
      user.set('allowBilling',true);
      user.set('allowODR',true);
      user.set('allowStripeReporting',true);
      user.set('managedMarket',["116","119","120","121"])
      user.set('notifyODR',true)
      return user.save();
    }
  }).then(function() {
    console.log('Configuring chris.bittner')
    var query = new Parse.Query(Parse.User);
    query.equalTo('username', 'chris.bittner@mutualmobile.com');
    return query.first();
  }).then(function(user) {
    if (user) {
      user.set('allowBilling',true);
      user.set('allowODR',true);
      user.set('allowStripeReporting',true);
      user.set('managedMarket',["116","119","120","121"])
      user.set('notifyODR',true)
      return user.save();
    } else {
      var user = new Parse.User();
      user.set('username','chris.bittner@mutualmobile.com');
      user.set('email','chris.bittner@mutualmobile.com');
      if (Environment.isProductionEnv()) {
        user.set('password','gabrwfsd');
      } else {
        user.set('password','123456');
      }
      user.set('allowBilling',true);
      user.set('allowODR',true);
      user.set('allowStripeReporting',true);
      user.set('managedMarket',["116","119","120","121"])
      user.set('notifyODR',true)
      return user.save();
    }
  }).then(function() {
    console.log('Configuring lenny')
    var query = new Parse.Query(Parse.User);
    query.equalTo('username', 'lenny.patierno@flexdrive.com');
    return query.first().then(function(user) {
        if (user) {
          user.set('managedMarket',["116","119","120","121"])
          if (Environment.isProductionEnv()) {
            user.set('notifyODR',true)
          } else {
            user.set('notifyODR',false)
          }
          return user.save();
        } else {
          return Parse.Promise.as();
        }
    });
  }).then(function() {
    console.log('Configuring greg')
    var query = new Parse.Query(Parse.User);
    query.equalTo('username', 'greg.walker@flexdrive.com');
    return query.first().then(function(user) {
        if (user) {
          user.set('managedMarket',["116","119","120","121"])
          if (Environment.isProductionEnv()) {
            user.set('notifyODR',true)
          } else {
            user.set('notifyODR',false)
          }
          return user.save();
        } else {
          return Parse.Promise.as();
        }
    });
  }).then(function() {
    console.log('Configuring anthony')
    var query = new Parse.Query(Parse.User);
    query.equalTo('username', 'anthony.fleo@flexdrive.com');
    return query.first().then(function(user) {
        if (user) {
          user.set('managedMarket',["119"])
          if (Environment.isProductionEnv()) {
            user.set('notifyODR',true)
          } else {
            user.set('notifyODR',false)
          }
          return user.save();
        } else {
          return Parse.Promise.as();
        }
    });
  }).then(function() {
    console.log('Configuring ryan')
    var query = new Parse.Query(Parse.User);
    query.equalTo('username', 'ryan.beeler@flexdrive.com');
    return query.first().then(function(user) {
        if (user) {
          user.set('managedMarket',["119"])
          if (Environment.isProductionEnv()) {
            user.set('notifyODR',true)
          } else {
            user.set('notifyODR',false)
          }
          return user.save();
        } else {
          return Parse.Promise.as();
        }
    });
  }).then(function() {
    console.log('Configuring alec')
    var query = new Parse.Query(Parse.User);
    query.equalTo('username', 'alec.blume@flexdrive.com');
    return query.first().then(function(user) {
        if (user) {
          user.set('managedMarket',["121"])
          if (Environment.isProductionEnv()) {
            user.set('notifyODR',true)
          } else {
            user.set('notifyODR',false)
          }
          return user.save();
        } else {
          return Parse.Promise.as();
        }
    });
  }).then(function() {
    console.log('Configuring patrick')
    var query = new Parse.Query(Parse.User);
    query.equalTo('username', 'patrick.block@flexdrive.com');
    return query.first().then(function(user) {
        if (user) {
          user.set('managedMarket',["120"])
          if (Environment.isProductionEnv()) {
            user.set('notifyODR',true)
          } else {
            user.set('notifyODR',false)
          }
          return user.save();
        } else {
          return Parse.Promise.as();
        }
    });
  }).then(function() {
    console.log('Configuring pedro')
    var query = new Parse.Query(Parse.User);
    query.equalTo('username', 'pedro.santizo@flexdrive.com');
    return query.first().then(function(user) {
        if (user) {
          user.set('managedMarket',["116"])
          if (Environment.isProductionEnv()) {
            user.set('notifyODR',true)
          } else {
            user.set('notifyODR',false)
          }
          return user.save();
        } else {
          return Parse.Promise.as();
        }
    });
  }).then(function() {
    console.log('Configuring ernie')
    var query = new Parse.Query(Parse.User);
    query.equalTo('username', 'ernie.delarosa@coxautoinc.com');
    return query.first().then(function(user) {
        if (user) {
          user.set('allowBilling',true);
          user.set('allowStripeReporting',true)
          return user.save();
        } else {
          var user = new Parse.User();
          user.set('username','ernie.delarosa@coxautoinc.com');
          user.set('email','Ernie.DeLaRosa@coxautoinc.com');
          if (Environment.isProductionEnv()) {
            user.set('password','cuteixbe');
          } else {
            user.set('password','123456');
          }
          user.set('allowBilling',true);
          user.set('allowStripeReporting',true);
          return user.save();
        }
    });
  }).then(function() {
    console.log('Configuring daryl')
    var query = new Parse.Query(Parse.User);
    query.equalTo('username', 'daryl.sistrunk@autotrader.com');
    return query.first().then(function(user) {
        if (user) {
          user.set('allowStripeReporting',true)
          return user.save();
        } else {
          return Parse.Promise.as();
        }
    });
  }).then(function() {
    status.success('');
  }, function(error) {
    status.error(JSON.stringify(error));
  });
}

function executeJob(jobName, jobFunction, failureErrorFunction, successMessage, parameters) {
  try {
    return jobFunction.apply(null, parameters ? parameters : []).then(function() {
      console.log(successMessage);
      return Parse.Promise.as();
    }, function(error) {
      var jobError = Error.flexDriveErrorForError(error, failureErrorFunction);
      return Log.logFlexDriveError(jobError, null, null, jobName, null, null, null, null).always(function() {
        console.error("Job Failed: " + JSON.stringify(jobError));
        return Parse.Promise.as();
      });
    });
  } catch (error) {
    var jobError = Error.flexDriveErrorForError(error, Error.systemError);
    return Log.logFlexDriveError(jobError, null, null, jobName, null, null, null, null).then(function() {
      console.error("Job Failed: " + JSON.stringify(jobError));
      return Parse.Promise.as();
    });
  }
}

function timeToGo(start) {
  var now = new Date();
  var runTimeMillis = now.getTime() - start.getTime();
  if (runTimeMillis > twelveMinutesMilliseconds) {
    console.log('more than 12 minutes, time to go ');
    return true;
  }
  return false;
}

function sendODRNotifcation(managers, user, reservation) {
  var chainedPromise = Parse.Promise.as();
  _.each(managers, function(manager) {
    chainedPromise = chainedPromise.then(function() {
      var found = false;
      var managedMarkets = manager.get('managedMarket');
      for (var i = 0; i < managedMarkets.length; ++i) {
        if (reservation.LocationID == managedMarkets[i]) {
          found = true;
          break;
        }
      }

      if (found) {
        return Mail.sendReservationNotification(manager.get('email'), user, reservation).fail(function(error) {
          return Log.warn('Problem sending ODR notification email - ' + JSON.stringify(error))
        })
      } else {
        return Parse.Promise.as();
      }
    });
  });
  return chainedPromise;
}

function processStripeReport(stripeReport, minutesRemaining) {
  var started = new Date()

  var stripeProcessingDate = stripeReport.get('stripeProcessingDate');

  if (stripeProcessingDate.getTime() > stripeReport.get('endDate').getTime()) {
    // No more days to process
    return Parse.Promise.as(stripeReport)
  }

  var nextProcessingDate = new Date(stripeProcessingDate.getFullYear(), stripeProcessingDate.getMonth(), (stripeProcessingDate.getDate() + 1))

  return processStripePages(stripeReport, stripeProcessingDate, nextProcessingDate, null, minutesRemaining, started).then(function(result) {
    if (result === 'done') {
      // stripe pages done so set stripeProcessingDate to nextProcessingDate'
      stripeReport.set('stripeProcessingDate', nextProcessingDate)
      return stripeReport.save();
    } else {
      // stripe pages not done'
      return Parse.Promise.as(stripeReport);
    }
  });
}

function processStripePages(stripeReport, stripeProcessingDate, nextProcessingDate, lastObjId, minutesRemaining, started) {
  // See if there is time to process
  var elapsedTime = Utils.elapsedTime(started,new Date())
  if (elapsedTime.minutes >= minutesRemaining) {
    // no more processing
    return Parse.Promise.as("out of time");
  }

  var results;
  return Stripe.getChargesForDateRange(stripeProcessingDate, nextProcessingDate, lastObjId).then(function(stripeChargeResults) {
    results = stripeChargeResults;
    return processStripeEntries(stripeReport, stripeChargeResults.data, minutesRemaining, started)
  }).then(function(result) {
    console.log('After processing stripe entry page ' + JSON.stringify(result))

    if (result.errors > 0) {
      console.log('At least one error so have it process again');
      return Parse.Promise.as();
    }

    if (result.status === 'done') {
      if (results.has_more === true) {
        // recurse
        return processStripePages(stripeReport, stripeProcessingDate, nextProcessingDate, results.data[results.data.length - 1].id, minutesRemaining, started)
      } else {
        // no next page so done
        return Parse.Promise.as('done')
      }
    } else {
      // not done so must have bailed
      return Parse.Promise.as();
    }
  }, function(error) {
    return Log.warn(JSON.stringify(error)).always(function() {
      stripeReport.set('error',true)
      return stripeReport.save()
    })
  })
}

function processStripeEntries(stripeReport, charges, minutesRemaining, started) {
  var result = {status:'done', toProcess:charges.length, found:0, added:0, notPaid:0, error:0}
  var chainedPromise = Parse.Promise.as(result);
  _.each(charges, function(charge) {
    chainedPromise = chainedPromise.then(function() {
      // See if there is time to process
      var elapsedTime = Utils.elapsedTime(started,new Date())
      if (elapsedTime.minutes >= minutesRemaining) {
        // no more processing
        result.status = 'out of time'
        return Parse.Promise.as(result);
      }

      if (!charge.paid) {
        result.status = 'done'
        ++result.notPaid
        return Parse.Promise.as(result);
      }

      var created = new Date(charge.created * 1000);
      var csvCharge = mapChargeToCsv(charge);
      var csvInvoice;
      var csvRefunds;
      var csvBilling;
      var csvTransaction;
      var netAmount = {amount:charge.amount}
      var numLineItems = 1
      var numRefunds = 1

      return Utils.getConfigValue('lineItemColumns').then(function(lineItemColumns) {
        numLineItems = parseInt(lineItemColumns);
        return Utils.getConfigValue('refundColumns')
      }).then(function(refundColumns) {
        numRefunds = parseInt(refundColumns)
        csvRefunds = mapRefundsToCsv(charge, numRefunds)
        return Stripe.getTransaction(charge.balance_transaction)
      }).then(function(transaction) {
        csvTransaction = mapTransactionToCsv(transaction, netAmount)
        return Stripe.getInvoice(charge.invoice)
      }).then(function(invoice) {
        csvInvoice = mapInvoiceToCsv(invoice, numLineItems)
        if (invoice) {
          var query = new Parse.Query('Billing');
          query.equalTo('stripeInvoiceId', invoice.id);
          return query.first()
        } else {
          return Parse.Promise.as()
        }
      }).then(function(billing) {
        csvBilling = mapBillingToCsv(billing)

        // Add the line csv to the line items
        var item = csvCharge + ',' + csvTransaction + ',' + netAmount.amount + ',' + csvInvoice + ',' + csvRefunds + ',' + csvBilling;
        return addLineItem(stripeReport, item, charge.id, result)
      }, function(error) {
        return Log.warn(JSON.stringify(error)).always(function() {
          result.status = 'done'
          ++result.error
          stripeReport.set('error', true)
          return stripeReport.save().then(function() {
            return Parse.Promise.as(result);
          })
        })
      });
    });
  });
  return chainedPromise;
}

function mapChargeToCsv(charge) {
  var createdDate = new Date(charge.created * 1000)
  return (createdDate.getMonth() + 1) + '/' + createdDate.getDate() + '/' + createdDate.getFullYear() + ',' + charge.customer + ',' + charge.receipt_email + ',' + charge.id + ',' + (charge.description == null ? '' : charge.description) + ',' + (charge.statement_descriptor == null ? '' : charge.statement_descriptor) + ',' + charge.amount + ',' + charge.amount_refunded
}

function mapInvoiceToCsv(invoice, numColumns) {
  var csv = ''
  if (invoice) {
    var rate;
    var taxes = []
    var other = []
    for (var i = 0; i < invoice.lines.data.length; ++i) {
      // Trim description
      if (invoice.lines.data[i].description) {
        invoice.lines.data[i].description = invoice.lines.data[i].description.trim();
      }
      // Check for Rate and Tax
      if (invoice.lines.data[i].description && invoice.lines.data[i].description.indexOf('Rate') !== -1) {
        rate = invoice.lines.data[i]
      } else if (invoice.lines.data[i].description && invoice.lines.data[i].description.indexOf('Tax') !== -1) {
        taxes.push(invoice.lines.data[i])
      } else {
        other.push(invoice.lines.data[i])
      }
    }

    var swapped;
    do {
      swapped = false;
      for (var i=0; i < taxes.length-1; i++) {
        if (taxes[i].description && taxes[i+1].description && taxes[i].description.charAt(0) > taxes[i+1].description.charAt(0)) {
          var temp = taxes[i];
          taxes[i] = taxes[i+1];
          taxes[i+1] = temp;
          swapped = true;
        }
      }
    } while (swapped);

    csv = invoice.id

    if (rate) {
      csv += ',' + rate.amount
    } else {
      csv += ','
    }

    var count = 0
    for (var i = 0; i < taxes.length; ++i) {
      if (taxes[i].description) {
        csv += ',' + taxes[i].description
      } else {
        csv += ','
      }
      ++count
      csv += ',' + taxes[i].amount
      ++count
    }

    for (var i = count; i < 8; ++i) {
      csv += ','
      ++count
    }

    for (var i = 0; i < other.length; ++i) {
      if (other[i].description) {
        csv += ',' + other[i].description
      } else {
        csv += ','
      }
      ++count
      csv += ',' + other[i].amount
      ++count
    }

    for (var i = count; i < numColumns * 2; ++i) {
      csv += ','
    }
  } else {
    csv += ','
    for (var i = 0; i < numColumns * 2; ++i) {
      csv += ','
    }
  }
  return csv
}

function mapRefundsToCsv(charge, numColumns) {
  var csv = ''
  var addComma = false;
  for (var i = 0; i < charge.refunds.data.length; ++i) {
    if (addComma) {
      csv += ',' + charge.refunds.data[i].amount
    } else {
      csv += charge.refunds.data[i].amount
      addComma = true
    }
  }
  for (var i = charge.refunds.data.length; i < numColumns; ++i) {
    if (addComma) {
      csv += ','
    } else {
      addComma = true
    }
  }
  return csv
}

function mapBillingToCsv(billing) {
  if (billing) {
    return billing.get('reservationId') + ',' + billing.get('customerIDRentCentric') + ',' + Data.Markets.marketNameForID(billing.get('locationId'))
  } else {
    return ',,'
  }
}

function mapTransactionToCsv(transaction, netAmount) {
  if (transaction) {
    netAmount.amount = netAmount.amount - transaction.fee
    return transaction.fee
  } else {
    return ''
  }
}

function addLineItem(stripeReport, item, id, result) {
  var lineItems = stripeReport.get('lineItems')
  var found = false
  for (var i = 0; i < lineItems.length; ++i) {
    if (lineItems[i].indexOf(id) !== -1) {
      found = true;
      break;
    }
  }

  if (found) {
    result.status = 'done'
    ++result.found
    return Parse.Promise.as(result);
  } else {
    lineItems.push(item)
    stripeReport.set('lineItems',lineItems)
    return stripeReport.save().then(function() {
      result.status = 'done'
      ++result.added
      return Parse.Promise.as(result);
    }, function(error) {
      return Log.warn('Problem saving stripe report ' + JSON.stringify(error)).always(function() {
        result.status = 'done'
        ++result.error
        stripeReport.set('error', true)
        return stripeReport.save().then(function() {
          return Parse.Promise.as(result);
        })
      })
    })
  }
}
