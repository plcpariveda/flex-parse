/**
* Rent Centric
* Parse Cloud Module
* 1.0.1
*/

var _ = require('underscore');
var Environment = require('./flexdrive-environment.js');
var Error = require('./flexdrive-error.js');
var Log = require('./flexdrive-log.js');
var Utils = require('./flexdrive-utils.js');

var _url = '';
var _clientID = '';
var _serviceUserName = '';
var _sericePassword = '';
var _headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json'
};
var _url = "https://www.stripe.com"

// RentCentric Customer Properties
// TODO: Document how this map is used.
var customerKeys  = {
  email: 'Email',
  licenseValidated: 'LicenseValidated',
  accountHolder: 'AccountHolder',
  address: 'Address',
  city: 'City',
  stateCode: 'StateCode',
  zip: 'Zip',
  countryCode: 'CountryCode',
  birthday: 'Birthday',
  firstName: 'FirstName',
  middleName: 'MiddleName',
  lastName: 'LastName',
  gender: 'Gender',
  licenseNumber: 'LicenseNumber',
  licenseState: 'LicenseState',
  licenseExpirationDate: 'LicenseExpiry',
  licenseValidated: 'LicenseValidated',
  phone: 'Phone',
};

var locationKeys = {
  locationId: 'LocationID'
};

var RESERVATION_ALREADY_PAID = 11;

// Cloud Module Public API:

module.exports = {
  /**
  * Get the version of the module.
  * @return {String}
  */
  version: '1.0.1',

  /**
  * Initialize the Rent Centric module with the proper credentials.
  * @param {String} url address of the Rent Centric web services
  * @param {String} clientID Your Rent Centric client ID
  * @param {String} serviceUserName Your Rent Centric service user name
  * @param {String} servicePassword Your Rent Centric service password
  */
  initialize: function(url, clientID, serviceUserName, servicePassword) {
    _url = url;
    _clientID = clientID;
    _serviceUserName = serviceUserName;
    _servicePassword = servicePassword;
    return this;
  },

  /**
  * RentCentric Customer property key map.
  * Used by clients to set values before creating customers.
  */
  customerKeys : function() {
    var keyMap = {};
    var keys = _.keys(customerKeys);
    _.each(keys, function(key) {
      keyMap[key] = key;
    });
    return keyMap;
  },

  getVersionNumber: function() {
    var body = {
      ClientID: _clientID,
      ServiceUserName: _serviceUserName,
      ServicePassword: _servicePassword
    };

    return callRentcentricWithPath('/GetVersionNumber', body, true, null).then(function(httpResponse) {
      return Parse.Promise.as(httpResponse.data.BuildNumber);
    });
  },

  Customers: {
    keys: {
      licenseValidated: 'LicenseValidated',
      customerId: 'CustomerID',
      firstName: customerKeys.firstName,
      lastName: customerKeys.lastName,
      email: 'Email',
      locationId: 'LocationID'
    },
    /**
    * Creates a new customer.
    */
    create: function(locationId, data) {
      var customerData = customerDataWithData(data);
      var body = {
        ClientID: _clientID,
        ServiceUserName: _serviceUserName,
        ServicePassword: _servicePassword,
        CustomerData: customerData,
        LocationID: locationId
      };

      return callRentcentricWithPath('/AddCustomer', body, false, null).then(function(httpResponse) {
        return Parse.Promise.as(httpResponse.data.CustomerID);
      });
    },

    /**
    * Retrieves the details of an existing customer.
    */
    retrieve: function(customerId) {
      var body = {
        ClientID: _clientID,
        ServiceUserName: _serviceUserName,
        ServicePassword: _servicePassword,
        CustomerID: customerId
      };

      return callRentcentricWithPath('/GetCustomer', body, false, null).then(function(httpResponse) {
        return Parse.Promise.as(httpResponse.data.CustomerInfo);
      });
    },

    /**
    * Updates the specified customer.
    */
    update: function(customerId, data) {
      var body = {
        ClientID: _clientID,
        ServiceUserName: _serviceUserName,
        ServicePassword: _servicePassword,
        CustomerID: customerId,
        CustomerData: data
      };

      return callRentcentricWithPath('/UpdateCustomer', body, false, null).then(function(httpResponse) {
        return Parse.Promise.as(httpResponse.data.CustomerID);
      });
    },

    // TODO: In future release, build generic version that can take any number of attributes to update.
    updateEmail: function(customerId, email) {
      var self = this
      return self.retrieve(customerId).then(function(customer) {
        customer[self.keys.email] = email;
        return self.update(customerId, customer);
      }).then(function() {
        return Parse.Promise.as();
      }, function(error) {
        // No need to log the error here, because the retrieve and update functions will log errors if there is a failure.
        return Parse.promise.error(error);
      });
    },

    V2: {
      update: function(customer) {
        var body = {
          ClientID: _clientID,
          ServiceUserName: _serviceUserName,
          ServicePassword: _servicePassword,
          CustomerID: customer.CustomerID,
          CustomerData: customer
        };

        return callRentcentricWithPath('/UpdateCustomer', body, true, null).then(function(httpResponse) {
          return Parse.Promise.as(httpResponse.data);
        });
      },
      retrieveConglomerateInfo: function(customerId) {
        var body = {
          ClientID: _clientID,
          ServiceUserName: _serviceUserName,
          ServicePassword: _servicePassword,
          CustomerID: customerId
        };

        var didNotSucceedCallBack = function(httpResponse, path, body, isV2) {
          if (httpResponse && httpResponse.data && httpResponse.data.StatusInfo && httpResponse.data.StatusInfo.StatusCode && httpResponse.data.StatusInfo.StatusCode == "102") {
            return Parse.Promise.error(Error.customerNotFoundError());
          } else {
            return logRentCentricErrorForHTTPResponse(httpResponse, path, body, isV2);
          }
        };

        return callRentcentricWithPath('/GetCustomerRunningInfo', body, true, didNotSucceedCallBack).then(function(httpResponse) {
          return Parse.Promise.as(httpResponse.data);
        });
      },
      retrieve: function(customerId) {
        var body = {
          ClientID: _clientID,
          ServiceUserName: _serviceUserName,
          ServicePassword: _servicePassword,
          CustomerID: customerId
        };

        var didNotSucceedCallBack = function(httpResponse, path, body, isV2) {
          if (httpResponse && httpResponse.data && httpResponse.data.StatusInfo && httpResponse.data.StatusInfo.StatusCode && httpResponse.data.StatusInfo.StatusCode == "102") {
            return Parse.Promise.error(Error.customerNotFoundError());
          } else {
            return logRentCentricErrorForHTTPResponse(httpResponse, path, body, isV2);
          }
        };

        return callRentcentricWithPath('/GetCustomer', body, true, didNotSucceedCallBack).then(function(httpResponse) {
          return Parse.Promise.as(httpResponse.data.CustomerInfo);
        });
      },
      create: function(customerInfo) {
        // locationId, lastName, email are required
        var customerData = {
          AccountHolder: customerInfo.email,
          CountryCode: 'us',
          Email: customerInfo.email,
          LastName: customerInfo.lastName,
        }

        if (customerInfo.birthday) {
          customerData['Birthday'] = customerInfo.birthday;
        }
        if (customerInfo.firstName) {
          customerData['FirstName'] = customerInfo.firstName;
        }
        if (customerInfo.middleName) {
          customerData['MiddleName'] = customerInfo.middleName;
        }
        if (customerInfo.phone) {
          customerData['Phone'] = customerInfo.phone;
        }
        if (customerInfo.gender) {
          customerData['Gender'] = customerInfo.gender;
        }
        if (customerInfo.address) {
            if (customerInfo.address.street) {
              customerData['Address'] = customerInfo.address.street;
            }
            if (customerInfo.address.city) {
              customerData['City'] = customerInfo.address.city;
            }
            if (customerInfo.address.state) {
              customerData['StateCode'] = customerInfo.address.state
            }
            if (customerInfo.address.zip) {
              customerData['Zip'] = customerInfo.address.zip;
            }
        }
        if (customerInfo.license) {
          if (customerInfo.license.number) {
            customerData['LicenseNumber'] = customerInfo.license.number;
          }
          if (customerInfo.license.state) {
            customerData['LicenseState'] = customerInfo.license.state;
          }
          if (customerInfo.license.expirationDate) {
            customerData['LicenseExpiry'] = customerInfo.license.expirationDate;
          }
        }

        var body = {
          ClientID: _clientID,
          ServiceUserName: _serviceUserName,
          ServicePassword: _servicePassword,
          CustomerData: customerData,
          LocationID: customerInfo.marketId
        };

        return callRentcentricWithPath('/AddCustomer', body, false, null).then(function(httpResponse) {
          return Parse.Promise.as(httpResponse.data.CustomerID);
        });
      }
    }
  },

  Reservations: {
    listStartTodayUnPaid: function(index, size) {
      var start = new Date();
      // Get the reservations for today (today being eastern time zone).
      var todayEST = Utils.getDateForTimezone(start, Utils.easternTimezone);
      var today = {
        Year: todayEST.getFullYear(),
        Month: todayEST.getMonth() + 1,
        Day: todayEST.getDate()
      };

      var body = {
        ClientID: _clientID,
        ServiceUserName: _serviceUserName,
        ServicePassword: _servicePassword,
        DateOutStart: today,
        PaidInFull: false,
        ReservationType: 'Billable',
        Index:index,
        Size:size
      };

      return callRentcentricWithPath('/GetReservationList', body, false, null).then(function(httpResponse) {
        var end = new Date();
        var elapsedTime = Utils.elapsedTime(start, end);
        if (elapsedTime.seconds > 8) {
          return Log.info('' + elapsedTime.seconds + ' seconds /GetReservationList returned ' + httpResponse.data.Reservations.length).always(function() {
            return Parse.Promise.as(httpResponse.data.Reservations);
          });
        } else {
          return Parse.Promise.as(httpResponse.data.Reservations);
        }
      });
    },
    countStartTodayUnPaid: function() {
      var start = new Date();
      // Get the reservations for today (today being eastern time zone).
      var todayEST = Utils.getDateForTimezone(start, Utils.easternTimezone);
      var today = {
        Year: todayEST.getFullYear(),
        Month: todayEST.getMonth() + 1,
        Day: todayEST.getDate()
      };

      var body = {
        ClientID: _clientID,
        ServiceUserName: _serviceUserName,
        ServicePassword: _servicePassword,
        DateOutStart: today,
        PaidInFull: false,
        ReservationType: 'Billable',
      };

      return callRentcentricWithPath('/GetReservationCount', body, false, null).then(function(httpResponse) {
        return Parse.Promise.as(httpResponse.data.ReservationCount);
      });
    },
    updatePaid: function(billing) {
      var taxList = [];
      var taxes = JSON.parse(billing.get("taxes"));
      _.each(taxes, function(taxInfo) {
        var tax = new Object();
        tax.TaxCode = taxInfo.code;
        tax.TaxName = taxInfo.name;
        tax.TaxPercentage = taxInfo.percentage;
        tax.TotalAmount = taxInfo.amount;
        taxList.push(tax);
      });

      var miscCharges = [];
      var fees = billing.get("fees");
      if (fees) {
        _.each(fees, function(fee) {
          var miscCharge = new Object();
          miscCharge.MiscChargeID = fee.id;
          miscCharge.TotalAmount = fee.amount;
          miscCharges.push(miscCharge);
        });
      }

      var res = [{
        PaymentAmount:billing.get('totalAmount'),
        PaymentMethod:"Stripe",
        ReservationID:billing.get('reservationId'),
        Taxes:taxList,
        MiscCharges:miscCharges
      }];

      if (billing.get('paid')) {
        res[0]["Success"] = {TransactionCode:billing.get('transCode')};
        res[0]['PaymentDate'] = billing.get('paid').getTime();
        res[0]['AuthCode'] = billing.get('authCode');
        res[0]['CardNumber'] = billing.get('lastFour');
      } else if (billing.get('paymentError')) {
        res[0]["Failure"] = billing.get('paymentError');
      }

      var body = {
        ClientID: _clientID,
        ServiceUserName: _serviceUserName,
        ServicePassword: _servicePassword,
        Reservations: res
      };

      return callRentcentricWithPath('/SetReservationPayments', body, false, null).then(function(httpResponse) {
        return Parse.Promise.as('reservation payment updated');
      });
    },
    V2: {
      cancel: function(customerId, reservationId) {
        var body = {
          ClientID: _clientID,
          ServiceUserName: _serviceUserName,
          ServicePassword: _servicePassword,
          ReservationID: reservationId,
          CustomerID: customerId
        };

        return callRentcentricWithPath('/CancelReservation', body, true, null).then(function(httpResponse) {
          return Parse.Promise.as('Sucess');
        });
      },
      delete: function(customerId, reservationId) {
        var body = {
          ClientID: _clientID,
          ServiceUserName: _serviceUserName,
          ServicePassword: _servicePassword,
          ReservationID: reservationId,
          CustomerID: customerId
        };

        return callRentcentricWithPath('/DeleteReservation', body, true, null).then(function(httpResponse) {
          return Parse.Promise.as('Sucess');
        });
      },
      createRenewal: function(rentalId, rateId, rateVehicleTypesId, days) {
        //console.log('create renewal entered rentalId=' + rentalId + ' rateId=' + rateId + ' rateVehicleTypesId=' + rateVehicleTypesId + ' days=' + days);

        var body = {
          ClientID: _clientID,
          ServiceUserName: _serviceUserName,
          ServicePassword: _servicePassword,
          RentalID: rentalId,
          RateID: rateId,
          RateVehicleTypesID: rateVehicleTypesId,
          Days: days
        };

        // API call succeeded but the user does not have an active rental.
        var didNotSucceedCallBack = function(httpResponse, path, body, isV2) {
          if (httpResponse && httpResponse.data && httpResponse.data.StatusInfo && httpResponse.data.StatusInfo.StatusCode && httpResponse.data.StatusInfo.StatusCode == "100") {
            return Parse.Promise.error(Error.rentalAlreadyRenewedError());
          } else if (httpResponse && httpResponse.data && httpResponse.data.StatusInfo && httpResponse.data.StatusInfo.StatusCode && httpResponse.data.StatusInfo.StatusCode == "101") {
            return Parse.Promise.error(Error.reservationAlreadyExistsForCustomerError());
          } else {
            return logRentCentricErrorForHTTPResponse(httpResponse, path, body, isV2);
          }
        };

        return callRentcentricWithPath('/CreateRenewalReservation', body, true, didNotSucceedCallBack).then(function(httpResponse) {
          var reservation = httpResponse.data.Reservations;
          return Parse.Promise.as(reservation);
        });
      },
      create: function(customerId, locationId, pickupLocationId, dropoffLocationId, assignedVehicleId, vehicleId, rateId, rateVehicleTypesId, reservationType, days, startDate, expectedPickupDate) {
        var body = {
          ClientID: _clientID,
          ServiceUserName: _serviceUserName,
          ServicePassword: _servicePassword,
          Reservation : {
            CustomerID: customerId,
            LocationID: locationId,
            PickupLocationID: pickupLocationId,
            DropoffLocationID: dropoffLocationId,
            ReservationType: reservationType,
            RateID: rateId,
            RateVehicleTypesID: rateVehicleTypesId,
            DateOut: startDate.getTime(),
            ExpectedDateOut: expectedPickupDate.getTime(),
            Days: days
          }
        };

/*        if (Environment.isDebugEnv()) {
          console.log('Creating reservation using primary key for vehicle id')
          body.Reservation.VehicleID = vehicleId
        } else {
          console.log('Creating reservation using assigned id for vehicle id')
          body.Reservation.VehicleID = assignedVehicleId
        }
*/
        body.Reservation.VehicleID = vehicleId;
        // API call succeeded but the user does not have an active rental.
        var didNotSucceedCallBack = function(httpResponse, path, body, isV2) {
          if (httpResponse && httpResponse.data && httpResponse.data.StatusInfo && httpResponse.data.StatusInfo.StatusCode && httpResponse.data.StatusInfo.StatusCode == "101") {
            return Parse.Promise.error(Error.reservationAlreadyExistsForCustomerError());
          } else if (httpResponse && httpResponse.data && httpResponse.data.StatusInfo && httpResponse.data.StatusInfo.StatusCode && httpResponse.data.StatusInfo.StatusCode == "2") {
            return Parse.Promise.error(Error.vehicleNotAvailableError());
          } else {
            return logRentCentricErrorForHTTPResponse(httpResponse, path, body, isV2);
          }
        };

        return callRentcentricWithPath('/CreateReservation', body, true, didNotSucceedCallBack).then(function(httpResponse) {
          return Parse.Promise.as(httpResponse.data.ReservationID);
        });
      },
      update: function(reservationId, rateId, rateVehicleTypesId, days, start, end, expectedStart) {
        //console.log('update reservation entered reservationId=' + reservationId + ' rateId=' + rateId + ' rateVehicleTypesId=' + rateVehicleTypesId + ' days=' + days + ' start ' + start + ' end ' + end);

        var body = {
          ClientID: _clientID,
          ServiceUserName: _serviceUserName,
          ServicePassword: _servicePassword,
          UpdateReservation: {
            DateOut:start,
            ReservationID: reservationId}
        };

        if (expectedStart) {
          body.UpdateReservation['ExpectedDateOut'] = expectedStart;
        }
        if (rateId) {
          body.UpdateReservation['RateID'] = rateId;
        }
        if (rateVehicleTypesId) {
          body.UpdateReservation['RateVehicleTypesID'] = rateVehicleTypesId;
        }
        if (days) {
          body.UpdateReservation['Days'] = days;
        }
        if (end) {
          body.UpdateReservation['DateIn'] = end;
        }
        return callRentcentricWithPath('/UpdateReservation', body, true, null).then(function(httpResponse) {
          return Parse.Promise.as({ "reservationId" : reservationId});
        });
      },
      retrieve: function(reservationId) {
        var body = {
          ClientID: _clientID,
          ServiceUserName: _serviceUserName,
          ServicePassword: _servicePassword,
          ReservationID: reservationId
        };

        var didNotSucceedCallBack = function(httpResponse, path, body, isV2) {
          if (httpResponse && httpResponse.data && httpResponse.data.StatusInfo && httpResponse.data.StatusInfo.StatusCode && httpResponse.data.StatusInfo.StatusCode == "102") {
            return Parse.Promise.error(Error.reservationNotFoundError());
          } else {
            return logRentCentricErrorForHTTPResponse(httpResponse, path, body, isV2);
          }
        };

        return callRentcentricWithPath('/GetReservationDetails', body, true, didNotSucceedCallBack).then(function(httpResponse) {
          return Parse.Promise.as(httpResponse.data.ReservationDetailsInfo);
        });
      },
      retrieveByCustomer: function(customerId) {
        var body = {
          ClientID: _clientID,
          ServiceUserName: _serviceUserName,
          ServicePassword: _servicePassword,
          CustomerID: customerId
        };

        return callRentcentricWithPath('/GetReservations', body, true, null).then(function(httpResponse) {
          return Parse.Promise.as(httpResponse.data.ReservationDetailsInfo);
        });
      },
      convertToRental: function(reservationId) {
        var body = {
          ClientID: _clientID,
          ServiceUserName: _serviceUserName,
          ServicePassword: _servicePassword,
          Reservations: [reservationId],
          DontSendScheduleJobEmail:true
        };

        return callRentcentricWithPath('/ConvertReservationsToRentals', body, true, null).then(function(httpResponse) {
          return Parse.Promise.as(httpResponse.data.ConvertReservations);
        });
      }
    }
  },

  Rentals: {
    keys: {
      rentalNumber: 'rentalNumber',
      vehicleId: 'vehicleId'
    },
    retrieveActive: function(customerId) {
      var body = {
        ClientID: _clientID,
        ServiceUserName: _serviceUserName,
        ServicePassword: _servicePassword,
        CustomerID: customerId
      };

      // API call succeeded but the user does not have an active rental.
      var didNotSucceedCallBack = function() {
        return Parse.Promise.as(null);
      };

      return callRentcentricWithPath('/GetActiveRental', body, false, didNotSucceedCallBack).then(function(httpResponse) {
        if (httpResponse) {
          var rental = {
            rentalNumber: httpResponse.data.RaNumber,
            vehicleId: httpResponse.data.VehicleID
          };
          return Parse.Promise.as(rental);
        } else {
          // Not vehicle for user
          return Parse.Promise.as();
        }
      });
    },
    V2: {
      updateRenewalFlag: function(rentalId, flag) {
        var promise = new Parse.Promise();
        var yesOrNo = flag ? "Yes" : "No";
        var body = {
          ClientID: _clientID,
          ServiceUserName: _serviceUserName,
          ServicePassword: _servicePassword,
          RentalID: rentalId,
          Flag: yesOrNo
        };

        // TODO: This method does not return a good error for an invalid rentalId or if the flag is not Yes or No.
        return callRentcentricWithPath('/SetRentalRenewal', body, true, null).then(function(httpResponse) {
          return Parse.Promise.as(httpResponse.data);
        });
      },
      retrieve: function(customerId, rentalId) {
        var body = {
          ClientID: _clientID,
          ServiceUserName: _serviceUserName,
          ServicePassword: _servicePassword,
          CustomerID: customerId,
          RentalID: rentalId
        };

        // API call succeeded check for error
        var didNotSucceedCallBack = function(httpResponse, path, body, isV2) {
          if (httpResponse && httpResponse.data && httpResponse.data.StatusInfo && httpResponse.data.StatusInfo.StatusCode && httpResponse.data.StatusInfo.StatusCode == "102") {
            return Parse.Promise.error(Error.rentalNotFoundError());
          } else {
            return logRentCentricErrorForHTTPResponse(httpResponse, path, body, isV2);
          }
        };

        return callRentcentricWithPath('/GetRentalDetails', body, true, didNotSucceedCallBack).then(function(httpResponse) {
          return Parse.Promise.as(httpResponse.data.RentalDetails);
        });
      },
      count: function(customerId) {
        var body = {
          ClientID: _clientID,
          ServiceUserName: _serviceUserName,
          ServicePassword: _servicePassword,
          CustomerID: customerId
        };

        return callRentcentricWithPath('/GetRentalCount', body, true, null).then(function(httpResponse) {
          if (httpResponse.data.RentalCount != null) {
            return Parse.Promise.as(httpResponse.data.RentalCount);
          } else {
            return Parse.Promise.error('Rental count body missing count ' + JSON.stringify(httpResponse.data));
          }
        });
      }
    }
  },

  Vehicles: {
    keys: {
      customerId: 'CustomerID',
      customerFirstName: 'CustomerFistName',
      customerMiddleName: 'CustomerMiddleName',
      customerLastName: 'CustomerLastName',
      vehicleId: 'VehicleID',
      assignedId: 'AssignedID',
      year: 'YearMade',
      make: 'MakeName',
      model: 'ModelName',
      exteriorColor: 'ExteriorColor',//NOTE: This key is not currently available from the list function.
      weeklyLowMilagePlanId: 'WeeklyLowMilagePlanID',
      weeklyHighMilagePlanId: 'WeeklyHighMilagePlanID',
      monthlyLowMilagePlanId: 'MonthlyLowMilagePlanID',
      monthlyHighMilagePlanId: 'MonthlyHighMilagePlanID'
    },
    retrieve: function(vehicleId) {
      var body = {
        ClientID: _clientID,
        ServiceUserName: _serviceUserName,
        ServicePassword: _servicePassword,
        VehicleID: vehicleId
      };

      return callRentcentricWithPath('/GetVehicleDetails', body, false, null).then(function(httpResponse) {
        return Parse.Promise.as(httpResponse.data.VehicleDetailsInfo);
      });
    },
    list: function(locationId, index, size) {
      var body = {
        ClientID: _clientID,
        ServiceUserName: _serviceUserName,
        ServicePassword: _servicePassword,
        LocationID: locationId,
        Statuses:["Available","On Rent"]
      };

      if (index && size) {
        body["Index"]=index;
        body["Size"] = size;
      }

      var start = new Date();
      return callRentcentricWithPath('/GetLocationVehicles', body, false, null).then(function(httpResponse) {
        var end = new Date();
        var elapsedTime = Utils.elapsedTime(start, end);
        if (elapsedTime.seconds > 8) {
          return Log.info('' + elapsedTime.seconds + ' seconds /GetLocationVehicles(LocationID=' + locationId + ',Statuses=[Available,On Rent]) returned ' + httpResponse.data.LocationVehiclesInfo.length).always(function() {
            return Parse.Promise.as(httpResponse.data.LocationVehiclesInfo);
          });
        } else {
          return Parse.Promise.as(httpResponse.data.LocationVehiclesInfo);
        }
      });
    },
    V2: {
      getRates: function(vehicleType, locationId) {
        var body = {
          ClientID: _clientID,
          ServiceUserName: _serviceUserName,
          ServicePassword: _servicePassword,
          LocationID: locationId,
          VehicleType: vehicleType
        };

        return callRentcentricWithPath('/GetRates', body, true, null).then(function(httpResponse) {
          var rates = httpResponse.data.Rates;
          //console.log("Rates: " + JSON.stringify(rates));
          // Do until FLXS-156. Filter the subrates and only keep
          // the one that matches the vehicle type
          if (vehicleType) {
            var vehicleRateType;
            _.each(rates, function(rate) {
              vehicleRateType = Utils.getRateVehicleType(vehicleType, rate.RateVehicleTypes);
              if (vehicleRateType) {
                rate.RateVehicleTypes.length = 0;
                rate.RateVehicleTypes.push(vehicleRateType);
              }
            });
          }

          //console.log("Rates: " + JSON.stringify(rates));
          return Parse.Promise.as(rates);
        });
      },
      retrieve: function(vehicleId) {
        var body = {
          ClientID: _clientID,
          ServiceUserName: _serviceUserName,
          ServicePassword: _servicePassword,
          VehicleID: vehicleId
        };

        var didNotSucceedCallBack = function(httpResponse, path, body, isV2) {
          if (httpResponse && httpResponse.data && httpResponse.data.StatusInfo && httpResponse.data.StatusInfo.StatusCode && httpResponse.data.StatusInfo.StatusCode == "102") {
            return Parse.Promise.error(Error.vehicleNotFoundError());
          } else {
            return logRentCentricErrorForHTTPResponse(httpResponse, path, body, isV2);
          }
        };

        return callRentcentricWithPath('/GetVehicleDetails', body, true, didNotSucceedCallBack).then(function(httpResponse) {
          return Parse.Promise.as(httpResponse.data.VehicleDetailsInfo);
        });
      },
      list: function(locationId, index, size) {
        var body = {
          ClientID: _clientID,
          ServiceUserName: _serviceUserName,
          ServicePassword: _servicePassword,
          Statuses:["Available","On Rent"],
          LocationID: locationId,
          Index:index,
          Size:size
        };

        var start = new Date();
        return callRentcentricWithPath('/GetLocationVehicles', body, true, null).then(function(httpResponse) {
          var end = new Date();
          var elapsedTime = Utils.elapsedTime(start, end);
          if (elapsedTime.seconds > 2) {
            return Log.info('' + elapsedTime.seconds + ' seconds /GetLocationVehicles(LocationID=' + locationId + ',Statuses=[Available,On Rent]),Index=' + index + ',Size In=' + size + ' Size Out=' + httpResponse.data.LocationVehiclesInfo.length).always(function() {
              return Parse.Promise.as({vehicles:httpResponse.data.LocationVehiclesInfo,index:httpResponse.data.Index,size:httpResponse.data.Size});
            });
          } else {
            return Parse.Promise.as({vehicles:httpResponse.data.LocationVehiclesInfo,index:httpResponse.data.Index,size:httpResponse.data.Size});
          }
        });
      },
    }
  },

  Locations: {
    list: function() {
      var body = {
        ClientID: _clientID,
        ServiceUserName: _serviceUserName,
        ServicePassword: _servicePassword,
        Active: true
      };

      return callRentcentricWithPath('/GetLocations', body, false, null).then(function(httpResponse) {
        return Parse.Promise.as(httpResponse.data.LocationsInfo);
      });
    },
    V2: {
      getTaxes: function(locationId) {
        var body = {
          ClientID: _clientID,
          ServiceUserName: _serviceUserName,
          ServicePassword: _servicePassword,
          LocationID: locationId
        };

        return callRentcentricWithPath('/GetLocationTaxes', body, true, null).then(function(httpResponse) {
          return Parse.Promise.as(httpResponse.data.Taxes);
        });
      },
      GetMiscCharges: function(locationId) {
        var body = {
          ClientID: _clientID,
          ServiceUserName: _serviceUserName,
          ServicePassword: _servicePassword,
          LocationID: locationId
        };

        return callRentcentricWithPath('/GetLocationMiscCharges', body, true, null).then(function(httpResponse) {
          return Parse.Promise.as(httpResponse.data.MiscCharges);
        });
      }
    }
  },
};

// Private API:

function customerDataWithData(data) {
  var customerData = {};
  var publicApiKeys = _.keys(data);
  _.each(publicApiKeys, function(key) {
    var jsonKey = customerKeys[key];
    if (jsonKey) {
      customerData[jsonKey] = data[key];
    }
  });
  return customerData;
}

function callRentcentricWithPath(path, body, isV2, didNotSucceedCallBack) {
  try {
    console.log("Calling RentCentric with: " + path);
    return Parse.Cloud.httpRequest({
      method: 'Post',
      url: _url + path,
      headers: _headers,
      body: body
    }).then(function(httpResponse) {
      try {
        var didSucceed = isSuccessResponse(httpResponse, path);
        if (didSucceed) {
          console.log("Call to RentCentric for " + path + " Succeeded");
          return Parse.Promise.as(httpResponse);
        } else {
          console.log("Call to RentCentric for " + path + " Failed (" + JSON.stringify(body) + ")");
          if (didNotSucceedCallBack) {
            return didNotSucceedCallBack(httpResponse, path, body, isV2);
          } else {
            return logRentCentricErrorForHTTPResponse(httpResponse, path, body, isV2);
          }
        }
      } catch (error) {
        console.log("Call to RentCentric for " + path + " Failed (" + JSON.stringify(body) + ")");
        return logRentCentricErrorForJavascriptError(error, path, body, isV2);
      };
    }, function(httpResponse) {
      console.log("Call to RentCentric for " + path + " Failed");
      return logRentCentricErrorForHTTPResponse(httpResponse, path, body, isV2);
    });
  } catch (error) {
    console.log("Call to RentCentric for " + path + " Failed");
    return logRentCentricErrorForJavascriptError(error, path, body, isV2);
  };
}

function logRentCentricErrorForJavascriptError(error, methodName, body, isV2) {
  var rentCentricError = Error.systemError();
  var name = error.name;
  var message = error.message;
  var errorMessage = { name : message };
  Error.addObjectToError(rentCentricError, error);

  var promiseError = isV2 ? rentCentricError : errorMessage;
  return Log.logFlexDriveError(rentCentricError, body, null, null, null, methodName, null, null).then(function() {
    return Parse.Promise.error(promiseError);
  }, function(error) {
    return Parse.Promise.error(promiseError);
  });
}

function logRentCentricErrorForHTTPResponse(httpResponse, methodName, body, isV2) {
  var status = statusForResponse(httpResponse);

  var rentCentricError;

  if (status.code == RESERVATION_ALREADY_PAID) {
    rentCentricError = Error.reservationCannotCancelAlreadyPaidError();
  } else {
    rentCentricError = Error.rentCentricError();
  }
  Error.addObjectToError(rentCentricError, status);

  var promiseError = isV2 ? rentCentricError : status.description;

  if (isV2) {
    return Parse.Promise.error(promiseError);
  } else {
    return Log.logFlexDriveError(rentCentricError, body, null, null, null, methodName, null, null).then(function() {
      return Parse.Promise.error(promiseError);
    }, function(error) {
      return Parse.Promise.error(promiseError);
    });
  }
}

function isSuccessResponse(httpResponse) {
  if (httpResponse.data) {
    var statusCode = httpResponse.data.StatusInfo.StatusCode;
    return statusCode == 0;
  } else {
    return false;
  }
}

function statusForResponse(httpResponse) {
  var statusCode = '';
  var statusDescription;
  var didSucceed = isSuccessResponse(httpResponse);

  if (httpResponse.data) {
    statusCode = httpResponse.data.StatusInfo.StatusCode;
    statusDescription = httpResponse.data.StatusInfo.Description;
  } else {
    statusDescription = httpResponse.text;
  }

  return {
    didSucceed: didSucceed,
    description: statusDescription,
    code: statusCode
  };
}
