var Billing = require('./flexdrive-billing.js');
var Environment = require('./flexdrive-environment.js');
var Error = require('./flexdrive-error.js');
var FlexDrive = require('./flexdrive.js');
var Utils = require('./flexdrive-utils.js');
var Log = require('./flexdrive-log.js');
var Data = require('./flexdrive-data.js');
var _ = require('underscore');
var json = require('express-json');
var bodyParser = require('body-parser');
var session = require('express-session');
var MongoStore = require('connect-mongo')(session);
var cookieParser = require('cookie-parser');
var basicAuth = require('basic-auth-connect');
var express = require("express");
var app = require('../../index.js');

var RentCentric = require("./rentcentric.js");
var RentCentricEnvironment = Environment.RentCentric.getEnvironment();
RentCentric.initialize(RentCentricEnvironment.url, RentCentricEnvironment.clientID, RentCentricEnvironment.serviceUserName, RentCentricEnvironment.servicePassword);

var ParseEnvironment = Environment.Parse.getEnvironment();

var protocol = Environment.isLocalEnv() ? 'http://' : 'https://';
var parseUrl =  protocol + ParseEnvironment.hostName;
var rentCentricCustomerUrl = RentCentricEnvironment.customerWebPage;
var rentCentricReservationUrl = RentCentricEnvironment.reservationWebPage;
var retrieveReservationErrorText = 'Could not run billing for reservation ';

var admin = express();
admin.use(json());
admin.set('views', __dirname + '/views');  // Specify the folder to find templates
admin.set('view engine', 'ejs');    // Set the template engine
admin.use(bodyParser.json());    // Middleware for reading request body
admin.use(bodyParser.urlencoded({
  extended: true
}));

// session management
var sessionConfig = {
  secret: process.env.COOKIE_SECRET,
  saveUninitialized: true,
  resave: true,
  store: new MongoStore({
    url: process.env.DATABASE_URI,
    collection: "admin_sessions"
  }),
  cookie : {
    secure: !Environment.isLocalEnv(),
    httpOnly: true
  }
};
admin.use(session(sessionConfig));

// Endpoint that will require basic auth
var auth = express();
auth.use(json())
auth.use(basicAuth(RentCentricEnvironment.basicAuthUser, RentCentricEnvironment.basicAuthPass));
auth.use(bodyParser.json());    // Middleware for reading request body

//TODO: re-enable https redirect when we have certs in place
if(!Environment.isLocalEnv()){
  // https only
  //var expressHttpsRedirect = require('express-https-redirect');
  //app.use('/', expressHttpsRedirect());
}
app.use('/api', auth); // auth endpoint path
app.use('/admin', admin); // auth endpoint path

admin.all('/', requireAuth);
auth.all('/', requireAuth);

function requireAuth(request, response) {
  if (request.session && request.session.user) {
    response.redirect('admin/home');
  } else {
    response.redirect('admin/login');
  }
}

admin.get('/login', function(request, response) {
  var error;
  var username = '';
  response.render('login', {username:username, error:error, url:parseUrl, env:ParseEnvironment.env});
});

admin.post('/login', function(request, response) {
  var error;
  var username = '';

  FlexDrive.Users.login(request.body.user,request.body.password).then(function(user) {
    if (user.get('admin') || user.get('allowBilling') || user.get('allowStripeReporting')) {
      request.session.user = user;
      response.redirect('home');
    } else {
      response.render('login', {username:username, error:"Unauthorized", url:parseUrl, env:ParseEnvironment.env});
    }
  }, function(error) {
    response.render('login', {username:username, error:"Unsuccessful log in: "+JSON.stringify(error), url:parseUrl, env:ParseEnvironment.env});
  });
});

admin.get('/logout', function(request, response) {
  Parse.User.logOut();
  request.session.user = null;
  var error;
  var username = '';
  response.render('login', {username:username, error:error, url:parseUrl, env:ParseEnvironment.env});
});

admin.get('/home', function(request, response) {
  if (!request.session || !request.session.user) {
    var error = '';
    var username = '';
    response.render('login', {username:username, error:error, url:parseUrl, env:ParseEnvironment.env});
    return;
  }
  response.render('home', {url:parseUrl, env:ParseEnvironment.env,isStripeReporter:request.session.user.allowStripeReporting,isAdmin:request.session.user.admin});
});


// Webpage to show billing results
admin.get('/billings', function(request, response) {

  if (!request.session || !request.session.user) {
    var error = '';
    var username = '';
    response.render('login', {username:username, error:error, url:parseUrl, env:ParseEnvironment.env});
    return;
  }

  Parse.Cloud.useMasterKey();

  var sort = 'none';
  if (request.query.sort) {
    sort = request.query.sort;
  }

  if (request.query.changeSort) {
    if (sort === 'none') {
      sort = 'up';
    } else if (sort === 'up') {
      sort = 'down';
    } else {
      sort = 'none';
    }
  }

  var reservationBilled;
  billReservation(request.query.reservationId).then(function(reservation) {
    if (reservation) {
      reservationBilled = reservation;
    }
    return billToday(request.query.run);
  }).then(function() {
    if (reservationBilled) {
      var dateOut = new Date(parseInt(reservationBilled.DateOut));
      request.query.when = '' + (dateOut.getMonth() + 1) + "/" + dateOut.getDate() + "/" + dateOut.getFullYear();
    }
    return getDateInfo(request.query.when, request.query.arrow === 'left', request.query.arrow === 'right');
  }).then(function(dateInfo) {
    getBillings(dateInfo.startDate, sort).then(function(results) {
      response.render('billings', { date: dateInfo.startDateMMDDYYYY, results: results, count: results.length, error: null, url:parseUrl, isToday:dateInfo.startDateIsToday, rcCustomerUrl:rentCentricCustomerUrl, rcReservationUrl:rentCentricReservationUrl, env:ParseEnvironment.env, sort:sort });
    }, function(error) {
      console.warn(JSON.stringify(error));
      response.render('billings', { date: dateInfo.startDateMMDDYYYY, results: [], count: 0, error: error + '. Press Go to reload the page.', url:parseUrl, isToday:dateInfo.startDateIsToday, rcCustomerUrl:rentCentricCustomerUrl, rcReservationUrl:rentCentricReservationUrl, env:ParseEnvironment.env, sort:sort });
    });
  }, function(error) {
    console.warn(JSON.stringify(error));
    getDateInfo(request.query.when, request.query.arrow === 'left', request.query.arrow === 'right').then(function(dateInfo) {
      getBillings(dateInfo.startDate, sort).then(function(results) {
        response.render('billings', { date: dateInfo.startDateMMDDYYYY, results: results, count: results.length, error: error, url:parseUrl, isToday:dateInfo.startDateIsToday, rcCustomerUrl:rentCentricCustomerUrl, rcReservationUrl:rentCentricReservationUrl, env:ParseEnvironment.env, sort:sort });
      }, function(error) {
        console.warn(JSON.stringify(error));
        response.render('billings', { date: dateInfo.startDateMMDDYYYY, results: [], count: 0, error: error + '. Press Go to reload the page.', url:parseUrl, isToday:dateInfo.startDateIsToday, rcCustomerUrl:rentCentricCustomerUrl, rcReservationUrl:rentCentricReservationUrl, env:ParseEnvironment.env, sort:sort });
      });
    });
  });
});

// Webpage to show a billing result
var statuses = ["Delinquent", "Returned", "Rebill"];
admin.get('/billings_edit', function(request, response) {

  if (!request.session || !request.session.user) {
    var error = '';
    var username = '';
    response.render('login', {username:username, error:error, url:parseUrl, env:ParseEnvironment.env});
    return;
  }

  Parse.Cloud.useMasterKey();

  return renderBilling(request, response);
});

admin.get('/billings_save', function(request, response) {

  if (!request.session || !request.session.user) {
    var error = '';
    var username = '';
    response.render('login', {username:username, error:error, url:parseUrl, env:ParseEnvironment.env});
    return;
  }

  Parse.Cloud.useMasterKey();

  if (request.query.action === 'cancel') {
    return renderBilling(request, response);
  }

  var sort = 'none';
  if (request.query.sort) {
    sort = request.query.sort;
  }

  var query = new Parse.Query('Billing');
  query.equalTo("reservationId", request.query.reservationId);
  return query.first().then(function(billing) {
    if (request.query.status && request.query.status != 'none') {
      billing.set('opsStatus', request.query.status);
    } else {
      billing.unset('opsStatus');
    }
    if (request.query.comment) {
      billing.set('opsComment', request.query.comment);
    } else {
      billing.unset('opsComment');
    }
    return billing.save();

  }).then(function(billing) {
    return getDateInfo(request.query.when, null, null);
  }).then(function(dateInfo) {
    getBillings(dateInfo.startDate, sort).then(function(results) {
      response.render('billings', { date: dateInfo.startDateMMDDYYYY, results: results, count: results.length, error: null, url:parseUrl, isToday:dateInfo.startDateIsToday, rcCustomerUrl:rentCentricCustomerUrl, rcReservationUrl:rentCentricReservationUrl, env:ParseEnvironment.env, sort:request.query.sort });
    }, function(error) {
      console.warn(JSON.stringify(error));
      response.render('billings', { date: dateInfo.startDateMMDDYYYY, results: [], count: 0, error: error + '. Press Go to reload the page.', url:parseUrl, isToday:dateInfo.startDateIsToday, rcCustomerUrl:rentCentricCustomerUrl, rcReservationUrl:rentCentricReservationUrl, env:ParseEnvironment.env, sort:request.query.sort });
    });
  });
});

// Webpage to show error log results
admin.get('/logging', function(request, response) {

  if (!request.session || !request.session.user) {
    var error = '';
    var username = '';
    response.render('login', {username:username, error:error, url:parseUrl, env:ParseEnvironment.env});
    return;
  }

  Parse.Cloud.useMasterKey();

  return getDateInfo(request.query.when, request.query.arrow === 'left', request.query.arrow === 'right').then(function(dateInfo) {
    getErrorLogs(dateInfo.startDate).then(function(results) {
      response.render('logging', { date: dateInfo.startDateMMDDYYYY, results: results, count: results.length, error: null, url:parseUrl, isToday:dateInfo.startDateIsToday, env:ParseEnvironment.env});
    }, function(error) {
      console.warn(JSON.stringify(error));
      response.render('logging', { date: dateInfo.startDateMMDDYYYY, results: [], count: 0, error: error + '. Press Go to reload the page.', url:parseUrl, isToday:dateInfo.startDateIsToday, env:ParseEnvironment.env});
    });
  }, function(error) {
    console.warn(JSON.stringify(error));
    getDateInfo(request.query.when, request.query.arrow === 'left', request.query.arrow === 'right').then(function(dateInfo) {
      getErrorLogs(dateInfo.startDate).then(function(results) {
        response.render('logging', { date: dateInfo.startDateMMDDYYYY, results: results, count: results.length, error: error, url:parseUrl, isToday:dateInfo.startDateIsToday, env:ParseEnvironment.env});
      }, function(error) {
        console.warn(JSON.stringify(error));
        response.render('logging', { date: dateInfo.startDateMMDDYYYY, results: [], count: 0, error: error + '. Press Go to reload the page.', url:parseUrl, isToday:dateInfo.startDateIsToday, env:ParseEnvironment.env});
      });
    });
  });
});

// Webpage to show a log
admin.get('/log', function(request, response) {

  if (!request.session || !request.session.user) {
    var error = '';
    var username = '';
    response.render('login', {username:username, error:error, url:parseUrl, env:ParseEnvironment.env});
    return;
  }

  Parse.Cloud.useMasterKey();

  return getDateInfo(request.query.when).then(function(dateInfo) {
    getErrorLog(request.query.id).then(function(result) {
      response.render('log', { date: dateInfo.startDateMMDDYYYY, result: result, error: null, url:parseUrl, env:ParseEnvironment.env});
    }, function(error) {
      console.warn(JSON.stringify(error));
      response.render('log', { date: dateInfo.startDateMMDDYYYY, result: null, error: error, url:parseUrl, env:ParseEnvironment.env});
    });
  }, function(error) {
    console.warn(JSON.stringify(error));
    response.render('log', { date: dateInfo.startDateMMDDYYYY, result: null, error: error, url:parseUrl, env:ParseEnvironment.env});
  });
});

// Webpage to show stripe reporting input page
admin.get('/stripeReporting', function(request, response) {

  if (!request.session || !request.session.user) {
    var error = '';
    var username = '';
    response.render('login', {username:username, error:error, url:parseUrl, env:ParseEnvironment.env});
    return;
  }

  if (!request.session.user.admin && !request.session.user.allowStripeReporting) {
    request.session.user = null;
    response.render('login', {username:username, error:"Unauthorized", url:parseUrl, env:ParseEnvironment.env});
  }

  Parse.Cloud.useMasterKey();

  var processMe = false
  if (request.query.processMe && request.query.processMe == 'true') {
    processMe = true
  }

  var deleteId
  if (request.query.action && request.query.action == 'delete' && request.query.id) {
    deleteId = request.query.id
  }

  var csv
  var csvCount = 0;
  var viewId
  if (request.query.action && request.query.action == 'view' && request.query.id) {
    viewId = request.query.id
  }

  var dateInfo;
  return getFromToDates(request.query.fromDate, request.query.toDate).then(function(dInfo) {
    dateInfo = dInfo;
    if (deleteId) {
      var query = new Parse.Query('StripeReport');
      query.equalTo('objectId',deleteId);
      return query.first().then(function(stripeReport) {
        return stripeReport.destroy()
      })
    } else {
      return Parse.Promise.as()
    }
  }).always(function() {
    if (viewId) {
      var lineItemColumns = 1
      var stripeReport
      var query = new Parse.Query('StripeReport');
      query.equalTo('objectId',viewId);
      return query.first().then(function(_stripeReport) {
        stripeReport = _stripeReport
        return Utils.getConfigValue('lineItemColumns')
      }).then(function(_lineItemColumns) {
        lineItemColumns = _lineItemColumns
        return Utils.getConfigValue('refundColumns')
      }).then(function(refundColumns) {
        csv = ''
        var lineItems = stripeReport.get('lineItems')
        for (var i = 0; i < lineItems.length; ++i) {
          csv += lineItems[i] + '\n'
        }
        csvCount = lineItems.length
        csvHeader = 'Charge Date,Stripe ID,Email,Charge ID,Description,Statement Description,Amount,Refunded,Fee,Net,Invoice,Rate'
        for (var i = 0; i < 4; ++i) {
          csvHeader += ',Description'
          csvHeader += ',Amount'
        }
        for (var i = 4; i < lineItemColumns; ++i) {
          csvHeader += ',Description'
          csvHeader += ',Amount'
        }
        for (var i = 0; i < refundColumns; ++i) {
          csvHeader += ',Refund'
        }
        csvHeader += ',Reservation ID,Rent Centric User ID,Market'
        csvStatus = stripeReport.get('status')
      })
    } else {
      return Parse.Promise.as()
    }
  }).always(function() {
    var query = new Parse.Query('StripeReport');
    query.descending('createdAt');
    return query.find();
  }).then(function(reports) {
    return terminateReports(reports, processMe);
  }).then(function() {
    if (processMe) {
      var StripeReport = Parse.Object.extend("StripeReport");
      var stripeReport = new StripeReport();
      stripeReport.set('status',"New")
      stripeReport.set('startDate',dateInfo.startDate)
      stripeReport.set('endDate',dateInfo.endDate)
      stripeReport.set('username',request.session.user.username)
      stripeReport.set('error',false)
      return stripeReport.save();
    } else {
      return Parse.Promise.as();
    }
  }).then(function() {
    var query = new Parse.Query('StripeReport');
    query.descending('createdAt');
    return query.find();
  }).then(function(reports) {
    if (viewId) {
      response.render('stripeReport', {csv:csv, count:csvCount, header:csvHeader, status:csvStatus});
    } else {
      var results = [];
      for (var i = 0; i < reports.length; ++i) {
        results.push(mapReportToResult(reports[i]))
      }
      response.render('stripeReporting', { fromDate: dateInfo.startDateMMDDYYYY, toDate: dateInfo.endDateMMDDYYYY, reports: results, error: null, url:parseUrl, env:ParseEnvironment.env});
    }
  }, function(error) {
    console.warn(JSON.stringify(error));
    response.render('stripeReporting', { fromDate: '', toDate: '', reports: null, error: error + '. Try again.', url:parseUrl, env:ParseEnvironment.env});
  });
});

function executeRequest(request, response, initialFunc, path, requiredParameterNames, functionParameters) {
  Parse.Cloud.useMasterKey();

  try {
    console.log("Entered: " + path + " parameters: " + JSON.stringify(request.body));
    var parameters = []
    var missingParameterNames = [];

    if (requiredParameterNames) {
      requiredParameterNames.forEach(function(parameterName) {
        var parameter = request.body[parameterName];
        if (parameter != null) {
          parameters.push(parameter);
        } else {
          missingParameterNames.push(parameterName);
        }
      });
    }

    if (missingParameterNames.length > 0) {
      var additionalInfo = "Missing Request Parameter(s): " + missingParameterNames.join(", ")
      var error = Error.invalidInputError();
      Error.addObjectToError(error, additionalInfo);
      return Log.logFlexDriveError(error, request.body, null, null, path, null, null, null).then(function() {
        response.status(200).send({status:"1",error:error});
      });

      return;
    }

    if (functionParameters) {
      parameters = functionParameters;
    }

    initialFunc.apply(null, parameters).then(function() {
      console.log("Success For: " + path);
      response.status(200).send({status:"0",error:null});
    }, function(error) {
      var flexDriveError = Error.flexDriveErrorForError(error, Error.systemError);
      return Log.logFlexDriveError(flexDriveError, request.body, null, null, path, null, null, null).then(function() {
        console.error("Failed: " + JSON.stringify(flexDriveError));
        response.status(200).send({status:"1",error:error});
      });
    });
  } catch(error) {
    var flexDriveError = Error.systemErrorForError(error);
    return Log.logFlexDriveError(flexDriveError, request.body, null, null, path, null, null, null).then(function() {
      console.error("Failed: " + JSON.stringify(flexDriveError));
      response.status(200).send({status:"1",error:error});
    });
  }
}

auth.post('/v2_ping', function(request, response) {
  response.status(200).send({status:"0",error:null});
});

auth.post('/v2_RentalsRenewalNotification', function(request, response) {
  executeRequest(request, response, FlexDrive.Vehicles.Rentals.sendTimeToRenewPushes, "/v2_RentalsRenewalNotification", ["renewals"], null);
});

auth.post('/v2_ValidateDriverLicense', function(request, response) {
  executeRequest(request, response, FlexDrive.Vehicles.Rentals.validateDriverLicense, "/v2_ValidateDriverLicense", ["rentCentricCustomerId", "flag"], null);
});

auth.post('/v2_ReservationCreated', function(request, response) {
  var reservation = request.body.ReservationDetailsInfo;
  var reservationId = reservation.ReservationID;
  var parameters = [reservationId, reservation];

  executeRequest(request, response, FlexDrive.Reservations.V2.handleReservation, "/v2_ReservationCreated", ["ReservationDetailsInfo"], parameters);
});

auth.post('/v2_Location', function(request, response) {
  var location = request.body.LocationsInfo;
  if (location.Active && location.ParentLocationID != 0) {
    var query = new Parse.Query('OffsiteLocation');
    query.equalTo('MarketID',location.ID);
    return query.first().then(function(offsiteLocation) {
      if (offsiteLocation) {
        console.log('Updating offsite location');
        offsiteLocation.set('parentId',location.ParentLocationID);
        offsiteLocation.set('MarketName',location.Name);
        offsiteLocation.set('location',JSON.stringify(location));
        return offsiteLocation.save();
      } else {
        console.log('Creating offsite location');
        var OffsiteLocation = Parse.Object.extend("OffsiteLocation");
        var offsiteLocation = new OffsiteLocation();
        offsiteLocation.set('parentId',location.ParentLocationID);
        offsiteLocation.set('MarketName',location.Name);
        offsiteLocation.set('MarketID',location.ID);
        offsiteLocation.set('location',JSON.stringify(location));
        return offsiteLocation.save();
      }
    }).always(function() {
      console.log('returning 200')
      response.status(200).send({status:"0",error:null});
    });
  } else if (location.Active && location.ParentLocationID == 0) {
    var query = new Parse.Query('Markets');
    query.equalTo('MarketID',location.ID);
    return query.first().then(function(market) {
      if (market) {
        console.log('Updating market location');
        market.set('location',JSON.stringify(location));
        return market.save();
      } else {
        console.log('Creating market location');
        var Markets = Parse.Object.extend("Markets");
        var market = new Markets();
        market.set('MarketName',location.Name);
        market.set('MarketID',location.ID);
        market.set('location',JSON.stringify(location));
        return market.save();
      }
    }).always(function() {
      console.log('returning 200')
      response.status(200).send({status:"0",error:null});
    });
  } else {
    console.log('Problem with location ' + JSON.stringify(location));
    return Log.warn('Problem with location ' + JSON.stringify(location)).always(function() {
      console.log('returning 200')
      response.status(200).send({status:"0",error:null});
    });
  }
});

auth.post('/v2_Rates', function(request, response) {
  if (request.body.Rates) {
    var rates = request.body.Rates;
    var query = new Parse.Query('Markets');
    query.equalTo('MarketID', rates[0].LocationID);
    return query.first().then(function(market) {
      if (market) {
        market.set('rates',rates);
        return market.save();
      } else {
        return Log.warn('Problem with the rates sent by rent centric: ' + JSON.stringify(error));
      }
    }).then(function() {
      response.status(200).send({status:"0",error:null});
    }, function(error) {
      return Log.warn('Problem with the rates sent by rent centric: ' + JSON.stringify(error)).always(function() {
        response.status(200).send({status:"0",error:null});
      });
    });
  }
});

auth.post('/v2_Taxes', function(request, response) {
  if (request.body.Taxes) {
    var taxes = request.body.Taxes;
    var query = new Parse.Query('Markets');
    query.equalTo('MarketID', taxes[0].LocationID);
    return query.first().then(function(market) {
      if (market) {
        market.set('taxes',taxes);
        return market.save();
      } else {
        return Log.warn('Problem with the taxes sent by rent centric: ' + JSON.stringify(error));
      }
    }).then(function() {
      response.status(200).send({status:"0",error:null});
    }, function(error) {
      return Log.warn('Problem with the taxes sent by rent centric: ' + JSON.stringify(error)).always(function() {
        response.status(200).send({status:"0",error:null});
      });
    });
  }
});

auth.post('/v2_MiscCharges', function(request, response) {
  if (request.body.MiscCharges) {
    var miscCharges = request.body.MiscCharges;

    // TODO Remove the following once rent centric implements FLRC-82
    if (miscCharges) {
      for (var i = 0; i < miscCharges.length; ++i) {
        miscCharges[i].MiscChargeID = parseFloat(miscCharges[i].MiscChargeID);
        miscCharges[i].MiscChargeRate = parseFloat(miscCharges[i].MiscChargeRate);
      }
    }

    var query = new Parse.Query('Markets');
    query.equalTo('MarketID', miscCharges[0].LocationID);
    return query.first().then(function(market) {
      if (market) {
        market.set('miscCharges',miscCharges);
        return market.save();
      } else {
        return Log.warn('Problem with the misc charges sent by rent centric: ' + JSON.stringify(error));
      }
    }).then(function() {
      response.status(200).send({status:"0",error:null});
    }, function(error) {
      return Log.warn('Problem with the misc charges sent by rent centric: ' + JSON.stringify(error)).always(function() {
        response.status(200).send({status:"0",error:null});
      });
    });
  }
});

// This line is required to make Express respond to http requests.
app.listen();

function billReservation(reservationId) {
  console.log("billReservation " + reservationId);
  if (!reservationId) {
    return Parse.Promise.as();
  }

  var reservationToBill;
  return FlexDrive.Reservations.V2.retrieve(reservationId).then(function(reservation) {
    reservationToBill = reservation;
    reservation['billMe'] = true;
    return Billing.run([reservation], true);
  }).then(function() {
    if (reservationToBill.skipProcessing) {
      return Parse.Promise.as();
    } else {
      var query = new Parse.Query('Billing');
      query.equalTo("reservationId", reservationId);
      return query.first();
    }
  }).then(function(billing) {
    if (billing) {
      return FlexDrive.Reservations.notifyUsersOfPayment([billing]);
    } else {
      return Parse.Promise.as();
    }
  }).then(function() {
    return Parse.Promise.as(reservationToBill);
  }, function(error) {
    if (error.code && error.code === Error.unknownRentCentricError.code) {
      console.warn(retrieveReservationErrorText + reservationId + ' error=' + JSON.stringify(error));
      return Parse.Promise.error(retrieveReservationErrorText + reservationId);
    } else {
      return Parse.Promise.error(JSON.stringify(error));
    }
  });
}

function billToday(flag) {
  if (!flag) {
    return Parse.Promise.as();
  }

  console.log("going to run billing");
  return Billing.run(null, false).then(function() {
    return Billing.report();
  }).then(function(billings) {
    return FlexDrive.Reservations.notifyUsersOfPayment(billings);
  }, function(error) {
    console.warn(JSON.stringify(error));
    return Parse.Promise.error(error);
  });
}

function getDateInfo(when, previous, next) {
  var startDate;
  if (when) {
    try {
      startDate = new Date(when);
    } catch(err) {
      error = 'Invalid date'
    }
  }

  if (startDate && isNaN(startDate.getTime())) {
    startDate = null;
  }

  if (!startDate) {
    startDate = new Date();
    startDate = new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate());
  }

  if (previous) {
    startDate = new Date(startDate.getFullYear(), startDate.getMonth(), (startDate.getDate()-1));
  }
  if (next) {
    startDate = new Date(startDate.getFullYear(), startDate.getMonth(), (startDate.getDate()+1));
  }

  startDateMMDDYYYY = '' + (startDate.getMonth()+1) + '/' + startDate.getDate() + '/' + startDate.getFullYear();

  var startDateIsToday = false;
  var now = new Date();
  if (now.getFullYear() === startDate.getFullYear() && now.getMonth() === startDate.getMonth() && now.getDate() === startDate.getDate()) {
    startDateIsToday = true;
  }

  //console.log('startDateIsToday='+startDateIsToday);
  return Parse.Promise.as({startDate:startDate,startDateMMDDYYYY:startDateMMDDYYYY,startDateIsToday:startDateIsToday});
}

function getFromToDates(fromDate, toDate) {
  var startDate;
  if (fromDate) {
    try {
      startDate = new Date(fromDate);
    } catch(err) {
      error = 'Invalid date'
    }
  }

  if (startDate && isNaN(startDate.getTime())) {
    startDate = null;
  }

  if (!startDate) {
    startDate = new Date();
    startDate = new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate());
  }

  var endDate;
  if (toDate) {
    try {
      endDate = new Date(toDate);
    } catch(err) {
      error = 'Invalid date'
    }
  }

  if (endDate && isNaN(endDate.getTime())) {
    endDate = null;
  }

  if (!endDate) {
    endDate = new Date();
    endDate = new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate());
  }

  startDateMMDDYYYY = '' + (startDate.getMonth()+1) + '/' + startDate.getDate() + '/' + startDate.getFullYear();
  endDateMMDDYYYY = '' + (endDate.getMonth()+1) + '/' + endDate.getDate() + '/' + endDate.getFullYear();

  return Parse.Promise.as({startDate:startDate,startDateMMDDYYYY:startDateMMDDYYYY,endDate:endDate,endDateMMDDYYYY:endDateMMDDYYYY});
}

function getBillings(startDate, sort) {
  var startDate = new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate());
  var endDate = new Date(startDate.getFullYear(), startDate.getMonth(), (startDate.getDate()+1));

  var startDateGMT = Utils.convertDateFromTZToGMT(startDate, Utils.easternTimezone);
  var endDateGMT = Utils.convertDateFromTZToGMT(endDate, Utils.easternTimezone);

  var query = new Parse.Query('Billing');
  query.limit(1000);
  if (sort === 'up') {
    query.ascending('opsStatus');
  } else if (sort === 'down') {
    query.descending('opsStatus');
  } else {
    query.descending('updatedAt');
  }

  query.greaterThanOrEqualTo('resStart', startDateGMT);
  query.lessThan('resStart', endDateGMT);
  return query.find().then(function(billings) {
    var results = [];
    for (var i = 0; i < billings.length; ++i) {
      result = mapBillingToResult(billings[i]);
      results.push(result);
    }
    return Parse.Promise.as(results);
  });
}

function getBilling(reservationId) {
  var query = new Parse.Query('Billing');
  query.equalTo("reservationId", reservationId);
  return query.first().then(function(billing) {
    return Parse.Promise.as(mapBillingToResult(billing));
  });
}

function mapBillingToResult(billing) {
  result = new Object();
  result.id = billing.id;
  result.reservationId = billing.get('reservationId');
  result.rcCustomerId = billing.get('customerIDRentCentric');
  if (billing.get('paid')) {
    paidDate = billing.get('paid');
    result.paid = Utils.getDateTimezoneAsTextMMDDYYYY(paidDate, Utils.easternTimezone);
  } else {
    result.paid = '';
  }

  var createdDate = billing.createdAt;
  result.created = Utils.getDateTimezoneAsTextMMDDYYYY(createdDate, Utils.easternTimezone);

  var updatedDate = billing.updatedAt;
  result.updated = Utils.getDateTimezoneAsTextMMDDYYYY(updatedDate, Utils.easternTimezone);

  result.email = billing.get('email') ? billing.get('email') : '';

  result.error = '';
  if (billing.get('lastError')) {
    result.error = billing.get('lastError');
  }

  if (billing.get('opsStatus') != null) {
    result.status = billing.get('opsStatus');
  } else {
    result.status = '';
  }

  if (billing.get('opsComment') != null) {
    result.comment = billing.get('opsComment');
  } else {
    result.comment = '';
  }

  if (billing.get('balanceDue')) {
    result.balanceDue = billing.get('balanceDue');
  } else {
    result.balanceDue = '';
  }

  if (billing.get('taxAmount')) {
    result.taxes = billing.get('taxAmount');
  } else {
    result.taxes = '';
  }

  if (billing.get('fees')) {
    var fees = billing.get('fees');
    result.fees = '';
    for (var i = 0; i < fees.length; ++i) {
      result.fees += '$' + fees[i].amount + ' (' + fees[i].name + ')';
    }
  } else {
    result.fees = '';
  }

  if (billing.get('totalAmount')) {
    result.total = billing.get('totalAmount');
  } else {
    result.total = '';
  }

  if (billing.get('customerIDStripe')) {
    result.stripeId = billing.get('customerIDStripe');
  } else {
    result.stripeId = '';
  }

  if (billing.get('stripeInvoiceId')) {
    result.invoiceId = billing.get('stripeInvoiceId');
  } else {
    result.invoiceId = '';
  }

  if (billing.get('customerIDRentCentric')) {
    result.rentCentricId = billing.get('customerIDRentCentric');
  } else {
    result.rentCentricId = '';
  }

  if (billing.get('lastFour')) {
    result.lastFour = billing.get('lastFour');
  } else {
    result.lastFour = '';
  }

  if (billing.get('locationId')) {
    result.locationId = billing.get('locationId');
  } else {
    result.locationId = '';
  }

  if (billing.get('reservationUpdated')) {
    result.reservationUpdated = billing.get('reservationUpdated');
  } else {
    result.reservationUpdated = '';
  }

  if (billing.get(Data.Billing.keys.pending)) {
    result.pending = billing.get(Data.Billing.keys.pending);
  } else {
    result.pending = '';
  }

  if (billing.get(Data.Billing.keys.processing)) {
    result.processing = billing.get(Data.Billing.keys.processing);
  } else {
    result.processing = '';
  }

  var resStartDateEDTText = '';
  if (billing.get('resStart')) {
    resStartDate = billing.get('resStart');
    result.reservationStart = Utils.getDateTimezoneAsTextMMDDYYYY(resStartDate, Utils.easternTimezone);
  }

  return result;
}


function getErrorLogs(startDate) {
  var endDate = new Date(startDate.getFullYear(), startDate.getMonth(), (startDate.getDate()+1));

  var query = new Parse.Query('Log');
  query.limit(1000);
  query.descending('updatedAt');
  query.greaterThanOrEqualTo('createdAt', startDate);
  query.lessThan('createdAt', endDate);
  var results = [];
  return query.find().then(function(logs) {
    var chainedPromise = Parse.Promise.as();
    _.each(logs, function(log) {
      chainedPromise = chainedPromise.then(function() {
        return mapLogToResult(log).then(function(result) {
          results.push(result);
          return Parse.Promise.as();
        });
      });
    });
    return chainedPromise;
  }).then(function() {
    return Parse.Promise.as(results);
  });
}

function getErrorLog(id) {
  var query = new Parse.Query('Log');
  query.equalTo('objectId', id);
  return query.first().then(function(log) {
    return mapLogToResult(log);
  }, function(error) {
    return Parse.Promise.error(error);
  });
}

function mapLogToResult(log) {
  result = new Object();

  result.id = log.id;

  var createdDate = log.createdAt;
  var createdDateEDTText = Utils.getDateTimezoneAsText(createdDate, Utils.easternTimezone);
  result.created = createdDateEDTText;

  var updatedDate = log.updatedAt;
  var updatedDateEDTText = Utils.getDateTimezoneAsText(updatedDate, Utils.easternTimezone);
  result.updated = updatedDateEDTText;

  result.type = log.get('type') ? log.get('type') : '';
  result.errorCode = log.get('errorCode') ? log.get('errorCode') : '';
  result.description = log.get('description') ? log.get('description') : '';
  result.internalMessages = log.get('internalMessages') ? log.get('internalMessages') : '';
  result.userMessage = log.get('userMessage') ? log.get('userMessage') : '';
  result.cloudFunctionName = log.get('cloudFunctionName') ? log.get('.FunctionName') : '';
  result.cloudJobName = log.get('cloudJobName') ? log.get('.JobName') : '';
  result.expressPath = log.get('expressPath') ? log.get('expressPath') : '';
  result.methodName = log.get('methodName') ? log.get('methodName') : '';
  result.installation = log.get('installation') ? JSON.stringify(log.get('installation'),null,2) : '';

  var internalMessagesAsText = [];
  if (log.get('internalMessages')) {
    for (var j = 0; j < log.get('internalMessages').length; ++j) {
      internalMessagesAsText.push(JSON.stringify(log.get('internalMessages')[j]));
    }
  }
  result.internalMessagesAsText = internalMessagesAsText;

  if (log.get('user')) {
    var query = new Parse.Query(Parse.User);
    query.equalTo('objectId', log.get('user').id);
    return query.first().then(function(user) {
      if (user) {
        result.userInfo = [];
        result.userInfo.push('id: ' + user.id);
        result.userInfo.push('email: ' + user.get('email'));
      }
      return Parse.Promise.as(result);
    }, function(error) {
      console.warn(JSON.stringify(error));
      return Parse.Promise.error(error);
    });
  } else {
    return Parse.Promise.as(result);
  }
}

function mapReportToResult(report) {
  result = new Object();
  result.id = report.id
  result.username = report.get('username')
  result.status = report.get('status')
  var startDate = report.get('startDate');
  result.start = (startDate.getMonth() + 1) + '/' + startDate.getDate() + '/' + startDate.getFullYear();
  var endDate = report.get('endDate');
  result.end = (endDate.getMonth() + 1) + '/' + endDate.getDate() + '/' + endDate.getFullYear();
  if (report.get('error')) {
    result.error = 'Yes'
  } else {
    result.error = ''
  }

  var stripeProcessingDate = report.get('stripeProcessingDate');
  if (stripeProcessingDate) {
    if (stripeProcessingDate <= report.get('endDate')) {
      result.stripeProcessingDate = (stripeProcessingDate.getMonth() + 1) + '/' + stripeProcessingDate.getDate() + '/' + stripeProcessingDate.getFullYear();
    } else {
      result.stripeProcessingDate = 'Finished';
    }
  } else {
    result.stripeProcessingDate = '';
  }

  return result;
}

function leftPadZero(input) {
  var output = '' + input;
  if (output) {
    if (output.length === 2) {
      return output;
    } else if (output.length == 1) {
      return '0' + output;
    } else {
      return '00';
    }
  } else {
    return '';
  }
}

function renderBilling(request, response) {
  var sort = 'none';
  if (request.query.sort) {
    sort = request.query.sort;
  }

  return getDateInfo(request.query.when, null, null).then(function(dateInfo) {
    getBilling(request.query.reservationId).then(function(result) {
      response.render('billing', { date: dateInfo.startDateMMDDYYYY, result: result, error: null, url:parseUrl, env:ParseEnvironment.env, statuses:statuses, sort:sort });
    }, function(error) {
      console.warn(JSON.stringify(error));
      response.render('billing', { date: dateInfo.startDateMMDDYYYY, result: null, error: error, url:parseUrl, env:ParseEnvironment.env, statuses:statuses, sort:sort });
    });
  }, function(error) {
    console.warn(JSON.stringify(error));
    getDateInfo(request.query.when, null, null).then(function(dateInfo) {
      getBilling(request.query.reservationId).then(function(result) {
        response.render('billing', { date: dateInfo.startDateMMDDYYYY, result: result, error: error, url:parseUrl, env:ParseEnvironment.env, statuses:statuses });
      }, function(error) {
        console.warn(JSON.stringify(error));
        response.render('billing', { date: dateInfo.startDateMMDDYYYY, result: null, error: error, url:parseUrl, env:ParseEnvironment.env, statuses:statuses });
      });
    });
  });
}

function terminateReports(reports, flag) {
  var chainedPromise = Parse.Promise.as();
  _.each(reports, function(report) {
    chainedPromise = chainedPromise.then(function() {
      if (flag && (report.get('status') == "Processing" || report.get('status') == "New")) {
        report.set('status',"Terminated")
        return report.save()
      } else {
        return Parse.Promise.as();
      }
    });
  });
  return chainedPromise;
}
