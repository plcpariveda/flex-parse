
var Billing = require('./flexdrive-billing.js');
var Environment = require('./flexdrive-environment.js');
var FlexDrive = require('./flexdrive.js');
var FlexdriveStripe = require("./flexdrive-stripe.js");
var Utils = require('./flexdrive-utils.js');
var Error = require('./flexdrive-error.js');
var Log = require('./flexdrive-log.js');
var Notifications = require('./flexdrive-notifications.js');


Parse.Cloud.define('v2_Reservations_payment', function(request, response) {
    var requiredParameterNames = ["reservationId"];
    execute_v2_function(request, response, true, FlexDrive.Reservations.V2.handleReservation, requiredParameterNames, null, "v2_Reservations_payment");
});

Parse.Cloud.define('v2_Reservations_repayment', function(request, response) {
    var requiredParameterNames = ["reservationId"];
    execute_v2_function(request, response, true, FlexDrive.Reservations.V2.payAgain, requiredParameterNames, null, "v2_Reservations_repayment");
});

Parse.Cloud.define('v2_Vehicles_getRates', function(request, response) {
  var parameterNames = ["vehicleType", "locationId"];
  execute_v2_function(request, response, false, FlexDrive.Locations.V2.getRates, parameterNames, null, "v2_Vehicles_getRates");
});

Parse.Cloud.define('v2_Vehicles_retrieve', function(request, response) {
  var requiredParameterNames = ["VehicleID"];
  var vehicleId = request.params.VehicleID;
  var rentalCount = request.params.rentalCount;
  var parseUser = request.user;
  var parameters = [parseUser, vehicleId, rentalCount];
  execute_v2_function(request, response, false, FlexDrive.Vehicles.V2.retrieve, requiredParameterNames, parameters, "v2_Vehicles_retrieve");
});

Parse.Cloud.define('v2_Users_login', function(request, response) {
  var username = request.params.username;
  var password = request.params.password;
  var parameters = [username, password];
  var requiredParameterNames = ["username","password"];

  execute_v2_function(request, response, false, FlexDrive.Users.V2.login, requiredParameterNames, parameters, "v2_Users_login");
});

Parse.Cloud.define('v2_Users_updateEmail', function(request, response) {
  var user = request.user;
  var newEmail = request.params.newEmail;
  var parameters = [user, newEmail];

  var requiredParameterNames = ["newEmail"];
  execute_v2_function(request, response, true, FlexDrive.Users.V2.updateEmail, requiredParameterNames, parameters, "v2_Users_updateEmail");
});

Parse.Cloud.define('v2_Users_resetPassword', function(request, response) {
  var email = request.params.email;
  var parameters = [email];

  var requiredParameterNames = ["email"];
  execute_v2_function(request, response, false, FlexDrive.Users.V2.resetPassword, requiredParameterNames, parameters, "v2_Users_resetPassword");
});

Parse.Cloud.define("v2_Users_retrieveCompositeInfo", function(request, response) {
  var parseUser = request.user;
  var email = request.params.email;
  var parameters = [parseUser,email];
  execute_v2_function(request, response, true, FlexDrive.Users.V2.retrieveCompositeInfo, null, parameters, "v2_Users_retrieveCompositeInfo");
});

Parse.Cloud.define("v2_Users_retrieve", function(request, response) {
  var parseUser = request.user;
  var emailSnippet = request.params.emailSnippet;

  Parse.Cloud.useMasterKey();
  var requiredParameterNames = ["emailSnippet"];
  var parameters = [parseUser, emailSnippet];
  execute_v2_function(request, response, true, FlexDrive.Users.V2.retrieve, requiredParameterNames, parameters, "v2_Users_retrieve");
});

Parse.Cloud.define("v2_Users_create", function(request, response) {

  var paymentToken = request.params.paymentToken;
  var customerInfo = request.params.customerInfo;
  var parameters = [paymentToken, customerInfo];

  var missingParameterNames = [];
  if (!customerInfo) {
    missingParameterNames.push('customerInfo');
  } else {
    if (!customerInfo.lastName) {
      missingParameterNames.push('lastName')
    }
    if (!customerInfo.email) {
      missingParameterNames.push('email')
    }
    if (!customerInfo.marketId) {
      missingParameterNames.push('marketId')
    }
    if (!customerInfo.password) {
      missingParameterNames.push('password')
    }
    if (customerInfo.acceptedMembershipAgreement == null) {
      missingParameterNames.push('acceptedMembershipAgreement')
    }
  }
  if (missingParameterNames.length > 0) {
    var additionalInfo = "Missing Request Parameter(s): " + missingParameterNames.join(", " );
    var error = Error.invalidInputError()
    Error.addObjectToError(error, additionalInfo)
    return Log.logFlexDriveError(error, request.params, 'v2_Users_create', null, null, null, null, request.installationId).then(function() {
      response.success({
        data: null,
        error: error
      });
    });
  } else {
    var requiredParameterNames = ["paymentToken", "customerInfo"];
    execute_v2_function(request, response, false, FlexDrive.Users.V2.create, requiredParameterNames, parameters, "v2_Users_create");
  }
});

// Backwards compatible for 2.0.x
Parse.Cloud.define('v2_Reservations_cancel', function(request, response) {
  var reservationId = request.params.reservationId;
  var parseUser = request.user;
  var parameters = [parseUser, reservationId];
  var requiredParameterNames = ["reservationId"];

  execute_v2_function(request, response, true, FlexDrive.Reservations.V2.delete, requiredParameterNames, parameters, "v2_Reservations_cancel");
});

// Really cancel
Parse.Cloud.define('v2_Reservations_cancel2', function(request, response) {
  var reservationId = request.params.reservationId;
  var parseUser = request.user;
  var parameters = [parseUser, reservationId];
  var requiredParameterNames = ["reservationId"];

  execute_v2_function(request, response, true, FlexDrive.Reservations.V2.cancel, requiredParameterNames, parameters, "v2_Reservations_cancel2");
});

Parse.Cloud.define('v2_Reservations_delete', function(request, response) {
  var reservationId = request.params.reservationId;
  var parseUser = request.user;
  var parameters = [parseUser, reservationId];
  var requiredParameterNames = ["reservationId"];

  execute_v2_function(request, response, true, FlexDrive.Reservations.V2.delete, requiredParameterNames, parameters, "v2_Reservations_delete");
});

Parse.Cloud.define('v2_Rentals_willNotRenew', function(request, response) {
  var requiredParameterNames = ["rentalId"];
  execute_v2_function(request, response, true, FlexDrive.Rentals.V2.willNotRenew, requiredParameterNames, null, "v2_Rentals_willNotRenew");
});

Parse.Cloud.define('v2_Notifications_retrieve', function(request, response) {
  var parseUser = request.user;
  var pagingLastNotificationCreatedAt = request.params.pagingLastNotificationCreatedAt;
  var pageSize = request.params.pageSize;
  var parameters = [parseUser, pagingLastNotificationCreatedAt, pageSize];

  execute_v2_function(request, response, true, Notifications.V2.retrieve, ["pageSize"], parameters, "v2_Notifications_retrieve");
});

Parse.Cloud.define('v2_Notifications_delete', function(request, response) {
  var parseUser = request.user;
  var notificationId = request.params.notificationId;
  var parameters = [parseUser, notificationId];

  execute_v2_function(request, response, true, Notifications.V2.delete, ["notificationId"], parameters, "v2_Notifications_delete");
});

Parse.Cloud.define('v2_Notifications_getUnreadCount', function(request, response) {
  var parseUser = request.user;
  var parameters = [parseUser];

  execute_v2_function(request, response, true, Notifications.V2.getUnreadCount, null, parameters, "v2_Notifications_getUnreadCount");
});

Parse.Cloud.define('v2_Reservations_createRenewal', function(request, response) {
  var rentalId = request.params.rentalId;
  var rateId = request.params.rateId;
  var rateVehicleTypesId = request.params.rateVehicleTypesId;
  var days = request.params.days;
  var payNow = request.params.payNow;
  var expired = request.params.expired;
  var password = request.params.password;
  var user = request.user;
  var parameters = [user, rentalId, rateId, rateVehicleTypesId, days, payNow, expired, password];

  var requiredParameterNames = ["rentalId", "rateId", "rateVehicleTypesId", "days"];
  execute_v2_function(request, response, true, FlexDrive.Reservations.V2.createRenewal, requiredParameterNames, parameters, "v2_Reservations_createRenewal");
});

Parse.Cloud.define('v2_Reservations_create', function(request, response) {

  var locationId = request.params.locationId;
  var offsiteLocationId = request.params.offsiteLocationId;
  var assignedVehicleId = request.params.assignedVehicleId;
  var vehicleId = request.params.vehicleId;
  var rateId = request.params.rateId;
  var rateVehicleTypesId = request.params.rateVehicleTypesId;
  var reservationType = request.params.reservationType;
  var days = request.params.days;
  var expectedPickup = request.params.expectedPickup;
  var payNow = request.params.payNow;
  var password = request.params.password;
  var user = request.user;
  var parameters = [user, locationId, offsiteLocationId, assignedVehicleId, vehicleId, rateId, rateVehicleTypesId, reservationType, days, expectedPickup, payNow, password];

  var requiredParameterNames = ["locationId", "assignedVehicleId", "vehicleId", "rateId", "rateVehicleTypesId", "reservationType", "days", "expectedPickup", "payNow"];
  execute_v2_function(request, response, true, FlexDrive.Reservations.V2.create, requiredParameterNames, parameters, "v2_Reservations_create");
});

Parse.Cloud.define('v2_Reservations_update', function(request, response) {
  var reservationId = request.params.reservationId;
  var rentalId = request.params.rentalId;
  var rateId = request.params.rateId;
  var rateVehicleTypesId = request.params.rateVehicleTypesId;
  var start = request.params.start;
  var days = request.params.days;
  var payNow = request.params.payNow;
  var expired = request.params.expired;
  var password = request.params.password;
  var user = request.user;
  var parameters = [user, reservationId, rentalId, rateId, rateVehicleTypesId, days, start, payNow, expired, password];

  var requiredParameterNames = ["reservationId", "rateId", "rateVehicleTypesId", "days", "start", "payNow"];
  execute_v2_function(request, response, true, FlexDrive.Reservations.V2.update, requiredParameterNames, parameters, "v2_Reservations_update");
});

Parse.Cloud.define('v2_Reservations_convertToRental', function(request, response) {
  var reservationId = request.params.reservationId;
  var start = request.params.start;
  var user = request.user;
  var parameters = [user, reservationId, start];

  var requiredParameterNames = ["reservationId", "start"];
  execute_v2_function(request, response, true, FlexDrive.Reservations.V2.convertToRental, requiredParameterNames, parameters, "v2_Reservations_convertToRental");
});

Parse.Cloud.define('v2_Payments_listCards', function(request, response) {
  var user = request.user;
  execute_v2_function(request, response, true, FlexdriveStripe.Payments.V2.listCards, null, [user], "v2_Payments_listCards");
});

Parse.Cloud.define('v2_Payments_createCard', function(request, response) {
  var user = request.user;
  var token = request.params.token;
  var makeDefault = request.params.makeDefault;
  var existingFingerprints = request.params.existingFingerprints;

  var parameters = [user, token, makeDefault, existingFingerprints];
  var requiredParameters = ["token", "makeDefault", "existingFingerprints"];

  execute_v2_function(request, response, true, FlexdriveStripe.Payments.V2.createCard, requiredParameters, parameters, "v2_Payments_createCard");
});

Parse.Cloud.define('v2_Payments_updateCard', function(request, response) {
  var user = request.user;
  var cardId = request.params.cardId;
  var expMonth = request.params.expMonth;
  var expYear = request.params.expYear;
  var zip = request.params.zip;

  var parameters = [user,cardId,expMonth,expYear,zip];
  var requiredParameters = ["cardId","expMonth","expYear","zip"];

  execute_v2_function(request, response, true, FlexdriveStripe.Payments.V2.updateCard, requiredParameters, parameters, "v2_Payments_updateCard");
});

Parse.Cloud.define('v2_Payments_setDefaultCard', function(request, response) {
  var user = request.user;
  var cardId = request.params.cardId;
  var parameters = [user, cardId];
  var requiredParameters = ["cardId"]

  execute_v2_function(request, response, true, FlexdriveStripe.Payments.V2.updateDefaultCard, requiredParameters, parameters, "v2_Payments_setDefaultCard");
});

Parse.Cloud.define('v2_Payments_deleteCard', function(request, response) {
  var user = request.user;
  var cardId = request.params.cardId;
  var parameters = [user, cardId];
  var requiredParameters = ["cardId"]

  execute_v2_function(request, response, true, FlexdriveStripe.Payments.V2.deleteCard, requiredParameters, parameters, "v2_Payments_deleteCard");
});

Parse.Cloud.define('v2_Vehicles_list', function(request, response) {  var user = request.user;
  var marketId = request.params.marketId;
  var index = request.params.index;
  var size = request.params.size;

  var parameters = [user, marketId, index, size];
  var requiredParameters = ["marketId","index","size"];

  execute_v2_function(request, response, false, FlexDrive.Vehicles.V2.list, requiredParameters, parameters, "v2_Vehicles_list");
});

Parse.Cloud.define('v2_Markets_nextBusinessDay', function(request, response) {  var user = request.user;
  var marketId = request.params.marketId;

  var parameters = [marketId];
  var requiredParameters = ["marketId"];

  execute_v2_function(request, response, false, FlexDrive.Markets.V2.nextBusinessDay, requiredParameters, parameters, "v2_Markets_nextBusinessDay");
});

Parse.Cloud.define('v2_Markets_list', function(request, response) {
  var parameters = [];
  var requiredParameters = [];

  execute_v2_function(request, response, false, FlexDrive.Markets.V2.list, requiredParameters, parameters, "v2_Markets_list");
});

Parse.Cloud.define('v2_Update_required', function(request, response) {
  var parseUser = request.user; // Only used for error logging.  A null user is ok.
  var version = request.params.version;
  var clientType = request.params.clientType;

  var clientTypeLower;
  if (clientType) {
    clientTypeLower = clientType.toLowerCase();
  }

  var isValidVersion = version && Utils.isValidVersion(version)
  var isValidClientType = clientTypeLower && (clientTypeLower == "ios" || clientTypeLower == "android" || clientTypeLower == "ipad")

  if (!isValidVersion) {
    var error = Error.invalidInputError();
    Error.addObjectToError(error, "The version parameter is required and must have the format x.x.x");
    return Log.logFlexDriveError(error, request.params, "v2_Update_required", null, null, null, parseUser, request.installationId).then(function() {
      response.success({
        data: null,
        error: error ? error : null
      });
    });
  }

  if (!isValidClientType) {
    var error = Error.invalidInputError();
    Error.addObjectToError(error,"The clientType parameter must be Android, iOS or iPad.  The parameter is not case sensitive.");
    return Log.logFlexDriveError(error, request.params, "v2_Update_required", null, null, null, parseUser, request.installationId).then(function() {
      response.success({
        data: null,
        error: error ? error : null
      });
    });
  }

  var parameters = [version, clientTypeLower];
  execute_v2_function(request, response, false, FlexDrive.Upgrades.getLatestUpgrade, null, parameters, "v2_Update_required");
});

/** This method will execute initialFunc with the properties extracted from the request object or provided in the functionParameters.
 *  request: functionRequest - The function request which caused this function to be called.
 *  response: functionResponse - The response which will be returned after initialFunc is finished executing.
 *  shouldRequireUser: Should the user's authorization be validated befor calling initialfunc.  If not, the masterKey will be used.
 *  initialFunc: The function to call.
 *  requiredParameterNames: The names of the parameters which are required to be provided by request.  If functionParameters is null, request parameters with the given names will be provided to initialFunc.
 *  functionParameters: The parameters to be provided to initialFunc.  If this property is null, parameters will be set based on requiredParameterNames.
 *  functionName: Name of the Cloud function.  Optional and only used for logging.
 */
function execute_v2_function(request, response, shouldRequireUser, initialFunc, requiredParameterNames, functionParameters, functionName) {
  try {
    var parseUser = request.user;
    console.log("Parse User: " + JSON.stringify(parseUser));
    console.log("Request: "+ JSON.stringify(request));
    if (shouldRequireUser) {
      if (!parseUser) {
        response.success({
          data: null,
          error: Error.unauthorizedUserError()
        });
        return;
      }
    } else {
      Parse.Cloud.useMasterKey();
    }

    var parameters = []
    var missingParameterNames = [];

    if (requiredParameterNames) {
      requiredParameterNames.forEach(function(parameterName) {
        var parameter = request.params[parameterName];

        if (parameter != null) {
          parameters.push(parameter);
        } else {
          missingParameterNames.push(parameterName);
        }
      });
    }

    if (functionParameters) {
      parameters = functionParameters;
    }

    if (missingParameterNames.length > 0) {
      var additionalInfo = "Missing Request Parameter(s): " + missingParameterNames.join(", " );
      var error = Error.invalidInputError()
      Error.addObjectToError(error, additionalInfo)
      return Log.logFlexDriveError(error, request.params, functionName, null, null, null, parseUser, request.installationId).then(function() {
        response.success({
          data: null,
          error: error
        });
      });
    }

    // Information about apply: http://hangar.runway7.net/javascript/difference-call-apply
    initialFunc.apply(null, parameters ? parameters : []).then(function(responseObject) {
      response.success({
        data: responseObject == null ? null : responseObject,
        error: null
      });
    }, function(err) {
      var error = Error.systemErrorForError(err);
      return Log.logFlexDriveError(error, request.params, functionName, null, null, null, parseUser, request.installationId).then(function() {
        response.success({
          data: null,
          error: error ? error : null
        });
      });
    });
  } catch(err) {
    var error = Error.systemErrorForError(err);
    return Log.logFlexDriveError(error, request.params, functionName, null ,null, null, parseUser, request.installationId).then(function() {
      response.success({
        data: null,
        error: error ? error : null
      });
    });
  }
};
