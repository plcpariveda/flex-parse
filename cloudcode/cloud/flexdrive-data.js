/**
* FlexDrive Data Access
* Parse Cloud Module
* 1.0.0
*/
// NOTE: !Related objects are not loaded when querying. Need to call fetch() on all related objects before using!
// NOTE: All queries default to 100 results. Must set .limit to retrieve more results.

// Imports:
var Environment = require('./flexdrive-environment.js');

var RentCentric = require("./rentcentric.js");
var RentCentricEnvironment = Environment.RentCentric.getEnvironment();
RentCentric.initialize(RentCentricEnvironment.url, RentCentricEnvironment.clientID, RentCentricEnvironment.serviceUserName, RentCentricEnvironment.servicePassword);

var Utils = require('./flexdrive-utils.js');
var _ = require('underscore');
var Error = require('./flexdrive-error.js');

// Values:

// Generic property keys
var createdAtKey = 'createdAt';
var increment = 'Increment';

// Drivers License Validation Statuses
var driversLicenseValidationPendingValue = 'pending';
var driversLicenseValidationPassValue = 'pass';

// Notification Types
var swapRequestNotificationType = 'SwapRequest';
var swapRequestResponseNotificationType = 'SwapRequest-Response';
var timeToRenewNotificationType = 'TimeToRenewRental';
var successfulPaymentNotificationType = 'SuccessfulPayment';
var paymentFailureNotificationType = 'PaymentFailure';
var membershipApprovalNotificationType = 'DriversLicense-Approved';

// Copy
var notificationSwapRequestCopy = function(senderUsername) { return 'New swap request from ' + senderUsername };
var timeToRenewNotificationCopy = 'Your current subscription is now eligible to be renewed.';
var membershipApprovalNotificationCopy = 'Driver’s info and payment information verified. Your Flexdrive membership has been approved.';

var timeToRenewNotificationSubject = 'Time To Renew';
var successfulPaymentNotificationSubject = 'Payment Successful';
var paymentFailureNotificationSubject = 'Payment Failed';
var membershipApprovalNotificationSubject = "Driver\'s License Approved"

// TODO: Make sure this accepted copy makes it to the app alert when responder taps accept.
var notificationSwapRequestAcceptedCopy = function(responderUsername) {return responderUsername + ' accepted your swap request. Flexdrive will message you soon to schedule your swap.'}
var notificationSwapRequestRejectedCopy = function(responderUsername) {return responderUsername + ' rejected your swap request. Explore other Flexdrive inventory to search for another match.'}
// Errors
var userCouldNotFindUserWithRentCentricCustomerIdErrorCopy = function(customerId) {return 'Could not find user with customer ID ' + customerId};
var swapRequestCouldNotFindWithIdErrorCopy = function(swapId) {return 'Could not find swap request with ID ' + swapId};

// Cloud Module Public API:

var EmailTemplate = {
  keys: {
    firstName : '{%firstname%}',
    adminEmail : '{%adminemail%}'
  }
};

var Users = {
  keys: {
    driversLicenseValidationStatus: 'licenseCheckStatus',
    profileImageFile: 'profileImageFile',
    rentCentricCustomerId: 'customerIDRentCentric',
    stripeCustomerId: 'customerIDStripe',
    email: 'email',
    username: 'username',
    validationPending: driversLicenseValidationPendingValue,
    validationPass: driversLicenseValidationPassValue
  },
  retrieveWithRentCentricCustomerId: function(rentCentricCustomerId, options) {
    Parse.Cloud.useMasterKey();
    var promise = new Parse.Promise();
    var query = new Parse.Query(Parse.User);
    query.equalTo(this.keys.rentCentricCustomerId, rentCentricCustomerId);
    query.first().then(function(user) {
      if (user) {
        Utils.reportSuccess(user, options, promise);
      } else {
        Utils.reportFailure(userCouldNotFindUserWithRentCentricCustomerIdErrorCopy(rentCentricCustomerId), options, promise);
      }
    }, function(error) {
      Utils.reportFailure(error, options, promise);
    });
    return promise;
  },
  v2_retrieveWithRentCentricCustomerId: function(rentCentricCustomerId) {
    Parse.Cloud.useMasterKey();

    var query = new Parse.Query(Parse.User);
    query.equalTo(this.keys.rentCentricCustomerId, rentCentricCustomerId);
    return query.first().then(function(user) {
      if (user) {
        return Parse.Promise.as(user)
      } else {
        return Parse.Promise.error(Error.unknownRentCentricCustomerIdError());
      }
    }, function(error) {
      return Parse.Promise.error(Error.unknownRentCentricCustomerIdError());
    });
  },
  retrieveWithUsername: function(username, options) {
    Parse.Cloud.useMasterKey();
    var promise = new Parse.Promise();
    var query = new Parse.Query(Parse.User);
    query.equalTo(this.keys.username, username);
    query.first().then(function(user) {
      Utils.reportSuccess(user, options, promise);
    }, function(error) {
      Utils.reportFailure(error, options, promise);
    });
    return promise;
  },
  retrieveWithEmail: function(email, options) {
    Parse.Cloud.useMasterKey();
    var promise = new Parse.Promise();
    var query = new Parse.Query(Parse.User);
    query.equalTo(this.keys.email, email);
    query.first().then(function(user) {
      Utils.reportSuccess(user, options, promise);
    }, function(error) {
      Utils.reportFailure(error, options, promise);
    });
    return promise;
  },
  retrieveProfileImageUrl: function(user) {
    var userProfileImage = user.get(this.keys.profileImageFile);
    var profileImageUrl = '';
    if (userProfileImage) {
      profileImageUrl = userProfileImage.url();
    }
    return profileImageUrl;
  },
  v2_markHasValidDriversLicense: function(user, value) {
    if (value === undefined || value === null || (typeof value !== 'boolean')) {
      return Parse.Promise.error(Error.invalidInputError());
    }

    var driversLicenseValidationValue;
    if (value) {
      driversLicenseValidationValue = driversLicenseValidationPassValue;
    } else {
      driversLicenseValidationValue = driversLicenseValidationPendingValue;
    }
    user.set(this.keys.driversLicenseValidationStatus, driversLicenseValidationValue);
    return user.save().then(function(savedUser) {
      return Parse.Promise.as(savedUser);
    }, function(error) {
      return Parse.Promise.error(Error.systemError());
    });
  }
};

var ReservationRequests = {
  className: 'ReservationRequest',
  keys: {
    user: 'user',
    vehicleId: 'vehicleId'
  },
  create: function(data, options) {
    // TODO: Validate input.
    Parse.Cloud.useMasterKey();
    var acl = new Parse.ACL(data.user);
    var promise = new Parse.Promise();
    var reservationRequest = new Parse.Object(this.className);
    reservationRequest.setACL(acl);
    reservationRequest.set(this.keys.user, data.user);
    reservationRequest.set(this.keys.vehicleId, data.vehicleId);
    reservationRequest.save().then(function(createdReservationRequest) {
      Utils.reportSuccess(createdReservationRequest, options, promise);
    }, function(error) {
      Utils.reportFailure(error, options, promise);
    });
    return promise;
  },
  fetchRelatedObjects: function(reservationRequest, options) {
    var promise = new Parse.Promise();
    reservationRequest.get(this.keys.user).fetch().then(function() {
      Utils.reportSuccess(null, options, promise);
    }, function(error) {
      Utils.reportFailure(error, options, promise);
    });
    return promise;
  },
  retrieve: function(user, vehicleId) {
    var query = new Parse.Query(this.className);
    query.equalTo(this.keys.user, user);
    query.equalTo(this.keys.vehicleId, vehicleId);
    return query.first();
  },
  list: function() {
    Parse.Cloud.useMasterKey();
    var query = new Parse.Query(this.className);
    return query.find();
  }
};

var Notifications = {
  className: 'Notification',
  keys: {
    alert: 'alert',
    badge: 'badge',
    type: 'type',
    vehicle: 'vehicle',
    rentalId: 'rentalId',
    reservationId: 'reservationId',
    token: 'token',
    message: 'message',
    subject: 'subject',
    user: 'user',
    confirmation: 'confirmation',
    emailSent: 'emailSent',
    pushSent: 'pushSent',
    rentCentricCustomerId: 'rentCentricCustomerId',
    createdDate: 'createdDate'
  },
  isPaymentFailureNotification: function(notification) {
    var notificationType = notification.get(this.keys.type);
    var isPaymentFailureNotification = false
    if (notificationType && notificationType == this.notificationTypeForPaymentFailure()) {
      isPaymentFailureNotification = true
    }
    return isPaymentFailureNotification;
  },
  emailSent: function(notification) {
    var emailSent = notification.get(this.keys.emailSent);
    if (emailSent && emailSent == true) {
      return true;
    } else {
      return false;
    }
  },
  pushSent: function(notification) {
    var pushSent = notification.get(this.keys.pushSent);
    if (pushSent && pushSent == true) {
      return true;
    } else {
      return false;
    }
  },
  notificationTypeForSuccessfulPayment: function() {
    return successfulPaymentNotificationType;
  },
  notificationTypeForPaymentFailure: function() {
    return paymentFailureNotificationType;
  },
  retrievePreviousPaymentNotificationForUser: function(user, reservationId, notificationType) {
    var query = new Parse.Query(this.className);
    query.equalTo(this.keys.user, user);
    query.equalTo(this.keys.reservationId, reservationId);
    query.equalTo(this.keys.type, notificationType);
    return query.first();
  },
  retrievePreviousTimeToRenewNotificationForUser: function(user, rentalId) {
    var query = new Parse.Query(this.className);
    query.equalTo(this.keys.user, user);
    query.equalTo(this.keys.rentalId, rentalId);
    query.equalTo(this.keys.type, timeToRenewNotificationType);
    return query.first();
  },
  createForSuccessfulPayment: function(user, reservationId, startDate, endDate) {
    var alert = 'Your payment for ' + notificationStringForDate(startDate) + ' - ' + notificationStringForDate(endDate) + ' has  been received. Thank you.';

    var notification = new Parse.Object(this.className);
    notification.set(this.keys.alert, alert);
    notification.set(this.keys.badge, increment);
    notification.set(this.keys.type, successfulPaymentNotificationType);
    notification.set(this.keys.reservationId, reservationId);
    notification.set(this.keys.user, user);
    notification.set(this.keys.subject, successfulPaymentNotificationSubject);
    notification.set(this.keys.rentCentricCustomerId, user.get("customerIDRentCentric"));
    notification.set(this.keys.createdDate, new Date());
    return notification;
  },
  createForPaymentFailure: function(user, reservationId, startDate, endDate) {
    var alert = 'An error has occurred with your payment for ' + notificationStringForDate(startDate) + ' - ' + notificationStringForDate(endDate) + '. You can update your payment info in the app to resolve this issue.';
    var notification = new Parse.Object(this.className);
    notification.set(this.keys.alert, alert);
    notification.set(this.keys.badge, increment);
    notification.set(this.keys.type, paymentFailureNotificationType);
    notification.set(this.keys.reservationId, reservationId);
    notification.set(this.keys.user, user);
    notification.set(this.keys.subject, paymentFailureNotificationSubject);
    notification.set(this.keys.rentCentricCustomerId, user.get("customerIDRentCentric"));
    notification.set(this.keys.createdDate, new Date());

    return notification;
  },
  createForTimeToRenew: function(user, rentalId) {
    var notification = new Parse.Object(this.className);
    notification.set(this.keys.alert, timeToRenewNotificationCopy);
    notification.set(this.keys.badge, increment);
    notification.set(this.keys.type, timeToRenewNotificationType);
    notification.set(this.keys.rentalId, rentalId);
    notification.set(this.keys.user, user);
    notification.set(this.keys.subject, timeToRenewNotificationSubject);
    notification.set(this.keys.rentCentricCustomerId, user.get("customerIDRentCentric"));
    notification.set(this.keys.createdDate, new Date());

    return notification;
  },
  createForMembershipApproved: function(user) {
    var notification = new Parse.Object(this.className);
    notification.set(this.keys.alert, membershipApprovalNotificationCopy);
    notification.set(this.keys.badge, increment);
    notification.set(this.keys.type, membershipApprovalNotificationType);
    notification.set(this.keys.user, user);
    notification.set(this.keys.subject, membershipApprovalNotificationSubject);
    notification.set(this.keys.rentCentricCustomerId, user.get("customerIDRentCentric"));
    notification.set(this.keys.createdDate, new Date());

    return notification;
  },
  createForSwap: function(swapRequest, options) {
    Parse.Cloud.useMasterKey();
    var promise = new Parse.Promise();
    // Set up data.
    var alert;
    var type;
    var vehicle = swapRequest.get(SwapRequests.keys.senderVehicleId);
    var token = swapRequest.id;
    var message;
    var user;
    var confirmation; // TODO: Does this value really need to be stored? How does the client use this value?
    // Set up data based on response.
    var response = swapRequest.get(SwapRequests.keys.confirmed);
    if (response == undefined) {// No response yet.
      var senderUsername = swapRequest.get(SwapRequests.keys.sender).getUsername();
      alert = notificationSwapRequestCopy(senderUsername);
      message = swapRequest.get(SwapRequests.keys.message);
      type = swapRequestNotificationType;
      user = swapRequest.get(SwapRequests.keys.receiver);
    } else {
      var receiverUsername = swapRequest.get(SwapRequests.keys.receiver).getUsername();
      if (response == true) {// Swap request accepted.
        alert = notificationSwapRequestAcceptedCopy(receiverUsername);
        confirmation = true;
      } else {// Swap request rejected.
        alert = notificationSwapRequestRejectedCopy(receiverUsername);
        confirmation = false;
      }
      type = swapRequestResponseNotificationType;
      user = swapRequest.get(SwapRequests.keys.sender);
    }
    // Create Notification object.
    var acl = new Parse.ACL(user);
    var notification = new Parse.Object(this.className);
    notification.setACL(acl);
    notification.set(this.keys.alert, alert);
    notification.set(this.keys.type, type);
    notification.set(this.keys.vehicle, vehicle);
    notification.set(this.keys.token, token);
    notification.set(this.keys.message, message);
    notification.set(this.keys.user, user);
    notification.set(this.keys.confirmation, confirmation);
    notification.save().then(function(createdNotification) {
      Utils.reportSuccess(createdNotification, options, promise);
    }, function(error) {
      Utils.reportFailure(error, options, promise);
    });
    return promise;
  }
};

var Upgrades = {
  className: 'Upgrade',
  keys: {
    clientType: 'clientType',
    version: 'version',
    timeToForce: 'timeToForce',
    description: 'description',
    needsUpgrade: 'needsUpgrade',
    upgradeURL: 'upgradeURL'
  },
  retrieveLastUpgrade: function(version, clientType) {
    Parse.Cloud.useMasterKey();
    var query = new Parse.Query(this.className);
    query.equalTo("clientType", clientType);
    query.descending('timeToForce');
    return query.first().then(function(upgrade) {
      return Parse.Promise.as(upgrade);
    }, function(error) {
      return Parse.Promise.error(Error.systemError());
    });
  }
};

var Billing = {
  className: 'Billing',
  keys: {
    pending: 'pending',
    processing: 'startProcessing',
  },
  compositeKeys: {
    PaymentInProgress: 'PaymentInProgress',
    PaymentProblem: 'PaymentProblem'
  },
};

var SwapRequests = {
  className: 'SwapRequest',
  keys: {
    active: 'active',
    confirmed: 'confirmed',
    sender: 'sender',
    receiver: 'receiver',
    senderVehicleId: 'senderVehicleID',
    receiverVehicleId: 'desiredVehicleID',
    message: 'senderMessage',
    respondedAt: 'respondedAt'
  },
  create: function(data, options) {
    var promise = new Parse.Promise();
    // TODO: Validate input.
    var swapRequest = new Parse.Object(this.className);
    swapRequest.setACL(SwapRequests_Internal.createACL(data.sender, data.receiver));
    swapRequest.set(this.keys.active, true);
    swapRequest.set(this.keys.sender, data.sender);
    swapRequest.set(this.keys.senderVehicleId, data.senderVehicleId);
    swapRequest.set(this.keys.receiver, data.receiver);
    swapRequest.set(this.keys.receiverVehicleId, data.receiverVehicleId);
    swapRequest.set(this.keys.message, data.message); // TODO: Test if message is undefined/null not passed in.
    swapRequest.save().then(function(createdSwapRequest) {
      Utils.reportSuccess(createdSwapRequest, options, promise);
    }, function(error) {
      Utils.reportFailure(error, options, promise);
    });
    return promise;
  },
  retreive: function(swapId, options) {
    Parse.Cloud.useMasterKey();
    var promise = new Parse.Promise();
    var query = new Parse.Query(this.className);
    var self = this;
    query.get(swapId).then(function(swapRequest) {
      if (swapRequest) {
        self.fetchRelatedObjects(swapRequest).always(function() {// TODO: Should this fail out if a relation fetch fails?
          Utils.reportSuccess(swapRequest, options, promise);
        });
      } else {
        Utils.reportFailure(swapRequestCouldNotFindWithIdErrorCopy(swapId), options, promise);
      }
    }, function(error) {
      Utils.reportFailure(error, options, promise);
    });
    return promise;
  },
  updateWithResponse: function(swapRequest, response, options) {
    var promise = new Parse.Promise();
    swapRequest.set(SwapRequests.keys.confirmed, Boolean(response)); // TODO: Make response already be a Boolean.
    swapRequest.set(this.keys.respondedAt, new Date());
    swapRequest.set(this.keys.active, false); // No longer active once someone has responded.
    swapRequest.save().then(function(savedSwapRequest) {
      Utils.reportSuccess(savedSwapRequest, options, promise);
    }, function(error) {
      Utils.reportFailure(error, options, promise);
    });
    return promise;
  },
  userHasActiveRequestForVehicle: function(user, vehicleId, options) {
    var promise = new Parse.Promise();
    var query = new Parse.Query(this.className);
    query.equalTo(this.keys.active, true);
    query.equalTo(this.keys.sender, user);
    query.equalTo(this.keys.receiverVehicleId, vehicleId);
    query.find().then(function(swapRequests) {
      var hasActiveRequest = false;
      if (swapRequests.length > 0) {
        hasActiveRequest = true;
      }
      Utils.reportSuccess(hasActiveRequest, options, promise);
    }, function(error) {
      Utils.reportFailure(error, options, promise);
    });
    return promise;
  },
  userHasActiveRequest: function(user, options) {
    var promise = new Parse.Promise();
    var query = new Parse.Query(this.className);
    query.equalTo(this.keys.active, true);
    query.equalTo(this.keys.sender, user);
    query.find().then(function(swapRequests) {
      var hasActiveRequest = false;
      if (swapRequests.length > 0) {
        hasActiveRequest = true;
      }
      Utils.reportSuccess(hasActiveRequest, options, promise);
    }, function(error) {
      Utils.reportFailure(error, options, promise);
    });
    return promise;
  },
  listOldActive: function(options) {
    var promise = new Parse.Promise();
    var date = new Date();
    date.setDate(date.getDate() - 3);
    var query = new Parse.Query(this.className);
    query.equalTo(this.keys.active, true);
    query.lessThan(createdAtKey, date);
    query.find().then(function(activeSwapRequests) {
      Utils.reportSuccess(activeSwapRequests, options, promise);
    }, function(error) {
      Utils.reportFailure(error, options, promise);
    });
    return promise;
  },
  deactivate: function(swapRequest, options) {
    var promise = new Parse.Promise();
    swapRequest.set(this.keys.active, false);
    swapRequest.save().then(function(savedSwapRequest) {
      Utils.reportSuccess(savedSwapRequest, options, promise);
    }, function(error) {
      Utils.reportFailure(error, options, promise);
    });
    return promise;
  },
  fetchRelatedObjects: function(swapRequest, options) {
    var promise = new Parse.Promise();
    var relatedFetchPromises = [];
    relatedFetchPromises.push(swapRequest.get(this.keys.sender).fetch());
    relatedFetchPromises.push(swapRequest.get(this.keys.receiver).fetch());
    Parse.Promise.when(relatedFetchPromises).then(function() {
      Utils.reportSuccess(null, options, promise);
    }, function(error) {
      Utils.reportFailure(error, options, promise);
    });
    return promise;
  }
};

var Installations = {
  keys: {
    channels: 'channels'
  }
};

var Markets = {
  combineMarketWithVehicles: function(marketId, vehicles){
    var promise = new Parse.Promise();
    if(typeof marketId === 'undefined'){
      Utils.reportSuccess(formattedMarkets, object, promise);
    }
    else if(typeof vehicles !== 'object'){
      Utils.reportFailure('Unable to append market because passed parameter is not an object.', object, promise);
    }
    else{
      var query = new Parse.Query('Markets');
      query.find().then(function(markets) {
        try{
          var returnObj = {
            Vehicles:vehicles
          };
          _.each(markets, function(m) {
            if(m.get('MarketID') == marketId){
              returnObj.Market = {MarketID:m.get('MarketID'), MarketName:m.get('MarketName')};
            }
          });
          Utils.reportSuccess(returnObj, vehicles, promise);
        }
        catch(e){
          Utils.reportFailure(e, vehicles, promise);
        }
      }, function(error) {
        Utils.reportFailure(error, vehicles, promise);
      });
    }
    return promise;
  },
  retrieveMarkets: function(markets){
    var promise = new Parse.Promise();
    if(!markets || !markets.length){
      Utils.reportFailure(error, markets, promise);
    }
    else{
      var query = new Parse.Query('Markets');
      query.find().then(function(parseMarkets) {
        var formattedMarkets = [];
        try{
          _.each(markets, function(m) {
            _.each(parseMarkets, function(pM) {
              if(pM.get('MarketID') == m.ID){
                formattedMarkets.push({MarketID:pM.get('MarketID'), MarketName:pM.get('MarketName')});
              }
            });
          });
          Utils.reportSuccess(formattedMarkets, markets, promise);
        }
        catch(e){
          Utils.reportFailure(e, markets, promise);
        }
      }, function(error) {
        Utils.reportFailure(error, markets, promise);
      });
    }
    return promise;
  },
  marketForID: function(marketId, options) {
    var query = new Parse.Query('Markets');
    query.equalTo('MarketID', marketId);
    return query.first().then(function(market) {
      if (market) {
        return Parse.Promise.as(market);
      } else {
        return Parse.Promise.as(null);
      }
    }).then(function(market) {
      var marketMap = null;
      if (market) {
        marketMap = {
          MarketID: market.get('MarketID'),
          MarketName: market.get('MarketName')
        };
      }
      return Parse.Promise.as(marketMap);
    });
  },
  marketNameForID: function(marketId) {
    if (marketId == "116") {
      return "Atlanta"
    } else if (marketId == "119") {
      return "Dallas"
    } else if (marketId == "120") {
      return "Nashville"
    } else if (marketId == "121") {
      return "Austin"
    } else {
      return "Unknown";
    }
  },
  marketForLocationName: function(locationName, options) {
    var query = new Parse.Query('Markets');
    query.equalTo('LocationName', locationName.toLowerCase());
    return query.first().then(function(market) {
      var marketMap = null;
      if (market) {
        marketMap = {
          MarketID: market.get('MarketID'),
          MarketName: market.get('MarketName')
        };
      }
      return Parse.Promise.as(marketMap);
    });
  },
  getMembershipFees: function(market) {
    var miscCharges = market.get('miscCharges');
    var membershipFees = [];
    for (var i = 0; i < miscCharges.length; ++i) {
      if (miscCharges[i].Group == "Membership") {
        membershipFees.push({id:miscCharges[i].MiscChargeID,name:miscCharges[i].MiscChargeName,amount:miscCharges[i].MiscChargeRate,type:miscCharges[i].Group});
      }
    }
    return membershipFees;
  },
  getDeposits: function(market,vehicleType) {
    var miscCharges = market.get('miscCharges');
    var deposits = [];
    for (var i = 0; i < miscCharges.length; ++i) {
      if (miscCharges[i].Group == "Deposit") {
        for (var j = 0; j < miscCharges[i].VehicleTypes.length; ++j) {
          if (miscCharges[i].VehicleTypes[j].VehicleTypeName == vehicleType) {
            deposits.push({id:miscCharges[i].MiscChargeID,name:miscCharges[i].MiscChargeName,amount:miscCharges[i].MiscChargeRate,type:miscCharges[i].Group});
          }
        }
      }
    }
    return deposits;
  },
  getLateFees: function(market) {
    var miscCharges = market.get('miscCharges');
    var lateFees = [];
    for (var i = 0; i < miscCharges.length; ++i) {
      if (miscCharges[i].Group == "Late") {
        lateFees.push({id:miscCharges[i].MiscChargeID,name:miscCharges[i].MiscChargeName,amount:miscCharges[i].MiscChargeRate,type:miscCharges[i].Group});
      }
    }
    return lateFees;
  },
};

module.exports = {
  /**
  * Get the version of the module.
  * @return {String}
  */
  version: '1.0.0',

  Users: Users,
  ReservationRequests: ReservationRequests,
  Notifications: Notifications,
  SwapRequests: SwapRequests,
  Installations: Installations,
  Markets: Markets,
  Upgrades: Upgrades,
  EmailTemplate: EmailTemplate,
  Billing: Billing
};

// Private API:

function notificationStringForDate(date) {
  var month = date.getMonth() + 1;
  var day = date.getDate();

  return (month + "/" + day);
}

var SwapRequests_Internal = {
  createACL: function(sender, receiver) {
    var acl = new Parse.ACL();
    acl.setPublicReadAccess(false);
    acl.setPublicWriteAccess(false);
    acl.setReadAccess(sender, true);
    acl.setWriteAccess(sender, true);
    acl.setReadAccess(receiver, true);
    acl.setWriteAccess(receiver, true);
    return acl;
  }
};
