/**
* FlexDrive Environment
* Parse Cloud Module
* 1.0.0
*/

// Imports:

var _ = require('underscore');

// App Id:
var ParseLocalAppId = 'local';
var ParseDebugAppId = 'cYfrTVTgEGBkEuOjo65nuYpm3fPTBBIFiDwUqO5Q';
var ParseClientDebugAppId = 'YtMLuIFIiUuV2GbWrnViPTsPAc8l7kR8GwEaO7qX';
var ParseServerDebugAppId = 'NzPlIn6lHbPKcmvZkAdOCwpNP2Idoh6lwCdEKoIi';
var ParseTestAppId = 'UTzeIHK9NVWlpPZA6r5m7kz4WWOVQjLRjdNnD79o';
var ParsePerformanceAppId = 'WybZjCk1SZUKa0LKZLvWhucKCLOITv4ZolxQhxUk';
var ParseStagingAppId = 'T8GTAIk55xY8S8LZ7I4l0ui08BM7LXhOjyRuEx2J';
var ParseProductionAppId = 'D6i4bd9w2htvUtzvL4PQdBlVQlndyt334s2hoe8l';

var RentCentricEnvironments = {
  local: {
    url: 'https://www4.rentcentric.com/SelfServiceCarRentalServiceDev/api.svc',
    clientID: '6078',
    serviceUserName: 'manheim',
    servicePassword: 'd2]YellowBox',
    basicAuthUser: 'localuser',
    basicAuthPass: 'localpass',
    customerWebPage: 'https://www4.rentcentric.com/Client6078/CustomerMain.aspx?CustomerID=',
    reservationWebPage: 'https://www4.rentcentric.com/Client6078/ReservationMain.aspx?ReservationID='
  },
  serverdebug: {
    url: 'https://www4.rentcentric.com/SelfServiceCarRentalServiceDev/api.svc',
    clientID: '6078',
    serviceUserName: 'manheim',
    servicePassword: 'd2]YellowBox',
    basicAuthUser: 'serverdebuguser',
    basicAuthPass: 'serverdebugpass',
    customerWebPage: 'https://www4.rentcentric.com/Client6078/CustomerMain.aspx?CustomerID=',
    reservationWebPage: 'https://www4.rentcentric.com/Client6078/ReservationMain.aspx?ReservationID='
  },
  clientdebug: {
    url: 'https://www4.rentcentric.com/SelfServiceCarRentalServiceDev/api.svc',
    clientID: '6078',
    serviceUserName: 'manheim',
    servicePassword: 'd2]YellowBox',
    basicAuthUser: 'clientdebuguser',
    basicAuthPass: 'clientdebugpass',
    customerWebPage: 'https://www4.rentcentric.com/Client6078/CustomerMain.aspx?CustomerID=',
    reservationWebPage: 'https://www4.rentcentric.com/Client6078/ReservationMain.aspx?ReservationID='
  },
  debug: {
    url: 'https://www4.rentcentric.com/SelfServiceCarRentalServiceDev/api.svc',
    clientID: '6078',
    serviceUserName: 'manheim',
    servicePassword: 'd2]YellowBox',
    basicAuthUser: 'debuguser',
    basicAuthPass: '23mxH,N^',
    customerWebPage: 'https://www4.rentcentric.com/Client6078/CustomerMain.aspx?CustomerID=',
    reservationWebPage: 'https://www4.rentcentric.com/Client6078/ReservationMain.aspx?ReservationID='
  },
  test: {
    url: 'https://www4.rentcentric.com/SelfServiceCarRentalServiceTest/api.svc',
    clientID: '6040',
    serviceUserName: 'manheim',
    servicePassword: 't2]WhiteBox',
    basicAuthUser: 'testuser',
    basicAuthPass: 'ENUF}43s',
    customerWebPage: 'https://www4.rentcentric.com/Client6040/CustomerMain.aspx?CustomerID=',
    reservationWebPage: 'https://www4.rentcentric.com/Client6040/ReservationMain.aspx?ReservationID='
  },
  performance: {
    url: 'https://www4.rentcentric.com/SelfServiceCarRentalServicePerf/api.svc',
    clientID: '6119',
    serviceUserName: 'manheim',
    servicePassword: 'p2]GreyBox',
    basicAuthUser: 'perfuser',
    basicAuthPass: '9VZ7E{ep',
    customerWebPage: 'https://www4.rentcentric.com/Client6119/CustomerMain.aspx?CustomerID=',
    reservationWebPage: 'https://www4.rentcentric.com/Client6119/ReservationMain.aspx?ReservationID='
  },
  staging: {
    url: 'https://www4.rentcentric.com/SelfServiceCarRentalServiceStaging/api.svc',
    clientID: '6094',
    serviceUserName: 'manheim',
    servicePassword: 's2]GreenBox',
    basicAuthUser: 'staginguser',
    basicAuthPass: 'u4KpRV_K',
    customerWebPage: 'https://www4.rentcentric.com/Client6094/CustomerMain.aspx?CustomerID=',
    reservationWebPage: 'https://www4.rentcentric.com/Client6094/ReservationMain.aspx?ReservationID='
  },
  production: {
    url: 'https://www4.rentcentric.com/SelfServiceCarRentalService/api.svc',
    clientID: '5928',
    serviceUserName: 'manheim',
    servicePassword: 'p2]BlueBox',
    basicAuthUser: 'produser',
    basicAuthPass: 'R6reQUJh]jFh',
    customerWebPage: 'https://www4.rentcentric.com/Client5928/CustomerMain.aspx?CustomerID=',
    reservationWebPage: 'https://www4.rentcentric.com/Client5928/ReservationMain.aspx?ReservationID='
  }
};

var StripeEnvironments = {
  local: {
    secretKey: 'sk_test_YxVg1dJMJQv7jQ9RZdq2VtPr'
  },
  debug: {
    secretKey: 'sk_test_qc1ceQi5kEPi2lSPT7Y7c7z0'
  },
  serverdebug: {
    secretKey: 'sk_test_qc1ceQi5kEPi2lSPT7Y7c7z0'
  },
  clientdebug: {
    secretKey: 'sk_test_qc1ceQi5kEPi2lSPT7Y7c7z0'
  },
  test: {
    secretKey: 'sk_test_qc1ceQi5kEPi2lSPT7Y7c7z0'
  },
  performance: {
    secretKey: 'sk_test_qc1ceQi5kEPi2lSPT7Y7c7z0'
  },
  staging: {
    secretKey: 'sk_test_qc1ceQi5kEPi2lSPT7Y7c7z0'
  },
  production: {
    secretKey: 'sk_live_2biiDNaGQFCLxuepSkYUDT6P'
  }
};

var MailgunEnvironments = {
  local: {
    env: "local",
    domainName: 'sandboxde5c5630415e4f80914db6b0d4a9ff5e.mailgun.org',
    apiKey: 'key-134763e8f6b7cd86be3367125b677a53',
    flexDriveITEmail : "flexdriveIT@gmail.com",
    flexDriveAdminEmailAddress: 'flexdrive.dev+local@gmail.com',
    flexDriveBillingEmailAddress: 'flexdrive.dev+local@gmail.com'
  },
  debug: {
    env: "debug",
    domainName: 'sandbox32cc13d8f33945d59296c18bd80843f7.mailgun.org',
    apiKey: 'key-0d37fd38dfc1b9aff6e165ad61e54b0b',
    flexDriveITEmail : "flexdriveIT@gmail.com",
    flexDriveAdminEmailAddress: 'flexdrive.dev@gmail.com',
    flexDriveBillingEmailAddress: 'flexdrive.dev@gmail.com'
  },
  serverdebug: {
    env: "serverdebug",
    domainName: 'sandbox32cc13d8f33945d59296c18bd80843f7.mailgun.org',
    apiKey: 'key-0d37fd38dfc1b9aff6e165ad61e54b0b',
    flexDriveITEmail : "flexdriveIT@gmail.com",
    flexDriveAdminEmailAddress: 'test@sandbox32cc13d8f33945d59296c18bd80843f7.mailgun.org',
    flexDriveBillingEmailAddress: 'test@sandbox32cc13d8f33945d59296c18bd80843f7.mailgun.org'
  },
  clientdebug: {
    env: "clientdebug",
    domainName: 'sandbox32cc13d8f33945d59296c18bd80843f7.mailgun.org',
    apiKey: 'key-0d37fd38dfc1b9aff6e165ad61e54b0b',
    flexDriveITEmail : "flexdriveIT@gmail.com",
    flexDriveAdminEmailAddress: 'test@sandbox32cc13d8f33945d59296c18bd80843f7.mailgun.org',
    flexDriveBillingEmailAddress: 'test@sandbox32cc13d8f33945d59296c18bd80843f7.mailgun.org'
  },
  test: {
    env: "test",
    domainName: 'sandbox32cc13d8f33945d59296c18bd80843f7.mailgun.org',
    apiKey: 'key-0d37fd38dfc1b9aff6e165ad61e54b0b',
    flexDriveITEmail : "flexdriveIT@gmail.com",
    flexDriveAdminEmailAddress: 'flexdrive.test@gmail.com',
    flexDriveBillingEmailAddress: 'flexdrive.test@gmail.com'
  },
  performance: {
    env: "performance",
    domainName: 'sandbox32cc13d8f33945d59296c18bd80843f7.mailgun.org',
    apiKey: 'key-0d37fd38dfc1b9aff6e165ad61e54b0b',
    flexDriveITEmail : "flexdriveIT@gmail.com",
    flexDriveAdminEmailAddress: 'flexdrive.performance@gmail.com',
    flexDriveBillingEmailAddress: 'flexdrive.performance@gmail.com'
  },
  staging: {
    env: "staging",
    domainName: 'sandbox32cc13d8f33945d59296c18bd80843f7.mailgun.org',
    apiKey: 'key-0d37fd38dfc1b9aff6e165ad61e54b0b',
    flexDriveITEmail : "FlexdriveIT@gmail.com",
    flexDriveAdminEmailAddress: 'flexdrive.staging@gmail.com',
    flexDriveBillingEmailAddress: 'flexdrive.staging@gmail.com'
  },
  production: {
    // no env for production
    domainName: 'flexdrive.com',
    apiKey: 'key-6fe3a6a80f92e506ec6ef7b5102da80c',
    flexDriveITEmail : "flexdriveIT@gmail.com",
    flexDriveAdminEmailAddress: 'memberservices@flexdrive.com',
    flexDriveBillingEmailAddress: 'billing@flexdrive.com'
  }
};

var ParseEnvironments = {
  local: {
    env: "local",
    hostName: "localhost:1337"
  },
  debug: {
    env: "debug",
    hostName: 'flexdrivedev.parseapp.com'
  },
  serverdebug: {
    env: "serverdebug",
    hostName: 'flexdriveserverdev.parseapp.com'
  },
  clientdebug: {
    env: "clientdebug",
    hostName: 'flexdriveclientdebug.parseapp.com'
  },
  test: {
    env: "test",
    hostName: 'flexdrivetest.parseapp.com'
  },
  performance: {
    env: "performance",
    hostName: 'flexdriveperf.parseapp.com'
  },
  staging: {
    env: "staging",
    hostName: 'flexdrivestaging.parseapp.com'
  },
  production: {
    env: "production",
    hostName: 'flexdrive.parseapp.com'
  }
};

// Cloud Module Public API:

module.exports = {
  /**
  * Get the version of the module.
  * @return {String}
  */
  version: '1.0.0',

  getEnvName: function() {
    if(isLocalEnvironment()){
      return 'local';
    } else if (isDebugEnvironment()) {
      return 'debug';
    } else if (isServerDebugEnvironment()) {
      return 'serverdebug';
    } else if (isClientDebugEnvironment()) {
      return 'clientdebug';
    } else if (isTestEnvironment()) {
      return 'test';
    } else if (isPerformanceEnvironment()) {
      return 'performance';
    } else if (isStagingEnvironment()) {
      return 'staging';
    } else if (isProductionEnvironment()) {
      return 'production';
    } else {
      return 'unknown';
    }
  },

  isLocalEnv: function(){
    return isLocalEnvironment();
  },
  isDebugEnv: function() {
    return isServerDebugEnvironment() || isClientDebugEnvironment() || isDebugEnvironment();
  },
  isTestEnv: function() {
    return isTestEnvironment();
  },
  isPerfEnv: function() {
    return isPerformanceEnvironment();
  },
  isStagingEnv: function() {
    return isStagingEnvironment();
  },
  isProductionEnv: function() {
    return isProductionEnvironment();
  },

  RentCentric: {
    getEnvironment: function() {
      if (isLocalEnvironment()){
        return RentCentricEnvironments.local;
      } else if(isDebugEnvironment()) {
        return RentCentricEnvironments.debug;
      } else if (isServerDebugEnvironment()) {
        return RentCentricEnvironments.serverdebug;
      } else if (isClientDebugEnvironment()) {
        return RentCentricEnvironments.clientdebug;
      } else if (isTestEnvironment()) {
        return RentCentricEnvironments.test;
      } else if (isPerformanceEnvironment()) {
        return RentCentricEnvironments.performance;
      } else if (isStagingEnvironment()) {
        return RentCentricEnvironments.staging;
      } else if (isProductionEnvironment()) {
        return RentCentricEnvironments.production;
      } else {
        console.error('FATAL! Could not determine RentCentric environment');
        return {};
      }
    }
  },
  Stripe: {
    getEnvironment: function() {
      if (isLocalEnvironment() || isDebugEnvironment()) {
        return StripeEnvironments.debug;
      } else if (isServerDebugEnvironment()) {
        return StripeEnvironments.serverdebug;
      } else if (isClientDebugEnvironment()) {
        return StripeEnvironments.clientdebug;
      } else if (isTestEnvironment()) {
        return StripeEnvironments.test;
      } else if (isPerformanceEnvironment()) {
        return StripeEnvironments.performance;
      } else if (isStagingEnvironment()) {
        return StripeEnvironments.staging;
      }  else if (isProductionEnvironment()) {
        return StripeEnvironments.production;
      } else {
        console.error('FATAL! Could not determine Stripe environment');
        return {};
      }
    }
  },
  Mailgun: {
    getEnvironment: function() {
      if (isLocalEnvironment() || isDebugEnvironment()) {
        return MailgunEnvironments.debug;
      } else if (isServerDebugEnvironment()) {
        return MailgunEnvironments.serverdebug;
      } else if (isClientDebugEnvironment()) {
        return MailgunEnvironments.clientdebug;
      } else if (isTestEnvironment()) {
        return MailgunEnvironments.test;
      } else if (isPerformanceEnvironment()) {
        return MailgunEnvironments.performance;
      }  else if (isStagingEnvironment()) {
        return MailgunEnvironments.staging;
      } else if (isProductionEnvironment()) {
        return MailgunEnvironments.production;
      } else {
        console.error('FATAL! Could not determine Mailgun environment');
        return {};
      }
    }
  },
  FlexDriveWebsite: {
    getURL: function() {
      if(isLocalEnvironment()){
        return "http://localhost:1337/parse";
      } else if (isDebugEnvironment()) {
        return "https://flexdrive-dev.herokuapp.com/";
      } else if (isServerDebugEnvironment()) {
        return "https://flexdrive-dev.herokuapp.com/";
      } else if (isClientDebugEnvironment()) {
        return "https://flexdrive-dev.herokuapp.com/";
      } else if (isTestEnvironment()) {
        return "https://flexdrive-test.herokuapp.com/"
      } else if (isPerformanceEnvironment()) {
        return "https://flexdrive-perf.herokuapp.com/"
      } else if (isStagingEnvironment()) {
        return "https://flexdrive-staging.herokuapp.com/"
      }  else if (isProductionEnvironment()) {
        return "https://www.flexdrive.com/";
      } else {
        console.error('FATAL! Could not determine the URL for the FlexDrive website');
        return "";
      }
    }
  },
  Parse: {
    getEnvironment: function() {
      if(isLocalEnvironment()){
        return ParseEnvironments.local;
      } else if (isDebugEnvironment()) {
        return ParseEnvironments.debug;
      } else if (isServerDebugEnvironment()) {
        return ParseEnvironments.serverdebug;
      } else if (isClientDebugEnvironment()) {
        return ParseEnvironments.clientdebug;
      } else if (isTestEnvironment()) {
        return ParseEnvironments.test;
      } else if (isPerformanceEnvironment()) {
        return ParseEnvironments.performance;
      } else if (isStagingEnvironment()) {
        return ParseEnvironments.staging;
      }  else if (isProductionEnvironment()) {
        return ParseEnvironments.production;
      } else {
        console.error('FATAL! Could not determine the URL for the parse environment');
        return "";
      }
    }
  }
};

// Private API:
function isLocalEnvironment(){
  return Parse.applicationId === ParseLocalAppId;
}
function isDebugEnvironment() {
  if (Parse.applicationId == ParseDebugAppId) {
    return true;
  } else {
    return false;
  }
};
function isServerDebugEnvironment() {
  if (Parse.applicationId == ParseServerDebugAppId) {
    return true;
  } else {
    return false;
  }
};
function isClientDebugEnvironment() {
  if (Parse.applicationId == ParseClientDebugAppId) {
    return true;
  } else {
    return false;
  }
};
function isTestEnvironment() {
  if (Parse.applicationId == ParseTestAppId) {
    return true;
  } else {
    return false;
  }
};
function isPerformanceEnvironment() {
  if (Parse.applicationId == ParsePerformanceAppId) {
    return true;
  } else {
    return false;
  }
};
function isStagingEnvironment() {
  if (Parse.applicationId == ParseStagingAppId) {
    return true;
  } else {
    return false;
  }
};
function isProductionEnvironment() {
  if (Parse.applicationId == ParseProductionAppId) {
    return true;
  } else {
    return false;
  }
};
