/**
* Flexdrive Stripe
* Parse Cloud Module
*/

// Imports

var Environment = require('./flexdrive-environment.js');
var Log = require('./flexdrive-log.js');
var Error = require('./flexdrive-error.js');

var StripeEnvironment = Environment.Stripe.getEnvironment();
var Stripe = require('stripe')(StripeEnvironment.secretKey);

var Buffer = require('buffer').Buffer;
var Utils = require('./flexdrive-utils.js');
var Data = require('./flexdrive-data.js');
var _ = require('underscore');

module.exports = {
  /**
  * Get the version of the module.
  * @return {String}
  */
  version: '1.0.0',

  charge: function(billing) {
    var rate;
    if (billing.get('netRate')) {
      rate = billing.get('netRate');
    } else {
      rate = billing.get('balanceDue');
    }

    var charges = [{amount:rate, description:"Rate"}];
    var fees = billing.get("fees");
    if (fees) {
      _.each(fees, function(fee) {
        charges.push({amount:fee.amount, description:fee.name});
      });
    }
    var taxes = JSON.parse(billing.get("taxes"));
    _.each(taxes, function(taxInfo) {
      charges.push({amount:taxInfo.amount, description:taxInfo.name});
    });

    var stripeInvoiceId = billing.get('stripeInvoiceId');
    var customerIDStripe = billing.get('customerIDStripe');
    var reservationId = billing.get('reservationId');
    var reservation = JSON.parse(billing.get('reservation'))
    var renewalOrODR = 'ODR';
    if (reservation.RentalID && reservation.RentalID !== "0") {
      renewalOrODR = 'Renewal';
    }
    return Stripe_Internal.cleanUpInvoice(stripeInvoiceId).then(function(invoice) {
      if (invoice) {
        if (invoice.paid) {
          return Parse.Promise.as(invoice);
        }
        if (!invoice.closed) {
          // Could not clean up the invoice
          return Parse.Promise.error("Could not clean up previous invoice " + stripeInvoiceId);
        }
      }
      return Parse.Promise.as();
    }).then(function(invoice) {
      if (invoice && invoice.paid) {
        return Parse.Promise.as(invoice);
      }
      return Stripe_Internal.cleanUpLineItems(customerIDStripe);
    }).then(function(invoice) {
      if (invoice && invoice.paid) {
        return Parse.Promise.as(invoice);
      }
      // Create line items and then create invoice
      return Stripe_Internal.createInvoiceLineItems(customerIDStripe,charges).then(function(invoiceLineItem) {
        return Stripe_Internal.createInvoice(invoiceLineItem.customer);
      });
    }).then(function(invoice) {
      if (invoice && invoice.paid) {
        return Parse.Promise.as(invoice);
      }
      billing.set('stripeInvoiceId',invoice.id);
      billing.set('totalAmount',Utils.convertToTwoDecimalNumber(invoice.total.toString()));
      var taxesPaid = 0;
      _.each(taxes, function(taxInfo) {
        taxesPaid += parseInt(Utils.convertToPennies(taxInfo.amount));
      });
      billing.set('taxAmount',Utils.convertToTwoDecimalNumber(taxesPaid.toString()));
      return billing.save().then(function(billing) {
        return Parse.Promise.as(invoice);
      });
    }).then(function(invoice) {
      if (invoice.paid) {
        return Parse.Promise.as(invoice);
      }
      //console.log('!!! add default card')
      return addDefaultCardOrNullToBillingForStripeCustomerId(customerIDStripe, billing, invoice);
    }).then(function(invoice) {
      if (invoice.paid) {
        return Parse.Promise.as(invoice);
      }

      return Stripe_Internal.payInvoice(invoice).then(function(invoice) {
        // All good here
        return Parse.Promise.as(invoice);
      }, function(error) {
        var errorAsJson = JSON.stringify(error);
        billing.set('paymentError', errorAsJson);
        billing.set(Data.Billing.keys.pending, null);
        return billing.save().then(function() {
          return Log.paymentError(renewalOrODR, errorAsJson, reservationId).always(function() {
            if (error.message) {
              return Parse.Promise.error(error.message);
            } else {
              return Parse.Promise.error('Stripe could not charge the card');
            }
          });
        });
      });
    }).then(function(invoice) {
      billing.set('paymentError', null);
      billing.set(Data.Billing.keys.pending, null);
      billing.set('paid',new Date());
      if (invoice.receipt_number) {
        billing.set('authCode',invoice.receipt_number);
      }
      return billing.save().then(function() {
        return Parse.Promise.as(invoice);
      });
    }).then(function(invoice) {
      return Stripe_Internal.getCharge(invoice.charge)
    }).then(function(charge) {
      billing.set('transCode', charge.balance_transaction);
      billing.set('lastFour', charge.source.last4);
      return billing.save();
    }, function(error) {
      var stripeError = Error.billingSystemError();
      Error.addObjectToError(stripeError, error);
      return Parse.Promise.error(error);
    });
  },
  getChargesForDateRange: function(date1, date2, lastObjId) {
    if (!date1 || !date2) {
      return Parse.Promise.error('Must set both dates')
    }

    var date1Seconds = Math.round(date1.getTime() / 1000)
    var date2Seconds = Math.round(date2.getTime() / 1000)

    var params = {
      'limit' : 100,
      'created[gte]' : date1Seconds,
      'created[lt]' : date2Seconds,
    }
    if (lastObjId) {
      params['starting_after'] = lastObjId;
    }

    var path = "charges";
    return callStripeWithPath(path, 'Get', null, params).then(function(data) {
      return Parse.Promise.as(data);
    }, function(error) {
      return Parse.Promise.error(errorForStripeError(error));
    });
  },
  getInvoice: function(invoiceId) {
    return Stripe_Internal.getInvoice(invoiceId)
  },
  getCharge: function(chargeId) {
    return Stripe_Internal.getCharge(chargeId)
  },
  getTransaction: function(transactionId) {
    return Stripe_Internal.getTransaction(transactionId)
  },
  Payments: {
    V2: {
      listCards: function(user) {
        var stripeId = user.get("customerIDStripe")
        var path = "customers/"  + stripeId;
        return callStripeWithPath(path, 'Get', null, null).then(function(httpResponse) {
          var cards = httpResponse.sources.data;
          var defaultCardId = httpResponse.default_source;
          module.exports.Payments.prepareCardsJSONForClient(cards, defaultCardId);
          return Parse.Promise.as(cards);
        }, function(error) {
          return Parse.Promise.error(errorForStripeError(error));
        });
      },
      updateDefaultCard: function(user, cardId) {
        var stripeId = user.get("customerIDStripe")
        var path = "customers/"  + stripeId;
        var body = "default_source=" + cardId
        return callStripeWithPath(path, 'Post', body, null).then(function(httpResponse) {
          return Parse.Promise.as()
        }, function(error) {
          return Parse.Promise.error(errorForStripeError(error));
        });
      },
      updateCard: function(user, cardId, expMonth, expYear, zip) {
        // List the cards
        var stripeId = user.get("customerIDStripe")
        var path = "customers/"  + stripeId;
        return callStripeWithPath(path, 'Get', null, null).then(function(httpResponse) {
          // Find the card
          var cards = httpResponse.sources.data;
          var card = null;
          for (var i = 0; i < cards.length; ++i) {
            if (cards[i].id === cardId) {
              card = cards[i]
              break;
            }
          }

          if (card) {
            // Update the card
            path += '/sources/' + cardId
            var body = "address_zip=" + zip + "&exp_month=" + expMonth + "&exp_year=" + expYear
            return callStripeWithPath(path, 'Post', body, null)
          } else {
            return Parse.Promise.as()
          }
        }).then(function() {
          return Parse.Promise.as()
        }, function(error) {
          return Parse.Promise.error(errorForStripeError(error));
        });
      },
      deleteCard: function(user, cardId) {
        var stripeId = user.get("customerIDStripe")
        var path = "customers/"  + stripeId + "/sources/" + cardId;
        return callStripeWithPath(path, 'Delete', null, null).then(function(httpResponse) {
          return Parse.Promise.as()
        }, function(error) {
          return Parse.Promise.error(errorForStripeError(error));
        });
      },
      createCard: function(user, token, makeDefault, existingFingerprints) {
        var stripeId = user.get("customerIDStripe")
        var path = "customers/"  + stripeId + "/sources";
        body = "source=" + token;

        return callStripeWithPath(path, 'Post', body, null).then(function(card) {
          var fingerprint = card.fingerprint;
          var fingerprintAlreadyExists = _.contains(existingFingerprints, fingerprint)

          if (fingerprintAlreadyExists) {
            console.log("Fingerprint already exists: " + fingerprint);
            var cardId = cardId;
            return module.exports.Payments.V2.deleteCard(user, card.id).then(function() {
              return Parse.Promise.error(Error.cardAlreadyExistsStripeError());
            }, function(error) {
              // TODO: Is there a better way to handle this?  The user will have duplicate cards
              // if the deleteCard call fails.
              return Parse.Promise.error(Error.cardAlreadyExistsStripeError());
            });
          } else {
            return Parse.Promise.as(card);
          }
        }).then(function(card) {
          removeUnneededFieldsFromCard(card)

          if (makeDefault) {
            return module.exports.Payments.V2.updateDefaultCard(user, card.id).then(function() {
              card['defaultCard'] = true;
              return Parse.Promise.as(card);
            });
          } else {
            card['defaultCard'] = false;
            return Parse.Promise.as(card);
          }
        }, function(error) {
          if (error && error.type && error.type == "card_error" && error.message && error.message.indexOf("zip code") != -1) {
            return Parse.Promise.error(Error.cardZipError());
          } else {
            return Parse.Promise.error(errorForStripeError(error));
          }
        });
      }
    },
    prepareCardsJSONForClient: function(cards, defaultCardId) {
      _.each(cards, function(card) {
        removeUnneededFieldsFromCard(card);
        if (card.id == defaultCardId) {
          card["defaultCard"] = true;
        } else {
          card["defaultCard"] = false;
        }
      });
    }
  },
  Customers: {
    V2: {
      delete: function(stripeId) {
        var path = "customers/"  + stripeId;
        return callStripeWithPath(path, 'Delete', null, null).fail(function(error) {
          return Parse.Promise.error(errorForStripeError(error));
        });
      },
    },
  }
};

var Stripe_Internal = {

  // If there is a paid invoice return it. If there is an unpaid invoice close it. If there is no invoice do nothing.
  cleanUpInvoice: function(stripeInvoiceId) {
    if (stripeInvoiceId) {
      return Stripe.invoices.retrieve(stripeInvoiceId).then(function(invoice) {
        if (invoice.paid) {
          return Parse.Promise.as(invoice);
        }
        // Invalid the invoice by marking it as closed
        return Stripe_Internal.closeInvoice(stripeInvoiceId);
      });
    } else {
      return Parse.Promise.as();
    }
  },

  // Assumes the billing job runs about every day so only goes
  // back 48 hours
  cleanUpLineItems: function(customerIDStripe) {
    //Stripe uses seconds, not milliseconds, for timestamps
    var now = new Date();
    var fortyEightHoursAgo = new Date(now.getTime() - (48 * 3600000));
    // Drop the milliseconds
    var fortyEightHoursAgo = Math.round(fortyEightHoursAgo / 1000);

    return Stripe_Internal.removeInvoiceLineItems(customerIDStripe, fortyEightHoursAgo);
  },

  removeInvoiceLineItems: function(customerIDStripe, stripeTimestamp) {
    return Stripe_Internal.getInvoiceLineItems(customerIDStripe, stripeTimestamp).then(function(lineItems) {
      //console.log('found ' + lineItems.data.length + " line items");
      var chainedPromise = Parse.Promise.as();
      _.each(lineItems.data, function(lineItem) {
        chainedPromise = chainedPromise.then(function() {
          if (lineItem.invoice) {
            // Line item belongs to an invoice so skip it
            //console.log('skipping line item ' + lineItem.id);
            return Parse.Promise.as();
          } else {
            // Line item does not belong to an invoice so delete it
            //console.log('destroying line item ' + lineItem.id);
            return Stripe.InvoiceItems.destroy(lineItem.id);
          }
        });
      });
      return chainedPromise;
    });
  },

  createInvoiceLineItems: function(customerIDStripe, charges) {
    var chainedPromise = Parse.Promise.as();
    _.each(charges, function(charge) {
      chainedPromise = chainedPromise.then(function() {
        var invoiceItemData = {
          'customer': customerIDStripe,
          'amount': Utils.convertToPennies(''+charge.amount),
          'currency': 'usd',
          'description': charge.description
        };
        return Stripe.invoiceItems.create(invoiceItemData);
      });
    });
    return chainedPromise;
  },

  getInvoiceLineItems: function(customerIDStripe, stripeTimestamp) {
    var buf = new Buffer(StripeEnvironment.secretKey, 'utf8');
    var base64EncodedKey = buf.toString('base64');
    return Parse.Cloud.httpRequest({
      method: 'GET',
      url: 'https://api.stripe.com/v1/invoiceitems',
      headers: {
        'Authorization': 'Basic ' + base64EncodedKey,
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      params: {
        'created[gt]' : stripeTimestamp,
        customer: customerIDStripe
      }
    }).then(function (httpResponse) {
      return Parse.Promise.as(httpResponse.data);
    }, function(httpErrorResponse) {
      return Parse.Promise.error(httpErrorResponse.data.error);
    });
  },

  getInvoice: function(invoiceId) {
    if (!invoiceId) {
      return Parse.Promise.as(null)
    }

    var buf = new Buffer(StripeEnvironment.secretKey, 'utf8');
    var base64EncodedKey = buf.toString('base64');
    return Parse.Cloud.httpRequest({
      method: 'GET',
      url: 'https://api.stripe.com/v1/invoices/' + invoiceId,
      headers: {
        'Authorization': 'Basic ' + base64EncodedKey,
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }).then(function (httpResponse) {
      return Parse.Promise.as(httpResponse.data);
    }, function(httpErrorResponse) {
      return Parse.Promise.error(httpErrorResponse.data.error);
    });
  },

  getTransaction: function(transactionId) {
    if (!transactionId) {
      return Parse.Promise.as(null)
    }

    var buf = new Buffer(StripeEnvironment.secretKey, 'utf8');
    var base64EncodedKey = buf.toString('base64');
    return Parse.Cloud.httpRequest({
      method: 'GET',
      url: 'https://api.stripe.com/v1/balance/history/' + transactionId,
      headers: {
        'Authorization': 'Basic ' + base64EncodedKey,
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }).then(function (httpResponse) {
      return Parse.Promise.as(httpResponse.data);
    }, function(httpErrorResponse) {
      return Parse.Promise.error(httpErrorResponse.data.error);
    });
  },

  createInvoice: function(customerIDStripe) {
    var buf = new Buffer(StripeEnvironment.secretKey, 'utf8');
    var base64EncodedKey = buf.toString('base64');
    return Parse.Cloud.httpRequest({
      method: 'POST',
      url: 'https://api.stripe.com/v1/invoices',
      headers: {
        'Authorization': 'Basic ' + base64EncodedKey,
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: 'customer=' + customerIDStripe
    }).then(function (httpResponse) {
      return Parse.Promise.as(httpResponse.data);
    }, function(httpErrorResponse) {
      return Parse.Promise.error(httpErrorResponse.data.error)  ;
    });
  },

  closeInvoice: function(invoiceId) {
    var buf = new Buffer(StripeEnvironment.secretKey, 'utf8');
    var base64EncodedKey = buf.toString('base64');
    return Parse.Cloud.httpRequest({
      method: 'POST',
      url: 'https://api.stripe.com/v1/invoices/' + invoiceId,
      headers: {
        'Authorization': 'Basic ' + base64EncodedKey,
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: 'closed=true'
    }).then(function (httpResponse) {
      return Parse.Promise.as(httpResponse.data);
    }, function(httpErrorResponse) {
      return Parse.Promise.error(httpErrorResponse.data.error)  ;
    });
  },

  payInvoice: function(invoice) {
    var buf = new Buffer(StripeEnvironment.secretKey, 'utf8');
    var base64EncodedKey = buf.toString('base64');
    return Parse.Cloud.httpRequest({
      method: 'POST',
      url: 'https://api.stripe.com/v1/invoices/' + invoice.id + '/pay',
      headers: {
        'Authorization': 'Basic ' + base64EncodedKey,
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }).then(function (httpResponse) {
      return Parse.Promise.as(httpResponse.data);
    }, function(httpErrorResponse) {
      if (httpErrorResponse.data && httpErrorResponse.data.error) {
        return Parse.Promise.error(httpErrorResponse.data.error);
      } else if (httpErrorResponse.data) {
        return Parse.Promise.error(httpErrorResponse.data);
      } else {
        return Parse.Promise.error(httpErrorResponse);
      }
    });
  },

  getCharge: function(chargeId) {
    var buf = new Buffer(StripeEnvironment.secretKey, 'utf8');
    var base64EncodedKey = buf.toString('base64');
    return Parse.Cloud.httpRequest({
      method: 'POST',
      url: 'https://api.stripe.com/v1/charges/' + chargeId,
      headers: {
        'Authorization': 'Basic ' + base64EncodedKey,
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }).then(function (httpResponse) {
      return Parse.Promise.as(httpResponse.data);
    }, function(httpErrorResponse) {
      if (httpErrorResponse.data) {
        return Parse.Promise.error(httpErrorResponse.data.error);
      } else {
        return Parse.Promise.error(httpErrorResponse.data);
      }
    });
  },
};

function errorForStripeError(error) {
  if (Error.isFlexDriveError(error)) {
    return error;
  }

  var stripeError = Error.unknownStripeError();

  if (Error.isJavascriptError(error)) {
    Error.addObjectToError(stripeError, error)
  } else {
    Error.addObjectToError(stripeError, {"Stripe Error" : error} );
  }

  return stripeError
}

function callStripeWithPath(path, method, body, parameters) {
  try {
    var buf = new Buffer(StripeEnvironment.secretKey, 'utf8');
    var base64EncodedKey = buf.toString('base64');
    var url = 'https://api.stripe.com/v1/' + path;
    console.log("Calling Stripe with: " + path + " and URL: " + url)

    return Parse.Cloud.httpRequest({
      method: method,
      url: url,
      headers: {
        'Authorization': 'Basic ' + base64EncodedKey,
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: body,
      params: parameters
    }).then(function(httpResponse) {
      return Parse.Promise.as(httpResponse.data);
    }, function(httpErrorResponse) {
      if (httpErrorResponse.data && httpErrorResponse.data.error) {
        console.log("Call to Stripe for " + path + " Failed: " + JSON.stringify(httpErrorResponse.data.error));
        return Parse.Promise.error(httpErrorResponse.data.error);
      } else if (httpErrorResponse.data) {
        console.log("Call to Stripe for " + path + " Failed: " + JSON.stringify(httpErrorResponse.data));
        return Parse.Promise.error(httpErrorResponse.data);
      } else {
        console.log("Call to Stripe for " + path + " Failed: " + JSON.stringify(httpErrorResponse));
        return Parse.Promise.error(httpErrorResponse);
      }
    });
  } catch (error) {
    console.log("Call to Stripe for " + path + " Failed");
    return Parse.Promise.error(error);
  };
}

function removeUnneededFieldsFromCard(card) {
  card.address_city = undefined;
  card.address_country = undefined;
  card.address_line1 = undefined;
  card.address_line1_check = undefined;
  card.address_line2 = undefined;
  card.address_state = undefined;
  card.address_zip_check = undefined;
  card.country = undefined;
  card.cvc_check = undefined;
  card.dynamic_last4 = undefined;
  card.funding = undefined;
  card.metadata = undefined;
  card.name = undefined;
  card.object = undefined;
  card.tokenization_method = undefined;
}

function logStripeErrorForJavascriptError(error, path) {
  var systemError = Error.systemError();
  var name = error.name;
  var message = error.message;
  var errorMessage = { name : message };
  Error.addObjectToError(systemError, error);

  return Log.logFlexDriveError(stripeError, null, null, null, null, path, null, null).then(function() {
    return Parse.Promise.error(error);
  }, function(error) {
    return Parse.Promise.error(error);
  });
}

function addDefaultCardOrNullToBillingForStripeCustomerId(customerId, billing, invoice) {
  var path = "customers/"  + customerId;
  return callStripeWithPath(path, 'Get', null, null).then(function(httpResponse) {
    var cards = httpResponse.sources.data;
    var defaultCardId = httpResponse.default_source;
    billing.set('stripeCardId', defaultCardId);
    return Utils.saveAndIgnoreErrorForObject(billing).then(function() {
      return Parse.Promise.as(invoice);
    })
  }, function(error) {
    return Parse.Promise.as(invoice);
  });
}
