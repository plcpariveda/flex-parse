/**
* FlexDrive Utilities
* Parse Cloud Module
* 1.0.0
*/

var Environment = require('./flexdrive-environment.js');
var RentCentricEnvironment = Environment.RentCentric.getEnvironment();
var Log = require('./flexdrive-log.js');
var _ = require('underscore');

module.exports = {
  /**
  * Get the version of the module.
  * @return {String}
  */
  version: '1.0.0',

  //;;!! need a timezone library
  easternTimezone: {Name:"EST",Offset:-5},
  centralTimezone: {Name:"CST",Offset:-6},

  reportSuccess: function(data, options, promise) {
    if (options && options.success) { options.success(data); }
    promise.resolve(data);
  },

  reportFailure: function(error, options, promise) {
    if (options && options.error) { options.error(error); }
    promise.reject(error);
  },

  getRateVehicleType: function(vehicleType, rateVehicleTypes) {
    var result = null;
    _.each(rateVehicleTypes, function(rateVehicleType) {
      if (rateVehicleType.VehicleType == vehicleType) {
        result = rateVehicleType;
      }
    });
    return result;
  },

  isValidVersion: function(version) {
    var components = version.split(".");

    var isValid = components.length == 3;

    if (isValid) {
      console.log(components);
      for (var i = 0; i < components.length; i++) {
        var component = components[i];
        if (this.isValidVersionComponent(component) === false) {
          isValid = false;
          break;
        }
      }
    }

    return isValid;
  },

  isValidVersionComponent: function(component) {
    var intValue = parseInt(component);
    return isNaN(intValue) === false && intValue.toString() == component;
  },

  needsUpgrade: function(currentVersion, upgradeVersion) {
    var currentVersionComponents = currentVersion.split(".");
    var upgradeVersionComponents = upgradeVersion.split(".");

    if (currentVersionComponents.length != 3 || upgradeVersionComponents.length != 3) {
      return false;
    }

    var shouldUpgrade = false;
    for (var i = 0; i < 3; i++) {
      if (currentVersionComponents[i] < upgradeVersionComponents[i]) {
        shouldUpgrade = true;
        break;
      }
    }

    return shouldUpgrade;
  },

  // input string
  convertToPennies: function(amount) {
    if (typeof amount === 'number') {
      amount = '' + amount;
    }
    var dotIndex = amount.indexOf('.');
    if (dotIndex == -1) {
      return amount + '00';
    }

    // If nothing after the dot
    if (amount.length == (dotIndex+1)) {
      // Remove the dot and add 00 for pennies
      return amount.replace('.','0') + '0';
    }

    // If there is only 1 character after the dot
    if (amount.length == (dotIndex+2)) {
      // Remove the dot and add 0
      return amount.replace('.','') + '0';
    }

    // If there two or more characters after the dot
    if (amount.length >= (dotIndex+3)) {
      // Remove the dot and add 00 for pennies
      return amount.replace('.','').substring(0,dotIndex+2);
    }

    // Should never get here
    return amount;
  },

  // Input string
  convertToTwoDecimalNumber: function(pennies) {
    if (typeof pennies === 'number') {
      pennies = '' + pennies;
    }
    if (pennies.length == 0) {
      return "0.00";
    }
    if (pennies.length == 1) {
      return '0.0' + pennies;
    }
    if (pennies.length == 2) {
      return '0.' + pennies;
    }
    return pennies.substring(0,pennies.length - 2) + '.' +
    pennies.substring(pennies.length - 2,pennies.length);
  },

  // ;;!! Need a timezone library
  isDateMonthYearEqual: function(date1, date2, tz) {
    if (!date1 || !date2) {
      return false;
    }

    var timezoneDate1 = this.getDateForTimezone(date1, tz);
    var timezoneDate2 = this.getDateForTimezone(date2, tz);

    var result = timezoneDate1.getMonth() == timezoneDate2.getMonth()
    && timezoneDate1.getDate() == timezoneDate2.getDate()
    && timezoneDate1.getFullYear() == timezoneDate2.getFullYear();

    // console.log("isDateMonthYearEqual " +
    // '\n date1 ' + date1.getTime() +
    // '\n date2 ' + date2.getTime() +
    // '\n date1 ' + date1 +
    // '\n date2 ' + date2 +
    // '\n timezoneDate1 ' + timezoneDate1 +
    // '\n timezoneDate2 ' + timezoneDate2 +
    // '\n tz ' + JSON.stringify(tz) +
    // '\n returning ' + result);

    return result;
  },

  convertDateFromTZToGMT: function(date, tz) {
    // Adjust the hours for the time
    var offset = tz.Offset * -1;
    return new Date(date.getFullYear(), date.getMonth(), date.getDate(),
    (date.getHours() + offset), date.getMinutes(), date.getSeconds(),
    date.getMilliseconds());
  },

  getDateForTimezone: function(date, tz) {
    // Adjust the hours for the time
    var offset = 0;
    if (tz) {
      offset = tz.Offset;
    }
    return new Date(date.getFullYear(), date.getMonth(), date.getDate(),
    (date.getHours() + offset), date.getMinutes(), date.getSeconds(),
    date.getMilliseconds());
  },

  getDateTimezoneAsText: function(date, tz) {

    if (!date || !tz) {
      return '';
    }
    var text = date.toString();
    if (date && tz) {
      var dateTz = this.getDateForTimezone(date, tz);
      dateTzAsText = dateTz.toString();
      var indexOfG = dateTzAsText.indexOf('G');
      if (indexOfG > -1) {
        text = dateTzAsText.substring(0,indexOfG) + tz.Name;
      }
    }
    return text;
  },

  getDateTimezoneAsTextMMDDYYYY: function(date, tz) {

    if (!date || !tz) {
      return '';
    }
    var dateTZ = module.exports.getDateForTimezone(date, tz);

    return '' + (dateTZ.getMonth() + 1) + '/' + dateTZ.getDate() + '/' + dateTZ.getFullYear() + ' ' + dateTZ.getHours() + ':' + module.exports.leftPad(dateTZ.getMinutes(), 2, '0') + ':' + module.exports.leftPad(dateTZ.getSeconds(), 2, '0') + ' ' + tz.Name;
  },

  delay: function(seconds) {
    var start = new Date();
    var now = new Date();
    var milliseconds = seconds * 1000
    while ( (now.getTime() - start.getTime()) < milliseconds) {
      now = new Date();
    }
  },

  elapsedTime: function(start, end) {
    var elaspedTimeInMillis = (end.getTime() - start.getTime());
    var elaspedTimeInSeconds = Math.round(elaspedTimeInMillis / 1000);
    var elaspedTimeInMinutes = elaspedTimeInSeconds / 60;
    return {millis:elaspedTimeInMillis, seconds:elaspedTimeInSeconds, minutes:elaspedTimeInMinutes};
  },

  saveAndIgnoreErrorForObject: function(object) {
    return object.save().always(function() {
      return Parse.Promise.as()
    })
  },

  leftPad: function(input, size, char) {
    if (size < 0) {
      size = 0;
    }
    if (!char) {
      char = '';
    }
    if (!input) {
      input = '';
    }

    if ((typeof input !== 'string') && (typeof input !== 'number')) {
      input = '';
    }

    if (typeof input === 'number') {
      input = '' + input;
    }

    var output = input;
    if (input.length < size) {
      for (var i = input.length; i < size; ++i) {
        output = char + output;
      }
    }

    return output;
  },

  getConfigValue: function(key) {
    var query = new Parse.Query('Config');
    query.equalTo('key', key);
    return query.first().then(function(config) {
      if(!config){
          return Parse.Promise.error("Could not retrieve config value for key: "+JSON.stringify(key));
      }
      return Parse.Promise.as(config.get('value'));
    },function(error) {
      return Parse.Promise.error(error);
    });
  },

  verifyPassword: function(user, password) {
    if (password) {
      return Parse.User.logIn(user.get('username'), password).then(function() {
        return Parse.Promise.as('true')
      }, function(error) {
        return Parse.Promise.as('false')
      })
    } else {
      return Parse.Promise.as('unknown')
    }
  },

  isReservationConfirmedAndOnlyConfirmed: function(reservation) {
    if (!reservation.ReservationStatus.Confirmed || reservation.ReservationStatus.Confirmed !== "Yes") {
      return false;
    }

    var numberOfYesProps = 0;
    for (var prop in reservation.ReservationStatus) {
      if (reservation.ReservationStatus[prop] === "Yes") {
        ++numberOfYesProps;
      }
    }

    if (numberOfYesProps > 1) {
      return false;
    }

    return true
  },
};
