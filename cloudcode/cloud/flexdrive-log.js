/**
* Flexdrive Log
* Parse Cloud Module
*/

var Environment = require('./flexdrive-environment.js');
var Error = require('./flexdrive-error.js');

var typeDebug = 'debug';
var typeInfo = 'info';
var typeWarn = 'warn';

module.exports = {
  logFlexDriveError: function(error, parameters, functionName, jobName, expressPath, methodName, user, installationId) {
    console.log("Logging Error: " + JSON.stringify(error));
    if (Error.isFlexDriveError(error) == false) {
      console.log("Tried to log a non FlexDrive error.");
      return Parse.Promise.as();
    }

    var Log = Parse.Object.extend("Log");
    var log = new Log();

    log.set("type", "error");
    log.set("errorCode", error.code);
    log.set("description", error.description);
    log.set("userMessage", error.userMessage);

    if (parameters) {
      var parametersMessage = { "requestParameters" : parameters };
      Error.addObjectToError(error, parametersMessage);
    }

    if (functionName) {
      log.set("cloudFunctionName", functionName);
    }

    if (jobName) {
      log.set("cloudJobName", jobName);
    }

    if (expressPath) {
      log.set("expressPath", expressPath);
    }

    if (methodName) {
      log.set("methodName", methodName);
    }

    if (error.internalMessages) {
      log.set("internalMessages", error.internalMessages);
    }

    if (user) {
      log.set("user", user);
    }

    if (installationId) {
      var query = new Parse.Query(Parse.Installation);
      query.equalTo("installationId", installationId)
      return query.first().then(function (installation) {
        if (installation) {
          log.set("installation", installation.toJSON());
        }
        return saveLog(log);
      }, function(error) {
        return saveLog(log);
      });
    } else {
      return saveLog(log);
    }
  },

  debug: function(text) {
    if (!text) {
      console.log("Text to log is missing.");
      return Parse.Promise.as();
    }

    console.log(text);

    if (!Environment.isDebugEnv()) {
      return Parse.Promise.as();
    }

    return writeToLog(typeDebug, text);
  },

  info: function(text) {
    if (!text) {
      console.log("Text to log is missing.");
      return Parse.Promise.as();
    }

    console.log(text);
    return writeToLog(typeInfo, text);
  },

  warn: function(text) {
    if (!text) {
      console.log("Text to log is missing.");
      return Parse.Promise.as();
    }

    console.warn(text);
    return writeToLog(typeWarn, text);
  },

  paymentError: function(renewalOrODR, errorText, reservationId) {
    if (!errorText || !reservationId) {
      console.log("Error text or reservation Id to log is missing.");
      return Parse.Promise.as();
    }

    console.log(errorText);
    return writeToLog(typeInfo, 'PaymentErrorStat', 'Recording payment errors', [renewalOrODR, ''+reservationId, errorText]);
  },

}

function saveLog(log) {
  return log.save().fail(function(error) {
    return Parse.Promise.as()
  });
}

function writeToLog(type, description, message, internalMessages) {
  var Log = Parse.Object.extend("Log");
  var log = new Log();

  log.set("type", "info");
  log.set("description", description);
  if (message) {
    log.set("userMessage", message);
  }
  if (internalMessages) {
    log.set("internalMessages", internalMessages);
  }
  return log.save().fail(function(error) {
    console.error("Logging Failed");
    return Parse.Promise.as()
  });
}
