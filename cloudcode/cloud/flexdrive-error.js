/**
* Flexdrive Errors
* Parse Cloud Module
*/

function FlexDriveError(code, description, userMessage) {
  this.code = code;
  this.description = description;
  this.userMessage = userMessage;
  this.internalMessages = [];
};

// Cloud Module Public API:
module.exports = {
  version: '1.0.0',

  isFlexDriveError : function(error) {
    return error instanceof FlexDriveError;
  },

  isJavascriptError : function(error) {
    return error.stack != null;
  },

  flexDriveErrorForError: function(error, errorFunction) {
    if (module.exports.isFlexDriveError(error)) {
      return error;
    }

    var flexDriveError = errorFunction();
    module.exports.addObjectToError(flexDriveError, error);

    return flexDriveError;
  },

  systemErrorForError: function(err) {
    if (module.exports.isFlexDriveError(err)) {
      return err;
    }

    var error = module.exports.systemError();
    module.exports.addObjectToError(error, err);

    return error;
  },

  addObjectToError: function(error, object) {
    if (!object || module.exports.isFlexDriveError(error) == false) {
      return;
    }

    if (module.exports.isJavascriptError(object)) {
      error.internalMessages.push(object.stack);
    } else if (module.exports.isFlexDriveError(object)) {
      error.internalMessages.push({ "internal error" : object });
    } else {
      error.internalMessages.push(object);
    }
  },

  unauthorizedUserError : function() {
    return new FlexDriveError(1,
      'Unauthorized',
      'Server error, try logging out and logging in again.');
  },
  systemError : function() {
    return new FlexDriveError(2,
      'SystemError',
      'Server error.');
  },
  webRegistrationError : function() {
    return new FlexDriveError(3,
      'WebRegistrationError',
      'Unexpected web registration error.');
  },
  rentCentricError : function() {
    return new FlexDriveError(4,
      'RentCentricError',
      'Error accessing RentCentric Server');
  },
  usernameTakenError : function() {
    return new FlexDriveError(5,
      'RegistrationError',
      'Username already taken.');
  },
  invalidEmailError : function() {
    return new FlexDriveError(6,
      'RegistrationError',
      'Invalid Email.');
  },
  registrationAgreementError : function() {
    return new FlexDriveError(7,
      'RegistrationAgreementError',
      'Must accept membership agreement.');
  },
  badCredentials : function() {
    return new FlexDriveError(8,
      'BadCredentials',
      'Bad credentials.');
  },
  lockedOut : function() {
    return new FlexDriveError(9,
      'LockedOut',
      'User locked out.');
  },
  invalidInputError : function() {
    return new FlexDriveError(20,
      'InvalidInput',
      'Unexpected error, missing inputs');
  },
  objectNotFoundError : function() {
    return new FlexDriveError(21,
      'ObjectNotFoundError',
      'Query resulted with no objects found');
  },
  unknownRentCentricError : function() {
    return new FlexDriveError(100,
      'UnknownRentCentricError',
      'System problem, please call xxx-xxx-xxxx.');
  },
  reservationCannotCancelAlreadyPaidError : function() {
    return new FlexDriveError(101,
      'ReservationCancelStateError',
      'Reservation cannot be canceled because it has already been paid.');
  },
  rentalExpiredError : function() {
    return new FlexDriveError(102,
      'RentalExpiredError',
      'Rental has expired.');
  },
  unknownRentCentricCustomerIdError : function() {
    return new FlexDriveError(103,
      'UnknownRentCentricCustomerIdError',
      'The RentCentricId does not correspond to an user in parse.');
  },
  renewalNotificationPreviouslySentError : function() {
    return new FlexDriveError(104,
      'RenewalNotificationPreviouslySentError',
      'A time to renew notification was already sent out for this rental.');
  },
  pushNotificationFailedError : function() {
    return new FlexDriveError(105,
      'PushNotificationFailedError',
      'Sending push notification failed.');
  },
  emailFailedError : function() {
    return new FlexDriveError(106,
      'EmailFailedError',
      'Sending email failed.');
  },
  reservationAlreadyExistsForRentalError: function() {
    return new FlexDriveError(107,
      'ReservationAlreadyExistsForRentalError',
      'A reservation already exists for the rental.  Please cancel the previous reservation or contact Flexdrive.');
  },
  reservationAlreadyExistsForCustomerError: function() {
    return new FlexDriveError(108,
      'ReservationAlreadyExistsForCustomerError',
      'You already have a reservation.  Please contact Flexdrive to cancel the reservation.');
  },
  reservationCannotCancelPaymentInProgressError : function() {
    return new FlexDriveError(109,
      'ReservationCancelStateError',
      'Reservation cannot be canceled because the payment is in progress.');
  },
  reservationCannotDeletePaymentInProgressError : function() {
    return new FlexDriveError(110,
      'ReservationDeleteStateError',
      'Reservation cannot be deleted because the payment is in progress.');
  },
  reservationCannotDeleteAlreadyPaidError : function() {
    return new FlexDriveError(111,
      'ReservationCancelStateError',
      'Reservation cannot be canceled because it has already been paid.');
  },
  rentalAlreadyRenewedError : function() {
    return new FlexDriveError(112,
      'RentalAlreadyRenewedError',
      'Rental has already been renewed.');
  },
  vehicleNotAvailableError : function() {
    return new FlexDriveError(113,
      'VehicleNotAvailableError',
      'The vehicle is not available.');
  },
  reservationNotPaidError : function() {
    return new FlexDriveError(114,
      'ReservationNotPaidError',
      'Reservation has not been paid.');
  },
  reservationConvertToRentalError : function() {
    return new FlexDriveError(115,
      'ReservationNotConvertedToRentalError',
      'Reservation was not converted to rental.');
  },
  rentalNotFoundError : function() {
    return new FlexDriveError(116,
      'RentalNotFoundError',
      'Rental not found.');
  },
  reservationNotFoundError : function() {
    return new FlexDriveError(117,
      'ReservationNotFoundError',
      'Reservation not found.');
  },
  reservationBadStatusError : function() {
    return new FlexDriveError(118,
      'ReservationBadStatusError',
      'Reservation status unexpected.');
  },
  customerNotFoundError : function() {
    return new FlexDriveError(119,
      'CustomerNotFoundError',
      'Customer not found.');
  },
  vehicleNotFoundError : function() {
    return new FlexDriveError(120,
      'VehicleNotFoundError',
      'Vehicle not found.');
  },
  invalidPassword : function() {
    return new FlexDriveError(121,
      'invalidPasswordError',
      'Invalid password.');
  },
  unknownStripeError : function() {
    return new FlexDriveError(200,
      'UnknownStripeError',
      'System problem, please call xxx-xxx-xxxx.');
  },
  cardAlreadyExistsStripeError: function() {
    return new FlexDriveError(201,
    'CardAlreadyExistsStripeError',
    'Tried to add a card which already exists.')
  },
  tokenAlreadyUsedStripeError: function() {
    return new FlexDriveError(202,
    'TokenAlreadyUsedStripeError',
    'Token has already been used.')
  },
  cardZipError : function() {
    return new FlexDriveError(203,
      'CardZipError',
      'Credit card zip not valid.');
  },

  billingSystemError : function() {
    return new FlexDriveError(300,
      'BillingSystemError',
      'Billing Server error.');
  },
  billingTaxError : function() {
    return new FlexDriveError(301,
      'BillingTaxError',
      'Billing Tax error.');
  },
  billingInProcessError : function() {
    return new FlexDriveError(302,
      'BillingInProcessError',
      'Billing is being run on the reservation or renewal.  No operations are allowed until the process completes.')
  }
};

// RentCentric status codes
// 2, vehicle not available
// 100, rental already renewed
// 101, reservation already exists for customer
// 102, rental not found
