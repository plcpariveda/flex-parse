/**
* TODO: This file is getting too big!!
*/

var Environment = require('./flexdrive-environment.js');
var Log = require('./flexdrive-log.js')

var RentCentric = require("./rentcentric.js");
var RentCentricEnvironment = Environment.RentCentric.getEnvironment();
RentCentric.initialize(RentCentricEnvironment.url, RentCentricEnvironment.clientID, RentCentricEnvironment.serviceUserName, RentCentricEnvironment.servicePassword);

var StripeEnvironment = Environment.Stripe.getEnvironment();
var stripe = require("stripe")(StripeEnvironment.secretKey);

var FlexdriveStripe = require("./flexdrive-stripe.js");

var Data = require('./flexdrive-data.js');
var Notifications = require('./flexdrive-notifications.js');
var Mail = require('./flexdrive-email.js');
var Utils = require('./flexdrive-utils.js');
var Billing = require('./flexdrive-billing.js');
var Error = require('./flexdrive-error.js');

var _ = require('underscore');

var userProfileImageUrlKey = 'UserProfileImage'; // TODO: Change this to ProfileImageURL
var userPaymentInfoKey = 'paymentInfo'; // TODO: Change this to PaymentInfo
var userPaymentsInfoKey = 'Cards';
var vehicleUserProfileImageUrlKey = 'UserProfileImage';
var vehicleWeeklyLowMilagePlanPriceKey = 'PriceWeeklyLow';
var vehicleWeeklyHighMilagePlanPriceKey = 'PriceWeeklyHigh';
var vehicleMonthlyLowMilagePlanPriceKey = 'PriceMonthlyLow';
var vehicleMonthlyHighMilagePlanPriceKey = 'PriceMonthlyHigh';
var vehicleActiveSwapRequestExistsKey = 'SwapRequestSent';
var preferredMarketKey = "PreferredMarket";
var marketKey = "Market";
var paid = "Paid";
var NA = 'N/A';

// Stripe API Keys
var stripeCustomerIdKey = 'id';

// Copy
var cannotCreateSwapRequestErrorCopy = 'Swap requests cannot be sent while other requests are in progress.';
var cannotRespondToSomoneElsesSwapRequest = 'Swap requests can only be accepted or rejected by the recipient.';
var cannotSwapWithYourselfErrorCopy = 'You cannot swap masterk with yourself. Explore other Flexdrive inventory.';
var couldNotFindVehicleDetailErrorCopy = 'Vehicle details not found. Please try again later.';

// Rent Centric Values
var rentCentricLocationId = {
    atlanta: '116'
};
var rentCentricCountryCodeUS = 'us';
var rentCentricLicenseValidatedFalse = 'false';

var errorSystems = {
    Stripe: 'Stripe'
};

// Cloud Module Public API:

// TODO: In future release, look for non-network activity that can be parallelized.
module.exports = {
  /**
  * Get the version of the module.
  * @return {String}
  */
  version: '1.0.1',

  errorSystems: errorSystems,
  Logs: {
    V2: {
      prune: function(name) {
        return Utils.getConfigValue("pruneDays").then(function(days) {
          console.log("Pruning " + name + ". Going back " + days + " days");
          var createdAt = new Date();
          createdAt.setDate(createdAt.getDate() - parseInt(days));
          var query = new Parse.Query(name);
          query.lessThanOrEqualTo("createdAt", createdAt);
          query.limit(1000);
          return query.find();
        }).then(function(objs) {
          console.log("Number of " + name + " pruned: " + objs.length);
          return Parse.Object.destroyAll(objs)
        }, function(err) {
          var error = Error.systemError();
          Error.addObjectToError(error, err);
          return Parse.Promise.as(error)
        });
      }
    }
  },

  pingRentCentric: function() {
    return RentCentric.getVersionNumber();
  },

  // TODO: Use Parse config parameters to turn logging on/off
  Users: {
    existsWithUsernameEmail: function(username, email, options) {
        var promise = new Parse.Promise();
        var userExists = false;
        var emailExists = false;
        var usernameExists = false;
        Data.Users.retrieveWithUsername(username).then(function(userWithMatchingUsername) {
            return Data.Users.retrieveWithEmail(email).then(function(userWithMatchingEmail) {
                // TODO: In future release, consolidate below into a method.
                if (userWithMatchingUsername) {
                    userExists = true;
                    if (userWithMatchingUsername.getUsername() == username) {
                        usernameExists = true;
                    }
                    if (userWithMatchingUsername.getEmail() == email) {
                        emailExists = true;
                    }
                }
                if (userWithMatchingEmail) {
                    userExists = true;
                    if (userWithMatchingEmail.getUsername() == username) {
                        usernameExists = true;
                    }
                    if (userWithMatchingEmail.getEmail() == email) {
                        emailExists = true;
                    }
                }
                var data = {
                    userExists: userExists,
                    emailExists: emailExists,
                    usernameExists: usernameExists
                };
                Utils.reportSuccess(data, options, promise);
            });
        }, function(error) {
            Utils.reportFailure(error, options, promise);
        });
        return promise;
    },
    createCustomerAccounts: function(parseUser, data, options) {
        // TODO: Validate input. if(paymentToken.length === 0)
        var promise = new Parse.Promise();
        Users_Internal.createStripeAccount(parseUser, data.stripePaymentToken).then(function(stripeCustomer) {
            parseUser.set(Data.Users.keys.stripeCustomerId, stripeCustomer[stripeCustomerIdKey]);
            return Users_Internal.createRentCentricAccount(parseUser, data.customerData);
        }).then(function(rentCentricCustomerId) {
            parseUser.set(Data.Users.keys.rentCentricCustomerId, rentCentricCustomerId);
            return parseUser.save().then(function(savedUser) {
                var k = RentCentric.customerKeys();
                var firstName = data.customerData[k.firstName];
                var lastName = data.customerData[k.lastName];
                var customerId = savedUser.get(Data.Users.keys.rentCentricCustomerId, rentCentricCustomerId);
                var customerData = {
                    id: customerId,
                    firstName: firstName,
                    lastName: lastName
                };
                Mail.sendNewUserRegistrationApplication(savedUser, customerData).always(function() {
                    return Parse.Promise.as();
                });
            });
        }).then(function() {
            Utils.reportSuccess(null, options, promise);
        }, function(error) {
            Utils.reportFailure(error, options, promise);
        });
        return promise;
    },
    updateEmail: function(parseUser, email, options) {
        // TODO: Check input.
        var promise = new Parse.Promise();
        var rentCentricCustomerId = parseUser.get(Data.Users.keys.rentCentricCustomerId);
        var stripeCustomerId = parseUser.get(Data.Users.keys.stripeCustomerId);
        // TODO: Clean this up. Consolidate logic.
        if (!stripeCustomerId || !rentCentricCustomerId) {
            Utils.reportFailure('Fatal error. Could not access either Stripe and/or Rent Centric customer ID.', options, promise);
            return;
        }
        var oldEmail = parseUser.getEmail();
        var oldUsername = parseUser.getUsername();
        // TODO: Bail if either customer ID is unavailable.
        parseUser.setEmail(email);
        parseUser.setUsername(email.toLowerCase());
        parseUser.save().then(function(savedParseUser) {
            return stripe.customers.update(stripeCustomerId, {email: email});
        }).then(function(updatedStripeCustomer) {
            RentCentric.Customers.updateEmail(rentCentricCustomerId, email).then(function() {
                Utils.reportSuccess(null, options, promise);
            }, function(error) {
                // TODO: Send email to FlexDrive letting them know customer has
                //  updated their email in Stripe but not in Rent Centric.
                Utils.reportSuccess(null, options, promise); // Success since email did update in Stripe.
            });
        }, function(error) {// Could not update Stripe Customer, rollback.
          console.error('Error updating emails: ' + JSON.stringify(error));
            parseUser.setEmail(oldEmail);
            parseUser.setUsername(oldUsername);
            parseUser.save().always(function() {
                Utils.reportFailure(error, options, promise);
            });
        });

        return promise;
    },
    retrieve: function(parseUser, options) {
        // TODO: Check input.
        var promise = new Parse.Promise();
        var userProfileImageUrl = Data.Users.retrieveProfileImageUrl(parseUser);
        var rentCentricCustomerId = parseUser.get(Data.Users.keys.rentCentricCustomerId);
        var stripeCustomerId = parseUser.get(Data.Users.keys.stripeCustomerId);
        // TODO: Bail if either customer ID is unavailable.
        var user;
        // TODO: Parallelize.
        RentCentric.Customers.retrieve(rentCentricCustomerId).then(function(rentCentricCustomer) {
            user = rentCentricCustomer;
            // Replace 'LocationID' with 'Market' to abstract RentCentric out.
            var locationID = user[RentCentric.Customers.keys.locationId];
            delete user[RentCentric.Customers.keys.locationId];
            return Data.Markets.marketForID(locationID).always(function(market) {
              if (market) {
                user[preferredMarketKey] = market;
              }
              return stripe.customers.retrieve(stripeCustomerId);
            });
        }).then(function(stripeCustomer) {
            user["LicenseNumber"] = "XXXXXXX";
            user[userProfileImageUrlKey] = userProfileImageUrl;
            user[userPaymentInfoKey] = stripeCustomer.cards.data[0]; // TODO(RC): Make sure there\'s even a card here.
            Utils.reportSuccess(user, options, promise);
        }, function(error) {
            // TODO: Handle error.
            Utils.reportFailure(error, options, promise);
        });
        return promise;
    },
    login: function(username, password) {
      // Usernames are lowercase emails
      username = username.toLowerCase();

      Parse.Cloud.useMasterKey();
      var userQuery = new Parse.Query(Parse.User);
      userQuery.equalTo("username", username);
      return userQuery.first().then(function(user) {
        if (!user) {
          return Parse.Promise.error("User not found");
        }
        if (user.get("convert") == true) {
          return Parse.Promise.error("lockedOut");
        } else {
          return Parse.User.logIn(username, password).then(function(user) {
            return Parse.Promise.as(user);
          }, function(error) {
            var failedAttempts = user.get("failedLoginAttempts");
            if (failedAttempts == null) {
              failedAttempts = [];
            }

            failedAttempts = _.filter(failedAttempts, function(failedAttemptDate) {
              var _24hoursAgoInMs = 24 * 60 * 60 * 1000;
              var msSinceFailedAttempt = new Date - failedAttemptDate;
              return msSinceFailedAttempt < _24hoursAgoInMs;
            });

            if (failedAttempts.length < 10) {
              failedAttempts.push(new Date());
              user.set("failedLoginAttempts", failedAttempts);
            }
            if (failedAttempts.length >= 10) {
              user.set("lockedOut", true);
            }

            return user.save().always(function() {
              return Parse.Promise.error("Could not login.");
            });
          });
        }
      }, function(error) {
        console.log("Error trying to login " + JSON.stringify(error));
        return Parse.Promise.error("Could not login.");
      });
    },
    V2: {
      login: function(username, password) {
        // Usernames are lowercase emails
        username = username.toLowerCase();

        Parse.Cloud.useMasterKey();
        var userQuery = new Parse.Query(Parse.User);
        userQuery.equalTo("username", username);
        return userQuery.first().then(function(user) {
          if (!user) {
            console.log('User not found');
            return Parse.Promise.error(Error.badCredentials());
          } else {
            return Parse.Promise.as(user);
          }
        }).then(function(user) {
          return lockedOut(user)
        }).then(function(lockedOutInfo) {
          if (lockedOutInfo.user.get("lockedAt")) {
            var lockedOutError = Error.lockedOut();
            lockedOutError['minutes'] = lockedOutInfo.minutes;
            return Parse.Promise.error(lockedOutError);
          } else {
            return Parse.Promise.as(lockedOutInfo.user);
          }
        }).then(function(user) {
          return Parse.User.logIn(username, password).then(function(user) {
            user.set('failedLoginAttempts',[])
            return user.save().always(function(user) {
              return Parse.Promise.as({sessionToken: user.getSessionToken()});
            })
          }, function(error) {
            console.log("Bad password");
            var failedAttempts = user.get("failedLoginAttempts");
            if (failedAttempts == null) {
              failedAttempts = [];
            }
            failedAttempts.push(new Date());
            user.set("failedLoginAttempts", failedAttempts);
            return user.save().then(function(user) {
              return lockedOut(user).always(function(lockedOutInfo) {
                if (lockedOutInfo.user.get("lockedAt")) {
                  var lockedOutError = Error.lockedOut();
                  lockedOutError['minutes'] = lockedOutInfo.minutes;
                  return Parse.Promise.error(lockedOutError);
                } else {
                  return Parse.Promise.error(Error.badCredentials());
                }
              });
            }, function(error) {
              return Parse.Promise.error(Error.badCredentials());
            });
          });
        }, function(err) {
          console.log("Error trying to login " + JSON.stringify(err));
          var error;
          if (Error.isFlexDriveError(err)) {
            error = err;
          } else {
            error = Error.systemError();
            Error.addObjectToError(Error, err);
          }
          return Parse.Promise.error(error);
        });
      },
      resetPassword: function(email) {
        var userQuery = new Parse.Query(Parse.User);
        userQuery.equalTo("username", email.toLowerCase());
        return userQuery.first().then(function(user) {
          if (user) {
            return Parse.User.requestPasswordReset(user.get('email')).then(function() {
              user.set('failedLoginAttempts',[])
              user.set('lockedAt',null)
              return user.save().always(function(user) {
                return Parse.Promise.as();
              });
            }, function(error) {
              return Parse.Promise.error(Error.systemError());
            });
          } else {
            return Parse.Promise.error(Error.objectNotFoundError());
          }
        });
      },
      retrieve: function(parseUser, emailSnippet) {
        if ( !parseUser.get('admin') && !parseUser.get('allowODR'))  {
          return Parse.Promise.error(Error.unauthorizedUserError());
        }
        if (!emailSnippet || emailSnippet.length < 1) {
          return Parse.Promise.error(Error.invalidInputError());
        }
        var query = new Parse.Query(Parse.User);
        query.startsWith('username', emailSnippet.toLowerCase());
        query.ascending('username');
        query.limit(1000);
        return query.find().then(function(users) {
          var newList = _.map(users, function(user) {
              return user.get('email');
          });
          return Parse.Promise.as(newList);
        });
      },
      retrieveCompositeInfo: function(parseUser, email) {
        if (email) {
          if ( !parseUser.get('admin') && !parseUser.get('allowODR'))  {
            return Parse.Promise.error(Error.unauthorizedUserError());
          }
          Parse.Cloud.useMasterKey();
        } else {
          email = parseUser.get('username');
        }

        var userProfileImageUrl;
        var stripeCustomerId;
        var rentCentricCustomerId;
        var customerRunningInfo;
        var customerInfo;
        var rentalInfo;
        var vehicleDetailsInfo;
        var customerLocationID;
        var reservations;

        var query = new Parse.Query(Parse.User);
        query.equalTo('username', email.toLowerCase());
        return query.first().then(function(user) {
          if (user) {
            userProfileImageUrl = Data.Users.retrieveProfileImageUrl(user);
            stripeCustomerId = user.get(Data.Users.keys.stripeCustomerId);
            rentCentricCustomerId = user.get(Data.Users.keys.rentCentricCustomerId);
            return RentCentric.Customers.V2.retrieveConglomerateInfo(rentCentricCustomerId)
          } else {
            return Parse.Promise.error(Error.invalidInputError());
          }
        }).then(function(conglomerate) {
          customerRunningInfo = conglomerate.CustomerRunningInfo;
          customerInfo = customerRunningInfo.CustomerInfo;
          rentalInfo = customerRunningInfo.RentalInfo;
          vehicleDetailsInfo = customerRunningInfo.VehicleDetailsInfo;
          reservations = customerRunningInfo.Reservations;

          // Replace 'LocationID' with 'Market' to abstract RentCentric out.
          customerLocationID = customerInfo["LocationID"];
          delete customerInfo["LocationID"];
          delete customerInfo["Password"];
          customerInfo["LicenseNumber"] = "XXXXXXX";
          customerInfo[userProfileImageUrlKey] = userProfileImageUrl;

          // Set the rental isExpired flag
          var now = new Date();
          if (rentalInfo && rentalInfo.ReturnDate) {
            if (now.getTime() > rentalInfo.ReturnDate) {
              rentalInfo.Expired = true
            } else {
              rentalInfo.Expired = false
            }
          }

          Users_Internal.checkStartRenewal(rentalInfo);
          return Users_Internal.findCurrentReservation(customerRunningInfo).then(function(reservation) {

            // Remove all reservations
            customerRunningInfo.Reservations.length = 0;

            if (reservation) {
              customerRunningInfo.Reservations.push(reservation);

              var query = new Parse.Query('Billing');
              query.equalTo('reservationId', reservation.ReservationID);
              return query.first();
            } else {
              return Parse.Promise.as(null);
            }
          });
        }).then(function(billing) {
          // TODO change to persist in RC instead of injecting
          if (billing && reservations.length > 0) {
            if (billing.get('paymentError')) {
              reservations[0][Data.Billing.compositeKeys.PaymentProblem] = billing.get('paymentError');
            }
            if (billing.get(Data.Billing.keys.pending)) {
              reservations[0][Data.Billing.compositeKeys.PaymentInProgress] = billing.get(Data.Billing.keys.pending);
            }
            if (billing.get('paid')) {
              reservations[0][paid] = true;
            } else {
              reservations[0][paid] = false;
            }
            if (billing.get('stripeCardId')) {
              reservations[0]['CardID'] = billing.get('stripeCardId');
            }
          }
          return Data.Markets.marketForID(customerLocationID);
        }).then(function(customerMarket) {
          if (customerMarket) {
            customerInfo[preferredMarketKey] = customerMarket;
          }

          if (vehicleDetailsInfo && vehicleDetailsInfo.ParentLocation.LocationID &&
            vehicleDetailsInfo.ParentLocation.LocationID != customerLocationID) {
              return Data.Markets.marketForID(vehicleDetailsInfo.ParentLocation.LocationID);
          } else {
            return Parse.Promise.as(customerMarket);
          }
        }).then(function(vehicleMarket) {
          if (vehicleDetailsInfo && vehicleMarket) {
            vehicleDetailsInfo[marketKey] = vehicleMarket;
          }

          if (rentalInfo && rentalInfo.PickupLocationName) {
              return Data.Markets.marketForLocationName(rentalInfo.PickupLocationName);
          } else {
            return Parse.Promise.as(vehicleMarket);
          }
        }).then(function(rentalMarket) {
          if (rentalInfo && rentalMarket) {
            rentalInfo[marketKey] = rentalMarket;
          }
          return stripe.customers.retrieve(stripeCustomerId);
        }).then(function(stripeCustomer) {
          if (stripeCustomer && stripeCustomer.cards &&
              stripeCustomer.cards.data) {
            // Left in for backwards compatability
            if (stripeCustomer.cards.data[0]) {
              customerInfo[userPaymentInfoKey] = stripeCustomer.cards.data[0];
            }

            var cards = stripeCustomer.sources.data;
            var defaultCardId = stripeCustomer.default_source;
            FlexdriveStripe.Payments.prepareCardsJSONForClient(cards, defaultCardId)
            customerInfo[userPaymentsInfoKey] = cards;
          }

          if (vehicleDetailsInfo) {
            var injectDeposits = true;
            if (rentalInfo) {
              // Do not inject deposit if there is already a rental
              injectDeposits = false;
            }

            var expired = false;
            if (rentalInfo && rentalInfo.Expired) {
              expired = true;
            }

            return Vehicles_Internal.v2_injectPricingIntoVehicle(vehicleDetailsInfo, customerRunningInfo.RentalCount, injectDeposits, expired);
          } else {
            return Parse.Promise.as();
          }
        }).then(function() {
          return Parse.Promise.as(customerRunningInfo);
        }, function(err) {
          var error;
          if (Error.isFlexDriveError(err)) {
            error = err;
          } else {
            error = Error.systemError();
            Error.addObjectToError(Error, err);
          }
          return Parse.Promise.error(error);
        });
      },
      create: function(paymentToken, customerInfo) {
        var parseUser;

        if (!customerInfo.acceptedMembershipAgreement) {
          return Parse.Promise.error(Error.registrationAgreementError());
        }

        var query = new Parse.Query(Parse.User);
        query.equalTo('username', customerInfo.email.toLowerCase());
        return query.first().then(function(_parseUser) {
          if (_parseUser) {
            return Parse.Promise.error(Error.usernameTakenError());
          }
          var user = new Parse.User();
          user.set("username",customerInfo.email.toLowerCase());
          user.set("password", customerInfo.password);
          user.set("email", customerInfo.email);
          if (customerInfo.billingAddress) {
            user.set("billingAddress", JSON.stringify(customerInfo.billingAddress));
          }
          user.set("acceptedMembershipAgreement", (new Date()));
          user.set(Data.Users.keys.driversLicenseValidationStatus,Data.Users.keys.validationPending);
          var acl = new Parse.ACL();
          acl.setPublicReadAccess(false);
          acl.setPublicWriteAccess(false);
          user.setACL(acl);
          return user.signUp().fail(function(error) {
            if (error.code && error.code == 125 && error.message && error.message == "invalid email address") {
              return Parse.Promise.error(Error.invalidEmailError());
            } else {
              return Parse.Promise.error(error);
            }
          });
        }).then(function(_parseUser) {
          parseUser = _parseUser;
          return RentCentric.Customers.V2.create(customerInfo);
        }).then(function(rentCentricCustomerId) {
          parseUser.set(Data.Users.keys.rentCentricCustomerId, rentCentricCustomerId);
          return parseUser.save();
        }).then(function(_parseUser) {
          parseUser = _parseUser;
          return Users_Internal.createStripeAccount(parseUser, paymentToken).catch(function(error) {
            if (error.system && error.system == 'Stripe' && error.stripeError && error.stripeError.message && error.stripeError.message.indexOf('You cannot use a Stripe token more than once') != -1) {
              return Parse.Promise.error(Error.tokenAlreadyUsedStripeError());
            } else {
              return Parse.Promise.error(error);
            }
          });
        }).then(function(stripeCustomer) {
          parseUser.set(Data.Users.keys.stripeCustomerId, stripeCustomer[stripeCustomerIdKey]);
          return parseUser.save();
        }).then(function(_parseUser) {
          parseUser = _parseUser;
          var customerData = {
              id: parseUser.get(Data.Users.keys.rentCentricCustomerId),
              firstName: customerInfo.firstName,
              lastName: customerInfo.lastName
          };
          return Mail.sendNewUserRegistrationApplication(parseUser, customerData).always(function() {
            return Parse.Promise.as("successs");
          });
        }, function(error) {
          if (parseUser) {
            var stripeId = parseUser.get('Data.Users.keys.stripeCustomerId');
            return parseUser.destroy().always(function() {
              if (stripeId) {
                FlexdriveStripe.Customers.V2.delete(stripeId).always(function() {
                  return Parse.Promise.error(error);
                });
              } else {
                return Parse.Promise.error(error);
              }
            });
          } else {
            return Parse.Promise.error(error);
          }
        });
      },
      updateEmail: function(parseUser, email) {
        var rentCentricCustomerId = parseUser.get(Data.Users.keys.rentCentricCustomerId);
        var stripeCustomerId = parseUser.get(Data.Users.keys.stripeCustomerId);
        if (!stripeCustomerId || !rentCentricCustomerId) {
          return Parse.Promise.error(Error.systemError());
        }

        var oldEmail = parseUser.get('email');
        var oldUsername = parseUser.get('username')

        parseUser.set('email',email);
        parseUser.set('username',email.toLowerCase());
        return parseUser.save().then(function() {
          return RentCentric.Customers.V2.retrieve(rentCentricCustomerId).then(function(customer) {
            customer['Email'] = email;
            return RentCentric.Customers.V2.update(customer);
          }).then(function() {
            return stripe.customers.update(stripeCustomerId, {email: email});
          }).then(function() {
            return Parse.Promise.as();
          }, function(err) {
            return stripe.customers.update(stripeCustomerId, {email: oldEmail}).always(function() {
              return RentCentric.Customers.V2.retrieve(rentCentricCustomerId);
            }).always(function(customer) {
              if (customer) {
                customer['Email'] = oldEmail;
                return RentCentric.Customers.V2.update(customer);
              } else {
                return Parse.Promise.as();
              }
            }).always(function() {
              parseUser.set('email',oldEmail);
              parseUser.set('username',oldEmail.toLowerCase());
              return parseUser.save();
            }).always(function() {
              var error = Error.systemError();
              Error.addObjectToError(error, err);
              return Parse.Promise.as(error)
            });
          });
        }, function(err) {
          var error;
          if (Error.isFlexDriveError(err)) {
            error = err;
          } else {
            // Check parse errors
            if (err.code && err.code == 202) {
              return Parse.Promise.error(Error.usernameTakenError())
            } else if (err.code && err.code == 125) {
              return Parse.Promise.error(Error.invalidEmailError())
            } else {
              // Default to system error
              error = Error.systemError();
              Error.addObjectToError(Error, err);
              return Parse.Promise.error(error);
            }
          }
        });
      },
      createTestUser: function(stripeUserId, rcUserId, email, password) {
        return RentCentric.Customers.V2.retrieveConglomerateInfo(rcUserId).then(function(rcUser) {
          var newUser = new Parse.User();
          newUser.set('username',email);
          newUser.set('email',email);
          newUser.set('password',password);
          newUser.set('acceptedTermsConditionsV1',true);
          newUser.set('acceptedPrivacyPolicyV1',true);
          newUser.set('customerIDRentCentric',rcUserId);
          newUser.set('customerIDStripe',stripeUserId);
          newUser.set('licenseCheckStatus','pass');
          return newUser.save();
        }, function(error) {
          console.log('Create test user error: ' + JSON.stringify(error));
          return Parse.Promise.error(Error.systemError());
        });
      },
      clean: function() {
        var query = new Parse.Query(Parse.User);
        query.doesNotExist('customerIDRentCentric');
        query.limit(1000);
        return query.find().then(function(users) {
          var chainedPromise = Parse.Promise.as();
          _.each(users, function(user) {
            chainedPromise = chainedPromise.then(function() {
              if (user.get('username').indexOf('failed_') == -1 && user.get('admin') == null && user.get('allowBilling') == null && user.get('allowODR') == null && user.get('allowStripeReporting') == null) {
                console.log('Dead user ' + user.get('username'));
                var username = user.get('username');
                if (username.indexOf('@')) {
                  username = username.replace('@','AT');
                }
                var now = new Date();
                var newUsername = 'failed_' + username + '_' + now.getTime() + '@failed.com';
                var newEmail = 'failed_' + username + '_' + now.getTime() + '@failed.com';
                user.set('username',newUsername);
                user.set('email',newEmail);
                return user.save();
              } else {
                return Parse.Promise.as();
              }
            });
          });
          return chainedPromise;
        });
      }
    }
  },

  Locations: {
    V2: {
      getRates: function(vehicleType, locationId) {
        var query = new Parse.Query('Markets');
        query.equalTo('MarketID', locationId);
        return query.first().then(function(market) {
          var ratesToReturn = [];
          var rates = market.get('rates');
          if (vehicleType) {
            _.each(rates, function(rate) {
              var vehicleRateType = Utils.getRateVehicleType(vehicleType, rate.RateVehicleTypes);
              if (vehicleRateType) {
                rate.RateVehicleTypes.length = 0;
                rate.RateVehicleTypes.push(vehicleRateType);
                ratesToReturn.push(rate);
              }
            });
          } else {
            ratesToReturn = rates;
          }
          return Parse.Promise.as(ratesToReturn);
        });
      }
    }
  },

  Vehicles: {
    anonymousList: function(marketId, version, index, size, options) {
        var promise = new Parse.Promise();
        var includeMarket = version ? (version >= 2) : false;
        this.list(marketId, version, index, size, options).then(function(obj) {
            if (includeMarket) {
              obj.Vehicles = _.map(obj.Vehicles, function(vehicle) {
                  return Vehicles_Internal.anonymizeVehicle(vehicle);
              });
            } else {
              obj = _.map(obj, function(vehicle) {
                  return Vehicles_Internal.anonymizeVehicle(vehicle);
              });
            }
            Utils.reportSuccess(obj, options, promise);
        }, function(error) {
            Utils.reportFailure(error, options, promise);
        });
        return promise;
    },
    list: function(marketId, version, index, size, options) {
        var promise = new Parse.Promise();
        var id = marketId ? marketId : rentCentricLocationId.atlanta;
        var injectMarkets = version ? (version >= 2) : false;
        RentCentric.Vehicles.list(id, index, size).then(function(vehicles) {
            var vehicles = _.map(vehicles, function(vehicle) {
                return Vehicles_Internal.removeCustomerNamesFromVehicle(vehicle);
            });
            return Vehicles_Internal.injectUserProfileImageUrlIntoVehicles(vehicles);
        }).then(function(vehicles) {
            if (injectMarkets) {
              Data.Markets.combineMarketWithVehicles(id, vehicles).then(function(obj){
                Utils.reportSuccess(obj, options, promise);
              }, function(err){
                Utils.reportFailure(err, options, promise);
              });
            } else {
              Utils.reportSuccess(vehicles, options, promise);
            }
        }, function(error) {
            Utils.reportFailure(error, options, promise);
        });
        return promise;
    },
    anonymousRetrieve: function(vehicleId, deviceType, options) {
      var promise = new Parse.Promise();
      this.retrieve(vehicleId, null, deviceType, options).then(function(vehicle) {
        vehicle = Vehicles_Internal.anonymizeVehicle(vehicle);
        Utils.reportSuccess(vehicle, options, promise);
      }, function(error) {
        Utils.reportFailure(error, options, promise);
      });
      return promise;
    },
    retrieve: function(vehicleId, user, deviceType, options) {
        var promise = new Parse.Promise();
        RentCentric.Vehicles.retrieve(vehicleId).then(function(vehicle) {
            return Vehicles_Internal.injectDetailsIntoVehicle(vehicle, user, deviceType).then(function(injectedVehicle) {
                return Parse.Promise.as(injectedVehicle);
            }, function(error) {
                return Parse.Promise.as(vehicle);
            });
        }).then(function(vehicle) {
            vehicle = Vehicles_Internal.removeCustomerNamesFromVehicle(vehicle);
            return Vehicles_Internal.transformLocationToMarketForVehicle(vehicle);
        }).then(function(vehicle) {
            Utils.reportSuccess(vehicle, options, promise);
        }, function(error) {
            Utils.reportFailure(error, options, promise);
        });
        return promise;
    },
    reserve: function(customerId, vehicleId, options) {
    },
    V2: {
      retrieve: function(user, vehicleId, rentalCount) {
        return RentCentric.Vehicles.V2.retrieve(vehicleId).then(function(vehicle) {
          return Vehicles_Internal.v2_injectDetailsIntoVehicle(vehicle, rentalCount);
        }).then(function(vehicle) {
          vehicle = Vehicles_Internal.removeCustomerNamesFromVehicle(vehicle);
          if (!user || user.get('licenseCheckStatus') == Data.Users.keys.validationPending) {
            Vehicles_Internal.anonymizeVehicle(vehicle);
          }
          return Vehicles_Internal.transformLocationToMarketForVehicle(vehicle);
        }).then(function(vehicle) {
          return Parse.Promise.as(vehicle);
        }, function(error) {
          return Parse.Promise.error(error);
        });
      },
      list: function(user, marketId, index, size) {
        var vehiclePage;
        return RentCentric.Vehicles.V2.list(marketId, index, size).then(function(vehicleInfo) {
          vehiclePage = vehicleInfo;

          // Remove middle and last name
          var vehicles = _.map(vehicleInfo.vehicles, function(vehicle) {
              return Vehicles_Internal.removeCustomerNamesFromVehicle(vehicle);
          });
          if (!user || user.get('licenseCheckStatus') == Data.Users.keys.validationPending) {
            // Change the first name
            _.map(vehicleInfo.vehicles, function(vehicle) {
                return Vehicles_Internal.anonymizeVehicle(vehicle);
            });
          }

          //;;!! hack FLXS-318
          _.each(vehicleInfo.vehicles, function(vehicle) {
            if (vehicle.Status == "Available") {
              vehicle.VehicleID = '00' + vehicle.VehicleID;
            }
          });


          if (!user || user.get('licenseCheckStatus') == Data.Users.keys.validationPending) {
            return Parse.Promise.as();
          } else {
            return Vehicles_Internal.injectUserProfileImageUrlIntoVehicles(vehicleInfo.vehicles);
          }
        }).then(function() {
          return Parse.Promise.as(vehiclePage);
        }, function(err) {
          var error;
          if (Error.isFlexDriveError(err)) {
            error = err;
          } else {
            error = Error.systemError();
            Error.addObjectToError(Error, err);
          }
          return Parse.Promise.error(error);
        });
      },
    },
    Swaps: {
      create: function(data, options) {
          // TODO: Check input.
          if (data.sender.id == data.receiver.id) {
              return Parse.Promise.error(cannotSwapWithYourselfErrorCopy);
          }
          Parse.Cloud.useMasterKey(); // TODO: Is this necessary?
          var promise = new Parse.Promise();
          Data.SwapRequests.userHasActiveRequest(data.sender).then(function(hasActiveRequest) {
              if (hasActiveRequest) {// Do not let user create a new request.
                  return Parse.Promise.error(cannotCreateSwapRequestErrorCopy);
              } else {
                  return Swaps_Internal.create(data);
              }
          }).then(function(swapRequest) {
              Utils.reportSuccess(swapRequest, options, promise);
          }, function(error) {
              Utils.reportFailure(error, options, promise);
          });
          return promise;
      },
      respond: function(swapId, data, options) {
          // TODO: Validate input.
          Parse.Cloud.useMasterKey();
          var promise = new Parse.Promise();
          Data.SwapRequests.retreive(swapId).then(function(swapRequest) {
              var receiver = swapRequest.get(Data.SwapRequests.keys.receiver);
              if (data.responder.id != receiver.id) {// Responder is not the swap receiver, abort.
                  return Parse.Promise.error(cannotRespondToSomoneElsesSwapRequest);
              } else {
                  return Data.SwapRequests.updateWithResponse(swapRequest, data.response);
              }
          }).then(function(updatedSwapRequest) {
              // TODO: In future release, rollback Swap Request updates if email fails to send.
              //  If this occurs with the current logic, FlexDrive will never see the swap response.
              // TODO: In future release, return a human readable error if cannot send email to FlexDrive.
              return Data.SwapRequests.fetchRelatedObjects(updatedSwapRequest).then(function() {
                  // TODO: In future release, check if both customer's current rentals' vehicles match the vehicle Ids in the SwapRequest record.
                  //   If they don't match either/both customer's have switched vehicles since creating the swap request.
                  var sender = updatedSwapRequest.get(Data.SwapRequests.keys.sender);
                  var senderCustomerId = sender.get(Data.Users.keys.rentCentricCustomerId);
                  return RentCentric.Customers.retrieve(senderCustomerId).then(function(senderCustomer) {
                      var receiver = updatedSwapRequest.get(Data.SwapRequests.keys.receiver);
                      var receiverCustomerId = receiver.get(Data.Users.keys.rentCentricCustomerId);
                      return RentCentric.Rentals.retrieveActive(senderCustomerId).then(function(senderRental) {
                          return RentCentric.Customers.retrieve(receiverCustomerId).then(function(receiverCustomer) {
                              return RentCentric.Rentals.retrieveActive(receiverCustomerId).then(function(receiverRental) {
                                  var senderRentalNumber = '[unknown]';
                                  if (senderRental) {
                                      senderRentalNumber = senderRental[RentCentric.Rentals.keys.rentalNumber];
                                  }
                                  var receiverRentalNumber = '[unknown]';
                                  if (receiverRental) {
                                      receiverRentalNumber = receiverRental[RentCentric.Rentals.keys.rentalNumber];
                                  }
                                  var senderData = {
                                      customer: {
                                          id: senderCustomer[RentCentric.Customers.keys.customerId],
                                          firstName: senderCustomer[RentCentric.Customers.keys.firstName],
                                          lastName: senderCustomer[RentCentric.Customers.keys.lastName]
                                      },
                                      rentalNumber: senderRentalNumber
                                  };
                                  var receiverData = {
                                      customer: {
                                          id: receiverCustomer[RentCentric.Customers.keys.customerId],
                                          firstName: receiverCustomer[RentCentric.Customers.keys.firstName],
                                          lastName: receiverCustomer[RentCentric.Customers.keys.lastName]
                                      },
                                      rentalNumber: receiverRentalNumber
                                  };
                                  return Mail.sendSwapAgreement(updatedSwapRequest, senderData, receiverData).then(function() {
                                      // TODO: Consider placing notification create logic inside Push.Or helper method in here.
                                      // TODO: Consider emailing FlexDrive the error condition, explain how they can fix it.
                                      return Data.Notifications.createForSwap(updatedSwapRequest).then(function(notification) {
                                          Notifications.Push.sendSwapNotification(notification).always(function() {
                                              Utils.reportSuccess(updatedSwapRequest, options, promise);
                                          });
                                      });
                                  });
                              });
                          });
                      });
                  });
              });
          }).fail(function(error) {
              Utils.reportFailure(error, options, promise);
          });
          return promise;
      },
      deactivateOldRequests: function(options) {
          Parse.Cloud.useMasterKey();
          var promise = new Parse.Promise();
          Data.SwapRequests.listOldActive().then(function(oldActiveSwapRequests) {
              var chainedPromise = Parse.Promise.as();
              _.each(oldActiveSwapRequests, function(swapRequest) {
                 chainedPromise = chainedPromise.always(function() {
                     return Data.SwapRequests.deactivate(swapRequest);
                 });
              });
              return chainedPromise;
          }).then(function() {
              Utils.reportSuccess(null, options, promise);
          }, function(error) {
              Utils.reportFailure(error, options, promise);
          });
          return promise;
      }
    },
    ReservationRequests: {
        create: function(data, options) {
            // TODO: Validate input.
            var promise = new Parse.Promise();
            Data.ReservationRequests.create(data).then(function(reservationRequest) {
                return RentCentric.Vehicles.retrieve(reservationRequest.get(Data.ReservationRequests.keys.vehicleId)).then(function(vehicle) {
                    var vehicleData = {
                        id: vehicle[RentCentric.Vehicles.keys.assignedId],
                        year: vehicle[RentCentric.Vehicles.keys.year],
                        make: vehicle[RentCentric.Vehicles.keys.make],
                        model: vehicle[RentCentric.Vehicles.keys.model],
                        color: vehicle[RentCentric.Vehicles.keys.exteriorColor]
                    };
                    return Data.ReservationRequests.fetchRelatedObjects(reservationRequest).then(function() {
                        var customerId = reservationRequest.get(Data.ReservationRequests.keys.user).get(Data.Users.keys.rentCentricCustomerId);
                        return RentCentric.Customers.retrieve(customerId).then(function(customer) {
                            var customerData = {
                                id: customer[RentCentric.Customers.keys.customerId],
                                firstName: customer[RentCentric.Customers.keys.firstName],
                                lastName: customer[RentCentric.Customers.keys.lastName]
                            };
                            return Mail.sendReservationRequest(reservationRequest, vehicleData, customerData);
                        });
                    });
                }).fail(function(error) {
                    var rollbackPromise = new Parse.Promise();
                    reservationRequest.destroy().always(function() {
                        rollbackPromise.reject(error); // Re-throw after rollback.
                    });
                    return rollbackPromise;
                });
            }).then(function(reservationRequest) {
                Utils.reportSuccess(reservationRequest, options, promise);
            }, function(error) {
                Utils.reportFailure(error, options, promise);
            });
            return promise;
        },
        hasReservation: function(user, vehicleId) {
          return Data.ReservationRequests.retrieve(user, vehicleId).then(function(reservation) {
            if (reservation) {
              return Parse.Promise.as(true);
            } else {
              return Parse.Promise.as(false);
            }
          });
        },
        deleteOld: function(numberOfDaysOld) {
          return Data.ReservationRequests.list().then(function(reservations) {
            // Calculate reference date
            var now = new Date();
            var milliSecondsInDay = 24 * 60 * 60 * 1000;
            var referenceDate = new Date(now.getTime() - (numberOfDaysOld * milliSecondsInDay));
            // Parallel promises
            var promises = [];
            // Delete old reservations
            _.each(reservations, function(reservation) {
              if (reservation.createdAt < referenceDate) {
                  promises.push(reservation.destroy());
              }
            });
            return Parse.Promise.when(promises).always(function() {
              return Parse.Promise.as(true);
            });
          });
        }
    },
    Rentals: {
        sendRenewalRequest: function(user, options) {
            var promise = new Parse.Promise();
            var customerId = user.get(Data.Users.keys.rentCentricCustomerId);
            RentCentric.Customers.retrieve(customerId).then(function(customer) {
                return RentCentric.Rentals.retrieveActive(customerId).then(function(rental) {
                    var firstName = customer[RentCentric.Customers.keys.firstName];
                    var lastName = customer[RentCentric.Customers.keys.lastName];
                    var data = {
                        customerData: {
                            id: customerId,
                            firstName: firstName,
                            lastName: lastName
                        },
                        rentalData: {
                            rentalNumber: rental.rentalNumber
                        }
                    };
                    return Mail.sendRentalRenewalRequest(user, data);
                });
            }).then(function() {
                Utils.reportSuccess(null, options, promise);
            }, function(error) {
                Utils.reportFailure(error, options, promise);
            });
            return promise;
        },
        retrieveActiveVehicleForUser: function(user, options) {
            var promise = new Parse.Promise();
            var customerId = user.get(Data.Users.keys.rentCentricCustomerId);
            RentCentric.Rentals.retrieveActive(customerId).then(function(rental) {
                if (!rental || !rental.vehicleId) {
                    return Parse.Promise.as();
                } else {
                    return RentCentric.Vehicles.retrieve(rental.vehicleId);
                }
            }).then(function(vehicle) {
                if (vehicle) {
                    return Vehicles_Internal.transformLocationToMarketForVehicle(vehicle);
                } else {
                    return Parse.Promise.as();
                }
            }).then(function(vehicle){
                Utils.reportSuccess(vehicle, options, promise);
            }, function(error) {
                // If there's no rental, the Rent Centric get active Rental errors out.
                // TODO: Is this the correct logic?
                Utils.reportSuccess(null, options, promise);
            });
            return promise;
        },
        sendTimeToRenewPushes: function(renewals) {
          var chainedPromise = Parse.Promise.as();

          console.log("Number of Renewals: " + renewals.length)
          _.each(renewals, function(renewal) {
            var rentCentricCustomerId = renewal.rentCentricCustomerId;
            var rentalId = renewal.rentalId;

            if (!rentCentricCustomerId || !rentalId) {
              console.log("Invalid Renewal - rentCentricCustomerId: " + rentCentricCustomerId + " RentalId: " + rentalId)
              return Parse.Promise.as()
            } else {
              chainedPromise = chainedPromise.then(function () {
                return sendTimeToRenewPush(rentCentricCustomerId, rentalId);
              });
            }
          });

          return chainedPromise;
        },
        // Assumes input checked to be no undefined and not null
        validateDriverLicense: function(rentCentricCustomerId, flag) {
          var parseUser;

          return Data.Users.v2_retrieveWithRentCentricCustomerId(rentCentricCustomerId).then(function(user) {
            return Data.Users.v2_markHasValidDriversLicense(user, flag);
          }).then(function(user) {
            if (flag) {
              parseUser = user
              var notification = Data.Notifications.createForMembershipApproved(user);
              return notification.save();
            } else {
              //console.log("No push or email necessary when validation is set to false.");
              return Parse.Promise.as();
            }
          }).then(function(notification) {
            if (notification) {
              var promises = []
              promises.push(sendMembershipApprovalPush(parseUser, notification));
              promises.push(sendMembershipApprovalEmail(parseUser, notification, rentCentricCustomerId));

              return Parse.Promise.when(promises)
            } else {
              return Parse.Promise.as()
            }
          }, function(err) {
            console.error(JSON.stringify(error));
            if (Error.isFlexDriveError(err)) {
              return Parse.Promise.error(err);
            } else {
              var error = Error.systemError();
              Error.addObjectToError(error, err);
              return Parse.Promise.error(error);
            }
          });
        }
    },
  },

  Billings: {
    pending: function() {
      return Billing.getPendingBillings().then(function(billings) {
        var chainedPromise = Parse.Promise.as();
        _.each(billings, function(billing) {
          chainedPromise = chainedPromise.then(function() {
            return getReservationForReservation(null, billing.get('reservationId')).then(function(reservation) {
              return runBillingForReservation(reservation, true);
            }, function(error) {
              var errorToLog = error;
              if (Error.isFlexDriveError(error) == false) {
                errorToLog = Error.systemError();
                Error.addObjectToError(errorToLog, error);
              }
              return Log.logFlexDriveError(errorToLog, null, null, "BillingPending", null, null, null, null);
            });
          });
        });
        return chainedPromise;
      });
    },
  },

  // Reservations
  Reservations: {
    notifyUsersOfPayment: function(billings) {
      return getBillingsForPaymentNotifications(billings).then(function(billings) {
        console.log("# of Billings: " + billings.length)
        var chainedPromises = []

        _.each(billings, function(billing, index) {
          var paidDate = billing.get('paid');
          var paymentError = billing.get('paymentError');
          var reservationId = billing.get('reservationId');
          var customerIDRentCentric = billing.get('customerIDRentCentric');
          var resendNotification = billing.get('resendNotifications');

          var shouldResendNotification = false;
          if (resendNotification == true) {
            shouldResendNotification = true;
            billing.set("resendNotifications", false)
            chainedPromises.push(Utils.saveAndIgnoreErrorForObject(billing))
          }

          var reservation = JSON.parse(billing.get('reservation'));
          var timeZone = JSON.parse(billing.get("timezone"));
          var startDate = Utils.getDateForTimezone(new Date(parseInt(reservation.DateOut)), timeZone);
          var endDate = Utils.getDateForTimezone(new Date(parseInt(reservation.DateIn)), timeZone);

          if (customerIDRentCentric && reservationId && startDate && endDate && (paidDate || paymentError)) {
            chainedPromises.push(sendPaymentNotificationForRentcentricId(customerIDRentCentric, reservationId, paidDate, paymentError, startDate, endDate, shouldResendNotification));
          } else {
            //console.log("Skipping non payment billing: RentCentric ID: " + customerIDRentCentric + " reservationId: " + reservationId + " paidDate: " + paidDate + " paymentError: " + paymentError);
          }
        });

        return Parse.Promise.when(chainedPromises);
      }, function(error) {
        return Parse.Promise.as();
      });
    },
    V2: {
      cancel: function(user, reservationId) {
        var rentCentricCustomerId = user.get(Data.Users.keys.rentCentricCustomerId);
        return Billing.get(reservationId).then(function(billing) {
          if (billing && billing.get('paid')) {
            var error = Error.reservationCannotCancelAlreadyPaidError();
            Error.addObjectToError(error, { 'billing' : billing});
            return Parse.Promise.error(error);
          } else if (billing && billing.get('pending')) {
            var error = Error.reservationCannotCancelPaymentInProgressError();
            Error.addObjectToError(error, { 'billing' : billing});
            return Parse.Promise.error(error);
          } else {
            return RentCentric.Reservations.V2.cancel(rentCentricCustomerId,  reservationId).fail(function(error) {
              return Parse.Promise.error(error);
            });
          }
        });
      },
      delete: function(user, reservationId) {
        var rentCentricCustomerId = user.get(Data.Users.keys.rentCentricCustomerId);
        return Billing.get(reservationId).then(function(billing) {
          if (billing && billing.get('paid')) {
            var error = Error.reservationCannotDeleteAlreadyPaidError();
            Error.addObjectToError(error, { 'billing' : billing});
            return Parse.Promise.error(error);
          } else if (billing && billing.get('pending')) {
            var error = Error.reservationCannotDeletePaymentInProgressError();
            Error.addObjectToError(error, { 'billing' : billing});
            return Parse.Promise.error(error);
          } else {
            return RentCentric.Reservations.V2.delete(rentCentricCustomerId,  reservationId).fail(function(error) {
              return Parse.Promise.error(error);
            });
          }
        });
      },
      createRenewal: function(user, rentalId, rateId, rateVehicleTypesId, days, payNow, expired, password) {
        return Utils.verifyPassword(user, password).always(function(result) {
          if (result === 'false') {
            return Parse.Promise.error(Error.invalidPassword());
          } else {
            return Parse.Promise.as()
          }
        }).then(function() {
          return module.exports.Users.V2.retrieveCompositeInfo(user)
        }).then(function(composite) {
          if (!composite.RentalInfo) {
            console.log('Cannot create renewal reservation because rental has been deleted or closed');
            var error = Error.rentalNotFoundError();
            return Parse.Promise.error(error);
          }
          if (composite.Reservations && composite.Reservations.length > 0) {
            console.log('Cannot create renewal reservation because reservation already exists');
            var error = Error.reservationAlreadyExistsForRentalError();
            return Parse.Promise.error(error);
          }
          return RentCentric.Reservations.V2.createRenewal(rentalId, rateId, rateVehicleTypesId, days)
        }).then(function(reservation) {
          if (payNow || isSameDayReservation(reservation) || isExpiredReservation(reservation)) {
            return Billing.createBilling(reservation, true, false, expired).always(function() {
              return Parse.Promise.as({reservationId:reservation.ReservationID});
            });
          } else {
            return Parse.Promise.as({reservationId:reservation.ReservationID});
          }
        }, function(error) {
          if (Error.isFlexDriveError(error)) {
            return Parse.Promise.error(error);
          } else {
            var rcError = Error.systemError();
            Error.addObjectToError(rcError, error);
            return Parse.Promise.error(rcError);
          }
        });
      },
      create: function(user, locationId, offsiteLocationId, assignedVehicleId, vehicleId, rateId, rateVehicleTypesId, reservationType, days, expectedPickup, payNow, password) {

        var location;
        var parentLocation;
        var reservation;

        return Utils.verifyPassword(user, password).always(function(result) {
          if (result === 'false') {
            return Parse.Promise.error(Error.invalidPassword());
          } else {
            return Parse.Promise.as()
          }
        }).then(function() {
          var query = new Parse.Query('Markets');
          query.equalTo('MarketID', locationId);
          return query.first()
        }).then(function(parent) {
          parentLocation = parent;

          if (offsiteLocationId) {
            query = new Parse.Query('OffsiteLocation');
            query.equalTo('MarketID', offsiteLocationId);
          } else {
            query = new Parse.Query('Markets');
            query.equalTo('MarketID', locationId);
          }
          return query.first()
        }).then(function(market) {
          // If market is not set then the offsite ID is really a market id
          if (!market) {
            query = new Parse.Query('Markets');
            query.equalTo('MarketID', locationId);
            return query.first();
          } else {
            return Parse.Promise.as(market);
          }
        }).then(function(market) {
          // Assumes that the market will be found so no check for null
          location = JSON.parse(market.get('location'));
          // Default to end of next business day
          return getNextBusinessDay(location.HoursOfOperation, location.Holidays, location.Timezone);
        }).then(function(nextDayTZ) {
          return getBusinessHours(nextDayTZ, null, location.HoursOfOperation);
        }).then(function(hours) {

          // FLXS-376 (big time hack) Add the hours that differ for the market form EST
          var offsetHack = parentLocation.get('TzOffsetFromEST');

          var expectedPickupDate = new Date(expectedPickup.year, (expectedPickup.month -1), expectedPickup.day, (expectedPickup.hour + offsetHack), expectedPickup.minute);
          expectedPickupDate = Utils.convertDateFromTZToGMT(expectedPickupDate, location.Timezone);

          var startDate;
          if (hours) {
            var indexOfColon = hours.end.indexOf(":");
            var endHour = hours.end.substring(0,indexOfColon);
            startDate = new Date(hours.date.year, (hours.date.month -1), hours.date.day, endHour);
            startDate = Utils.convertDateFromTZToGMT(startDate, location.Timezone);
          } else {
            startDate = expectedPickupDate;
          }

          var pickupLocationId = offsiteLocationId ? offsiteLocationId : locationId;
          var dropoffLocationId = offsiteLocationId ? offsiteLocationId : locationId;
          return RentCentric.Reservations.V2.create(user.get('customerIDRentCentric'), locationId, pickupLocationId, dropoffLocationId, assignedVehicleId, vehicleId, rateId, rateVehicleTypesId, reservationType, days, startDate, expectedPickupDate);
        }).then(function(reservationId) {
          // ;;!! remove this once FLRC-111 is done
          return getReservationForReservation(null, reservationId);
        }).then(function(res) {
          reservation = res;
          if (payNow) {
            return Billing.createBilling(reservation, true, true, false).always(function() {
              return Parse.Promise.as();
            });
          } else {
            return Parse.Promise.as();
          }
        }).then(function() {
          return Parse.Promise.as({reservationId:reservation.ReservationID});
        }, function(error) {
          if (Error.isFlexDriveError(error)) {
            return Parse.Promise.error(error);
          } else {
            var rcError = Error.unknownRentCentricError();
            Error.addObjectToError(rcError, error);
            return Parse.Promise.error(rcError);
          }
        });
      },
      update: function(user, reservationId, rentalId, rateId, rateVehicleTypesId, days, start, payNow, expired, password) {

        return Utils.verifyPassword(user, password).always(function(result) {
          if (result === 'false') {
            return Parse.Promise.error(Error.invalidPassword());
          } else {
            return Parse.Promise.as()
          }
        }).then(function() {
          if (rentalId) {
            //;;!! don't need composite just need to know if the rental is valid
            //;;!! see if then call to get rental or get active rental will do instead
            //;;!! or call RC running info to avoid injection into composite
            return module.exports.Users.V2.retrieveCompositeInfo(user).then(function(composite) {
              if (!composite.RentalInfo) {
                console.log('Cannot create renewal reservation because rental has been deleted or closed');
                var error = Error.rentalNotFoundError();
                return Parse.Promise.error(error);
              }
              return updateReservation(reservationId, rateId, rateVehicleTypesId, days, start, null, expired, payNow);
            }, function(error) {
              if (Error.isFlexDriveError(error)) {
                return Parse.Promise.error(error);
              } else {
                var rcError = Error.systemError();
                Error.addObjectToError(rcError, error);
                return Parse.Promise.error(rcError);
              }
            });
          } else {
            return updateReservation(reservationId, rateId, rateVehicleTypesId, days, start, null, expired, payNow);
          }
        })
      },
      convertToRental: function(user, reservationId, start) {
        if ( !user.get('admin') && !user.get('allowODR'))  {
          return Parse.Promise.error(Error.unauthorizedUserError());
        }

        var query = new Parse.Query('Billing');
        query.equalTo('reservationId', reservationId);
        return query.first().then(function(billing) {
          if (!billing || !billing.get('paid')) {
            return Parse.Promise.error(Error.reservationNotPaidError());
          } else {
            return Parse.Promise.as();
          }
        }).then(function() {
          return RentCentric.Reservations.V2.retrieve(reservationId);
        }).then(function(reservation) {
          // ;;!! once FLXS-398 is fixed uncomment code
          // if (reservation.BalanceDue != 0) {
          //   return Parse.Promise.error(Error.reservationNotPaidError());
          // }
          if (!reservation.DateOut || !reservation.DateIn) {
            // Should never happen
            return Parse.Promise.error(Error.unknownRentCentricError());
          }

          if (!reservation.ReservationStatus.Confirmed || reservation.ReservationStatus.Confirmed !== "Yes") {
            return Parse.Promise.error(Error.reservationBadStatusError());
          }

          var numberOfYesProps = 0;
          for (var prop in reservation.ReservationStatus) {
            if (reservation.ReservationStatus[prop] === "Yes") {
              ++numberOfYesProps;
            }
          }

          if (numberOfYesProps > 1) {
            return Parse.Promise.error(Error.reservationBadStatusError());
          }

          var delta = start - parseInt(reservation.DateOut);
          var end = parseInt(reservation.DateIn) + delta;
          return updateReservation(reservationId, null, null, null, start, end, false, false);
        }).then(function() {
          return RentCentric.Reservations.V2.convertToRental(reservationId);
        }).then(function(converted) {
          if (!converted || converted.length !== 1) {
            return Parse.Promise.error(Error.reservationConvertToRentalError());
          }
          return Parse.Promise.as({reservationId:reservationId});
        }, function(err) {
          var error;
          if (Error.isFlexDriveError(err)) {
            error = err;
          } else {
            error = Error.systemError();
            Error.addObjectToError(Error, err);
          }
          return Parse.Promise.error(error);
        });
      },
      handleReservation: function(reservationId, reservation) {
        return getReservationForReservation(reservation, reservationId).then(function(reservation) {
          reservationId = reservation.ReservationID;
          return Billing.get(reservation.ReservationID).then(function(billing) {
            if (billing && billing.get('pending')) {
              return runBillingForReservation(reservation, true);
            } else {
              if (isSameDayReservation(reservation) || isExpiredReservation(reservation)) {
                return runBillingForReservation(reservation, false);
              } else {
                //console.log('Not running billing because reservation is not today');
                return Parse.Promise.as();
              }
            }
          });
        });
      },
      payAgain: function(reservationId) {
        return getReservationForReservation(null, reservationId).then(function(reservation) {
          return runBillingForReservation(reservation, true);
        });
      },
      retrieve: function(reservationId) {
        return RentCentric.Reservations.V2.retrieve(reservationId).then(function(reservation) {
          return Parse.Promise.as(reservation);
        }, function(error) {
          return Parse.Promise.error(error);
        });
      }
    }
  },

// Rentals
  Rentals: {
    V2: {
      willNotRenew: function(rentalId) {
        return RentCentric.Rentals.V2.updateRenewalFlag(rentalId, false).then(function() {
          return Parse.Promise.as();
        }, function(error) {
          return Parse.Promise.error(error);
        });
      }
    }
  },

  Markets:{
    list: function(options){
      var promise = new Parse.Promise();
      RentCentric.Locations.list().then(function(markets){
        Data.Markets.retrieveMarkets(markets).then(function(formattedMarkets) {
          Utils.reportSuccess(formattedMarkets, options, promise);
        },function(err){
          Utils.reportFailure(err, options, promise);
        });

      }, function(err){
        Utils.reportFailure(err, options, promise);
      });
      return promise;
    },
    V2: {
      nextBusinessDay: function(marketId) {
        var query = new Parse.Query('Markets');
        query.equalTo('MarketID', marketId);
        return query.first().then(function(market) {
          if (!market) {
            return getNextBusinessDay([]);
          }
          var location = JSON.parse(market.get('location'));
          return getNextBusinessDay(location.HoursOfOperation, location.Holidays, location.Timezone);
        }).then(function(day) {
          return Parse.Promise.as({"month":(day.getMonth() + 1),"day":day.getDate(),"year":day.getFullYear()});
        });
      },
      list: function() {
        var query = new Parse.Query('Markets');
        query.ascending('MarketName')
        return query.find().then(function(markets) {
          var marketList = [];
          for (var i = 0; i < markets.length; ++i) {
            var market = {"MarketID":markets[i].get('MarketID'),"MarketName":markets[i].get('MarketName')};
            marketList.push(market);
          }
          return Parse.Promise.as(marketList);
        });
      },
    }
  },
  Upgrades:{
  	getLatestUpgrade: function(version, clientType) {
		var promise = new Parse.Promise();
		Data.Upgrades.retrieveLastUpgrade(version, clientType).then(function(upgrade) {
			var newUpgrade = {};
			var upgradeKeys = Data.Upgrades.keys;

			if (upgrade) {
        var timeToForce = upgrade.get(upgradeKeys.timeToForce);

        if (timeToForce) {
  				var date = new Date(timeToForce);
   				var epochDate = date.getTime();
  				newUpgrade[upgradeKeys.timeToForce] = epochDate.toString();
        }

        var upgradeURL = upgrade.get(upgradeKeys.upgradeURL)
        if (upgradeURL) {
          newUpgrade[upgradeKeys.upgradeURL] = upgradeURL
        }

				newUpgrade[upgradeKeys.clientType] = clientType;

				var upgradeVersion = upgrade.get(upgradeKeys.version);
				newUpgrade[upgradeKeys.version] = upgradeVersion;
				newUpgrade[upgradeKeys.needsUpgrade] = Utils.needsUpgrade(version, upgradeVersion);

				if (upgrade.get(upgradeKeys.description)) {
					newUpgrade[upgradeKeys.description] = upgrade.get(upgradeKeys.description);
				}
			} else {
				newUpgrade[upgradeKeys.clientType] = clientType;
				newUpgrade[upgradeKeys.needsUpgrade] = false;
			}

			Utils.reportSuccess(newUpgrade, null, promise);
		}, function(error) {
			Utils.reportFailure(error, null, promise);
		});

		return promise;
		}
	}
};

// Private API:

var Users_Internal = {
  createStripeAccount: function(user, paymentToken) {
    var customer = {
      email: user.getEmail(),
      card: paymentToken
    };
    return stripe.tokens.retrieve(paymentToken).then(function(token) {
      if (token.card.cvc_check == "fail") {
        var errorData = {
          system: errorSystems.Stripe,
          stripeError: {
            type: "card_error",
            message: "Invalid CVC."
          }
        };
        return Parse.Promise.error(errorData);
      } else if (token.card.address_zip_check == "fail") {
        return Parse.Promise.error(Error.cardZipError());
      } else {
        return stripe.customers.create(customer).catch(function(error) {
          if (error && error.name == 'card_error' && error.message && error.message.indexOf('zip code') != -1) {
            return Parse.Promise.error(Error.cardZipError());
          } else {
            var errorData = {
              system: errorSystems.Stripe,
              stripeError: {
                type: error.name,
                message: error.message
              }
            };
            return Parse.Promise.error(errorData);
          }
        });
      }
    }, function(error) {
      var error = {"Stripe Error" : error};
      return Parse.Promise.error(error);
    });
  },
    createRentCentricAccount: function(user, data) {
        var k = RentCentric.customerKeys();
        data[k.email] = user.getEmail();
        data[k.accountHolder] = user.getUsername();
        data[k.countryCode] = rentCentricCountryCodeUS; // Default is U.S.
        data[k.licenseValidated] = rentCentricLicenseValidatedFalse; // Default is false.
        var marketID = data["marketID"];
        var locationID = rentCentricLocationId.atlanta; // Default location is Atlanta.
        if (marketID) {
            locationID = marketID;
        }
        return RentCentric.Customers.create(locationID, data).fail(function (error) {
          var error = {"RentCentric Error" : error};
          return Parse.Promise.error(error);
        });
    },
    // Set the flag that the clients look at to show the renewal
    // if the end date is within 3 days
    checkStartRenewal: function(rentalInfo) {
      if (!rentalInfo) {
        return;
      }
      if (rentalInfo.RenewalNotificationSent == true) {
        return;
      }

      if (rentalInfo.Expired) {
        rentalInfo.RenewalNotificationSent = true;
      }

      if (!rentalInfo.ReturnDate) {
        return;
      }

      var now = new Date();
      var returnDate = new Date(parseInt(rentalInfo.ReturnDate));

      // Check if within the config number of days to renew
      var renewDays = 3;
      var renewDaysMillis = renewDays * 24 * 60 * 60 * 1000;

      var doit = returnDate.getTime() - now.getTime() < renewDaysMillis;
      if (returnDate.getTime() - now.getTime() < renewDaysMillis) {
        rentalInfo.RenewalNotificationSent = true;
      }

    },
    // If a renewal then return the renewal reservation. If not a renewal then return the most current.
    findCurrentReservation: function(customerRunningInfo) {
      var matchedReservation = null;

      if (customerRunningInfo.RentalInfo && customerRunningInfo.RentalInfo.Expired) {
        // ;;!! Hack until FLRC-102 is ready
        return RentCentric.Reservations.V2.retrieveByCustomer(customerRunningInfo.CustomerInfo.CustomerID).then(function(reservations) {
          if (reservations && reservations.length > 0) {
            for (var i = 0; i < reservations.length; ++i) {
              var resVehicleId = reservations[i].VehicleID;
              var vehicleId = customerRunningInfo.VehicleDetailsInfo.VehicleID;
              var resBegin = new Date(parseInt(reservations[i].DateOut))
              var rentEnd = new Date(parseInt(customerRunningInfo.RentalInfo.ReturnDate))
              if (resVehicleId == vehicleId && Utils.isDateMonthYearEqual(resBegin, rentEnd)) {
                // Is reservation confirmed and only confirmed
                if (Utils.isReservationConfirmedAndOnlyConfirmed(reservations[i])) {
                  matchedReservation = reservations[i]
                }
                break;
              }
            }
            return Parse.Promise.as(matchedReservation);
          } else {
            return Parse.Promise.as(matchedReservation);
          }
        })
      } else {
        if (!customerRunningInfo.Reservations || customerRunningInfo.Reservations.length < 1) {
          //return Log.debug("no reservations");
          return Parse.Promise.as();
        }
        var customerInfo = customerRunningInfo.CustomerInfo;
        var rentalInfo = customerRunningInfo.RentalInfo;

        // First look for a renewal
        if (rentalInfo) {
          _.each(customerRunningInfo.Reservations, function(reservation) {
            if (reservation.RentalID == rentalInfo.RentalID) {
              if (Utils.isReservationConfirmedAndOnlyConfirmed(reservation)) {
                matchedReservation = reservation;
              }
            }
          });
        } else {
          // Find the most recent
          if (!matchedReservation) {
            _.each(customerRunningInfo.Reservations, function(reservation) {
              if (Utils.isReservationConfirmedAndOnlyConfirmed(reservation)) {
                if (!matchedReservation) {
                  matchedReservation = reservation;
                } else {
                  var matchedReservationStartDate = new Date(parseInt(matchedReservation.DateOut));
                  var reservationStartDate = new Date(parseInt(reservation.DateOut));
                  if (matchedReservationStartDate > reservationStartDate) {
                    matchedReservation = reservation;
                  }
                }
              }
            });
          }
        }
      }

      return Parse.Promise.as(matchedReservation);
    }
};

var Swaps_Internal = {
    create: function(data, options) {
        var promise = new Parse.Promise();
        Data.SwapRequests.create(data).then(function(swapRequest) {
            Data.Notifications.createForSwap(swapRequest).then(function(notification) {
                Notifications.Push.sendSwapNotification(notification).always(function() {
                    Utils.reportSuccess(swapRequest, options, promise);
                });
            }, function(error) {// Error creating Notification.
                swapRequest.destroy().always(function() {
                   Utils.reportFailure(error, options, promise);
                });
            });
        }, function(error) {// Error creating Swap Request.
            Utils.reportFailure(error, options, promise);
        });
        return promise;
    }
};

var Vehicles_Internal = {
  transformLocationToMarketForVehicle: function(vehicle, options) {
    return Data.Markets.marketForID(vehicle.ParentLocation.LocationID).then(function(market) {
      if (market) {
        vehicle['Market'] = market;
      }
      return Parse.Promise.as(vehicle);
    }, function(error) {
      return Parse.Promise.as(vehicle);
    });
  },
  injectUserProfileImageUrlIntoVehicles: function(vehicles) {
    Parse.Cloud.useMasterKey();
    var injectedVehicles = [];
    var parallelPromises = [];
    var self = this;
    _.each(vehicles, function(vehicle) {
      var p = self.injectUserProfileImageUrlIntoVehicle(vehicle).then(function(injectedVehicle) {
        injectedVehicles.push(injectedVehicle);
      });
      parallelPromises.push(p);
    });
    return Parse.Promise.when(parallelPromises).always(function() {
      return Parse.Promise.as(injectedVehicles);
    });
  },
  injectDetailsIntoVehicle: function(vehicle, user, deviceType) {
    //console.log('injectDetailsIntoVehicle entered');
    var self = this;
    var _vehicle = vehicle;
    return self.injectUserProfileImageUrlIntoVehicle(vehicle).then(function(vehicleWithUserProfileImageUrl) {
      return Parse.Promise.as(vehicleWithUserProfileImageUrl);
    }).then(function(vehicleToInjectPricing) {
      _vehicle = vehicleToInjectPricing;
      return self.injectPricingIntoVehicle(vehicleToInjectPricing, deviceType);
    }).then(function(vehicleToInjectSwapRequestInfo) {
      _vehicle = vehicleToInjectSwapRequestInfo;
      if (!user) {
        return Parse.Promise.as(vehicleToInjectSwapRequestInfo);
      } else {
        return self.injectSwapRequestInfoIntoVehicle(user, vehicleToInjectSwapRequestInfo);
      }
    }, function(error) {
      console.log('injectDetailsIntoVehicle error ' + JSON.stringify(error));
      return Parse.Promise.as(_vehicle);
    });
  },
  v2_injectDetailsIntoVehicle: function(vehicle, rentalCount) {
    var self = this;
    var _vehicle = vehicle;
    return self.injectUserProfileImageUrlIntoVehicle(vehicle).then(function(vehicleWithUserProfileImageUrl) {
      return Parse.Promise.as(vehicleWithUserProfileImageUrl);
    }).then(function(vehicleToInjectPricing) {
      _vehicle = vehicleToInjectPricing;
      return self.v2_injectPricingIntoVehicle(vehicleToInjectPricing, rentalCount, true);
    }).then(function(vehicleWithPricing) {
      _vehicle = vehicleWithPricing;
      return self.v2_injectLocationHours(vehicleWithPricing);
    }, function(error) {
      return Parse.Promise.as(_vehicle);
    });
  },
  injectUserProfileImageUrlIntoVehicle: function(vehicle) {
    var customerId = vehicle[RentCentric.Vehicles.keys.customerId];
    if (!customerId) {// Vehicle is available, no-one is currently renting.
      return Parse.Promise.as(vehicle);
    }
    return Data.Users.retrieveWithRentCentricCustomerId(customerId).then(function(user) {
      return Data.Users.retrieveProfileImageUrl(user);
    }).then(function(profileImageUrl) {// TODO: Test this when user does not have a profile image.
      vehicle[vehicleUserProfileImageUrlKey] = profileImageUrl;
      return Parse.Promise.as(vehicle);
    }, function(error) {
      return Parse.Promise.as(vehicle);
    });
  },

  // Uses the rates from rent centric instead of the subscription table
  injectPricingIntoVehicle: function(vehicle, deviceType) {
    // Hack to check the device type to v1 android not handling
    // a string for price w/o a number in it
    if (deviceType && deviceType === 'ios') {
      vehicle[vehicleMonthlyLowMilagePlanPriceKey] = NA;
      vehicle[vehicleWeeklyLowMilagePlanPriceKey] = NA;
      vehicle[vehicleMonthlyHighMilagePlanPriceKey] = NA;
      vehicle[vehicleWeeklyHighMilagePlanPriceKey] = NA;
    }

    return module.exports.Locations.V2.getRates(vehicle.VehicleType, vehicle.ParentLocation.LocationID).then(function(rates){
      _.each(rates, function(rate) {
        // Check if the rate is Capped - there should only be one but if there are more then this will set the price each time so the price will be overwritten each time and the last one will be used.
        if (rate.RateCode && rate.RateCode.toLowerCase().indexOf('capped') > -1) {
          var rateVehicleType = Utils.getRateVehicleType(vehicle.VehicleType, rate.RateVehicleTypes);
          vehicle[vehicleMonthlyLowMilagePlanPriceKey] = '' +  rateVehicleType.MonthlyRate;
          vehicle[vehicleWeeklyLowMilagePlanPriceKey] = '' + rateVehicleType.WeeklyRate;
        }

        // Check if the rate is Unlimited - there should only be one but if there are more then this will set the price each time so the price will be overwritten each time and the last one will be used.
        if (rate.RateCode && rate.RateCode.toLowerCase().indexOf('unlimited') > -1) {
          var rateVehicleType = Utils.getRateVehicleType(vehicle.VehicleType, rate.RateVehicleTypes);
          vehicle[vehicleMonthlyHighMilagePlanPriceKey] = '' +  rateVehicleType.MonthlyRate;
          vehicle[vehicleWeeklyHighMilagePlanPriceKey] = '' + rateVehicleType.WeeklyRate;

          if (vehicle[vehicleMonthlyLowMilagePlanPriceKey] == NA) {
            vehicle[vehicleMonthlyLowMilagePlanPriceKey] = '' +  rateVehicleType.MonthlyRate;
          }

          if (vehicle[vehicleWeeklyLowMilagePlanPriceKey] == NA) {
            vehicle[vehicleWeeklyLowMilagePlanPriceKey] = '' + rateVehicleType.WeeklyRate;
          }
        }
      });
      return Parse.Promise.as(vehicle);
    });
  },

  // Inject the new rates from RC
  v2_injectPricingIntoVehicle: function(vehicle, rentalCount, injectDeposits, injectLateFees) {
    // Get the rates
    return module.exports.Locations.V2.getRates(vehicle.VehicleType, vehicle.ParentLocation.LocationID).then(function(rates) {
      vehicle['Rates'] = rates;
      vehicle['MonthlyPricing'] = [];
      vehicle['WeeklyPricing'] = [];
      _.each(rates, function(rate) {
        vehicle.MonthlyPricing.push({fees:[],rate:{rateId:rate.RateID,rateVehicleTypesId:rate.RateVehicleTypes[0].RateVehicleTypesID,rateCode:rate.RateCode,amount:rate.RateVehicleTypes[0].MonthlyRate}});
        vehicle.WeeklyPricing.push({fees:[],rate:{rateId:rate.RateID,rateVehicleTypesId:rate.RateVehicleTypes[0].RateVehicleTypesID,rateCode:rate.RateCode,amount:rate.RateVehicleTypes[0].WeeklyRate}});
      });

      var query = new Parse.Query('Markets');
      query.equalTo('MarketID',vehicle.ParentLocation.LocationID);
      return query.first();
    }).then(function(market) {
      if (rentalCount != null && rentalCount == 0) {
        for (var i = 0; i < vehicle.MonthlyPricing.length; ++i) {
          var membershipFees = Data.Markets.getMembershipFees(market);
          for (var j = 0; j < membershipFees.length; ++j) {
            vehicle.MonthlyPricing[i].fees.push(membershipFees[j]);
          }
        }
        for (var i = 0; i < vehicle.WeeklyPricing.length; ++i) {
          var membershipFees = Data.Markets.getMembershipFees(market);
          for (var j = 0; j < membershipFees.length; ++j) {
            vehicle.WeeklyPricing[i].fees.push(membershipFees[j]);
          }
        }
      }

      if (injectDeposits) {
        for (var i = 0; i < vehicle.MonthlyPricing.length; ++i) {
          var deposits = Data.Markets.getDeposits(market,vehicle.VehicleType);
          for (var j = 0; j < deposits.length; ++j) {
            vehicle.MonthlyPricing[i].fees.push(deposits[j]);
          }
        }
        for (var i = 0; i < vehicle.WeeklyPricing.length; ++i) {
          var deposits = Data.Markets.getDeposits(market,vehicle.VehicleType);
          for (var j = 0; j < deposits.length; ++j) {
            vehicle.WeeklyPricing[i].fees.push(deposits[j]);
          }
        }
      }

      if (injectLateFees) {
        for (var i = 0; i < vehicle.MonthlyPricing.length; ++i) {
          var lateFees = Data.Markets.getLateFees(market);
          for (var j = 0; j < lateFees.length; ++j) {
            vehicle.MonthlyPricing[i].fees.push(lateFees[j]);
          }
        }
        for (var i = 0; i < vehicle.WeeklyPricing.length; ++i) {
          var lateFees = Data.Markets.getLateFees(market);
          for (var j = 0; j < lateFees.length; ++j) {
            vehicle.WeeklyPricing[i].fees.push(lateFees[j]);
          }
        }
      }
    }).then(function() {
      var chainedPromise = Parse.Promise.as();
      _.each(vehicle.MonthlyPricing, function(pricing) {
        chainedPromise = chainedPromise.then(function() {
          var totalAmount = pricing.rate.amount;

          var tempTotal = parseInt(Utils.convertToPennies(totalAmount));
          for (var i = 0; i < pricing.fees.length; ++i) {
            if (pricing.fees[i].type != "Deposit") {
              var tempFee = parseInt(Utils.convertToPennies(pricing.fees[i].amount));
              tempTotal += tempFee;
            }
          }
          tempTotal = Utils.convertToTwoDecimalNumber(tempTotal);

          if (typeof totalAmount === 'string') {
            totalAmount = tempTotal;
          } else {
            totalAmount = parseFloat(tempTotal);
          }

          return Billing.getEstimatedTaxes(vehicle.ParentLocation.LocationID, totalAmount).then(function(taxes) {
            pricing['taxes'] = taxes;
            return Parse.Promise.as();
          });
        });
      });
      return chainedPromise;
    }).then(function() {
      var chainedPromise = Parse.Promise.as();
      _.each(vehicle.WeeklyPricing, function(pricing) {
        chainedPromise = chainedPromise.then(function() {
          var totalAmount = pricing.rate.amount;

          var tempTotal = parseFloat('' + totalAmount);
          for (var i = 0; i < pricing.fees.length; ++i) {
            if (pricing.fees[i].type != "Deposit") {
              var tempFee = parseFloat('' + pricing.fees[i].amount);
              tempTotal += tempFee;
            }
          }

          if (typeof totalAmount === 'string') {
            totalAmount = '' + tempTotal;
          } else {
            totalAmount = tempTotal;
          }

          return Billing.getEstimatedTaxes(vehicle.ParentLocation.LocationID, totalAmount).then(function(taxes) {
            pricing['taxes'] = taxes;
            return Parse.Promise.as();
          });
        });
      });
      return chainedPromise;
    }).then(function() {
      return Parse.Promise.as(vehicle);
    });
  },
  // Inject the new rates from RC
  v2_injectLocationHours: function(vehicle) {
    var location;
    var query = new Parse.Query('Markets');
    if (vehicle.ParentLocation.LocationID) {
      query.equalTo('MarketID', vehicle.ParentLocation.LocationID);
    }
    return query.first().then(function(market) {
      location = JSON.parse(market.get('location'));
      // See if today is a business day
      return getTodayBusinessDay(location.HoursOfOperation, location.Holidays, location.Timezone);
    }).then(function(todayTZ) {
      return getBusinessHours(todayTZ, null, location.HoursOfOperation);
    }).then(function(todayBizHours) {
      if (todayBizHours) {
        vehicle.ParentLocation['TodayBusinessHours'] = todayBizHours;
      }
      return getNextBusinessDay(location.HoursOfOperation, location.Holidays, location.Timezone);
    }).then(function(nextBusinessDayTZ) {
      return getBusinessHours(nextBusinessDayTZ, null, location.HoursOfOperation);
    }).then(function(nextDayBizHours) {
      if (nextDayBizHours) {
        vehicle.ParentLocation['NextDayBusinessHours'] = nextDayBizHours;
      }
      if (vehicle.OffsiteLocation && vehicle.OffsiteLocation.LocationID) {
        query = new Parse.Query('OffsiteLocation');
        query.equalTo('MarketID', vehicle.OffsiteLocation.LocationID);
        return query.first()
      } else {
        return Parse.Promise.as();
      }
    }).then(function(market) {
      location = null;
      if (market) {
        location = JSON.parse(market.get('location'));
        // See if today is a business day
        return getTodayBusinessDay(location.HoursOfOperation, location.Holidays, location.Timezone);
      } else {
        return Parse.Promise.as();
      }
    }).then(function(todayTZ) {
      if (location) {
        return getBusinessHours(todayTZ, null, location.HoursOfOperation);
      } else {
        return Parse.Promise.as();
      }
    }).then(function(todayBizHours) {
      if (todayBizHours) {
        vehicle.OffsiteLocation['TodayBusinessHours'] = todayBizHours;
      }
      if (location) {
        return getNextBusinessDay(location.HoursOfOperation, location.Holidays, location.Timezone);
      } else {
        return Parse.Promise.as();
      }
    }).then(function(nextBusinessDayTZ) {
      if (location) {
        return getBusinessHours(nextBusinessDayTZ, null, location.HoursOfOperation);
      } else {
        return Parse.Promise.as();
      }
    }).then(function(nextDayBizHours) {
      if (nextDayBizHours) {
        vehicle.OffsiteLocation['NextDayBusinessHours'] = nextDayBizHours;
      }
      return Parse.Promise.as(vehicle);
    });
  },
  injectSwapRequestInfoIntoVehicle: function(user, vehicle) {
    // TODO: In future release, consider checking the swap request receiver matches the vehicle\'s Rent Centric customer Id.
    return Data.SwapRequests.userHasActiveRequestForVehicle(user, vehicle[RentCentric.Vehicles.keys.vehicleId]).then(function(hasActiveRequest) {
      vehicle[vehicleActiveSwapRequestExistsKey] = hasActiveRequest;
      return Parse.Promise.as(vehicle);
    });
  },
  removeCustomerNamesFromVehicle: function(vehicle) {
    // TODO: In future version, ask Rent Centric to not pass through customer's first,middle, and last name.
    //  Instead use username from the client.
    vehicle[RentCentric.Vehicles.keys.customerMiddleName] = '';
    vehicle[RentCentric.Vehicles.keys.customerLastName] = '';
    return vehicle;
  },
  anonymizeVehicle: function(vehicle) {
    vehicle[RentCentric.Vehicles.keys.customerFirstName] = 'Flexdrive Driver';
    vehicle[vehicleUserProfileImageUrlKey] = null;
    return vehicle;
  }
};

// Private API
function sendTimeToRenewPush (rentCentricCustomerId, rentalId) {
    console.log("Will Try Push - rentCentricCustomerId: " + rentCentricCustomerId + " RentalId: " + rentalId)
    var keys = Data.Notifications.keys;
    var previousNotificationSent = false;
    var rentalUser;
    var notification;

    return Data.Users.v2_retrieveWithRentCentricCustomerId(rentCentricCustomerId).then(function(user) {
        if (!user) {
          var error = Error.unknownRentCentricCustomerIdError();
          Error.addObjectToError({ "RentCentricCustomerId" : rentCentricCustomerId });
          return Parse.Promise.error(error);
        } else {
            rentalUser = user;
            return Data.Notifications.retrievePreviousTimeToRenewNotificationForUser(user, rentalId)
        }
    }).then(function(previousNotification) {
        if (previousNotification) {
            notification = previousNotification;

            var sent = previousNotification.get(keys.pushSent);
            if (sent) {
                previousNotificationSent = true;
            }
        } else {
            notification = Data.Notifications.createForTimeToRenew(rentalUser, rentalId)
        }

        return notification.save();
    }).then(function() {
        if (previousNotificationSent) {
            return Parse.Promise.error(Error.renewalNotificationPreviouslySentError());
        } else {
            return Notifications.Push.V2.sendStandardPushNotification(notification, rentalUser)
        }
    }).then(function() {
        //console.log("Successful Push");
        notification.set(keys.pushSent, true);
        return notification.save();
    }, function (error) {
      var parameters = { "params" : {
        "rentCentricCustomerId" : rentCentricCustomerId,
        "rentalId" : rentalId }};

        var saveNotification;
        if (notification && previousNotificationSent == false) {
          notification.set(keys.pushSent, false);
          saveNotification = notification;
        }

        return saveNotificationAndLogError(error, saveNotification, parameters, "sendTimeToRenewPush");
    });
}

// This method will never return an error.  Errors are ignored so one failed push does not kill all of the pushes in the sendPaymentNotifications method.
function sendPaymentNotificationForRentcentricId(customerIDRentCentric, reservationId, paidDate, paymentError, startDate, endDate, shouldResendNotification) {
    var keys = Data.Notifications.keys;
    var reservationUser;

    return Data.Users.v2_retrieveWithRentCentricCustomerId(customerIDRentCentric).then(function (user) {
        //console.log("Will Notify User: RentCentric ID: " + customerIDRentCentric + " reservationId: " + reservationId + " paidDate: " + paidDate + " paymentError: " + paymentError + " startDate: " + startDate + " endDate: " + endDate);
        if (!user) {
            return Parse.Promise.error(Error.unknownRentCentricCustomerIdError().userMessage);
        } else {
            reservationUser = user;
            var notificationType = notificationTypeForPaidDate(paidDate, paymentError)
            return Data.Notifications.retrievePreviousPaymentNotificationForUser(reservationUser, reservationId, notificationType);
        }
    }).then(function(previousNotification) {
      var notification;

      if (previousNotification && !shouldResendNotification) {
        notification = previousNotification
      } else {
        if (paymentError) {
            notification = Data.Notifications.createForPaymentFailure(reservationUser, reservationId, startDate, endDate);
        } else {
            notification = Data.Notifications.createForSuccessfulPayment(reservationUser, reservationId, startDate, endDate);
        }
      }
      return notification.save();
    }).then(function(notification) {
      var promises = []

      var shouldSendPush = Data.Notifications.pushSent(notification) == false || shouldResendNotification == true;
      if (shouldSendPush) {
        promises.push(sendPaymentPush(reservationUser, notification, reservationId));
      } else {
        //console.log("Skipping previously sent push - reservationId: " + reservationId)
      }

      var isPaymentFailureNotification = Data.Notifications.isPaymentFailureNotification(notification);
      var emailSent = Data.Notifications.emailSent(notification)
      if (isPaymentFailureNotification && (emailSent == false || shouldResendNotification == true)) {
        promises.push(sendPaymentFailedEmail(reservationUser, notification, reservationId, customerIDRentCentric));
      } else if (isPaymentFailureNotification == false) {
        //console.log("Skipping email for successful payment - reservationId: " + reservationId)
      } else {
        //console.log("Skipping previously sent email - reservationId: " + reservationId)
      }

      return Parse.Promise.when(promises)
    }, function(error) {
       if (!reservationUser) {
        // Only called if an invalid rentcentric id was submitted, so let's show the details for the push before showing the failure log.
        //console.log("Push Failed: RentCentric ID: " + customerIDRentCentric + " reservationId: " + reservationId + " paidDate: " + paidDate + " paymentError: " + paymentError + " startDate: " + startDate + " endDate: " + endDate);
        console.log("Push Failed - reservationId: " + reservationId + " error: " + error.userMessage);
      } else {
        console.log("Push Failed - reservationId: " + reservationId + " error: " + error);
      }
      var parameters = { "params" :
      {
        "customerIDRentCentric" : customerIDRentCentric,
        "reservationId" : reservationId,
        "paidDate" : paidDate,
        "paymentError" : paymentError,
        "startDate" : startDate,
        "endDate" : endDate
      }};

      return saveNotificationAndLogError(error, null, parameters, "sendPaymentNotificationForRentcentricId");
    });
  }

  function sendPaymentPush(user, notification, reservationId) {
    var keys = Data.Notifications.keys;
    return Notifications.Push.V2.sendStandardPushNotification(notification, user).then(function(pushResult) {
      if (pushResult.result == true) {
        var notificationType = notification.get(keys.type);
        console.log("Succesful push - type: " + notificationType + " reservationId: " + reservationId);
        notification.set(keys.pushSent, true);
        return notification.save();
      } else {
        return Parse.Promise.error(Error.pushNotificationFailedError())
      }
    }, function(error) {
      console.log("Push Failed: " + Error.pushNotificationFailedError().userMessage);
      notification.set(keys.pushSent, false);

      var parameters = { "params" :
      {
        "reservationId" : reservationId
      }};

      return saveNotificationAndLogError(error, notification, parameters, "sendPaymentPush");
    });
}

function sendPaymentFailedEmail(user, notification, reservationId, customerIDRentCentric) {
  var keys = Data.Notifications.keys;
  var rentCentricCustomer;

  return Mail.sendTemplateEmail(user, "emailtemplates/payment_failed_email_template.html", 'Flexdrive Renewal Payment Issues').then(function() {
    console.log("Sent Email for reservation id: " + reservationId)
    notification.set(keys.emailSent, true);
    return notification.save();
  }, function(error) {
    console.log("Error sending email for reservation id: " + reservationId)
    notification.set(keys.emailSent, false);

    var parameters = { "params" :
    {
      "reservationId" : reservationId,
      "customerIDRentCentric" : customerIDRentCentric
    }};

    return saveNotificationAndLogError(error, notification, parameters, "sendPaymentFailedEmail");
  });
}

function notificationTypeForPaidDate(paidDate, paymentError) {
  var notificationType;

  if (paymentError) {
      notificationType = Data.Notifications.notificationTypeForPaymentFailure();
  } else {
      notificationType = Data.Notifications.notificationTypeForSuccessfulPayment();
  }

  return notificationType
}

function sendMembershipApprovalPush(user, notification) {
  var keys = Data.Notifications.keys;

  return Notifications.Push.V2.sendStandardPushNotification(notification, user).then(function(pushResult) {
    if (pushResult.result == true) {
      var notificationType = notification.get(keys.type);
      console.log("Succesful push - type: " + notificationType);
      notification.set(keys.pushSent, true);
      return notification.save();
    } else {
      return Parse.Promise.error(Error.pushNotificationFailedError())
    }
  }, function(error) {
    console.log("Push Failed: " + Error.pushNotificationFailedError().userMessage)
    notification.set(keys.pushSent, false);

    return saveNotificationAndLogError(error, notification, null, "sendMembershipApprovalPush");
  });
}

function sendMembershipApprovalEmail(user, notification, rentCentricCustomerId) {
  var keys = Data.Notifications.keys;
  var rentCentricCustomer;

  return Mail.sendTemplateEmail(user, "emailtemplates/membership_approved_email_template.html", 'Membership Approved').then(function() {
    console.log("Sent Email for RentCentric Customer Id: " + rentCentricCustomerId)
    notification.set(keys.emailSent, true);
    return notification.save()
  }, function(error) {
    console.log("sendMembershipApprovalEmail error sending email for RentCentric Customer Id: " + rentCentricCustomerId + ' error is ' + JSON.stringify(error));
    notification.set(keys.emailSent, false);
    return saveNotificationAndLogError(error, notification, null, "sendMembershipApprovalEmail");
  });
}

function saveNotificationAndLogError(error, notification, parameters, methodName) {
  var userMessage = "";
  if (error.userMessage) {
    userMessage = error.userMessage;
  }
  console.log("Push Error: " + userMessage)

  return Log.logFlexDriveError(error, parameters, null, null, null, methodName, null, null).then(function() {
    if (notification) {
      return notification.save();
    } else {
      return Parse.Promise.as();
    }
  }).then(function() {
    return Parse.Promise.as(userMessage);
  }, function(error) {
    return Parse.Promise.as(userMessage);
  });
}

function getReservationForReservation(reservation, reservationId) {
  if (reservation) {
    return Parse.Promise.as(reservation);
  } else {
    return module.exports.Reservations.V2.retrieve(reservationId);
  }
}

function getBillingsForPaymentNotifications(billings) {
  if (billings) {
    return Parse.Promise.as(billings)
  } else {
    var query = new Parse.Query('Billing');

    var today = new Date();
    var todayStart = new Date(today.getFullYear(), today.getMonth(), today.getDate());
    var tomorrowStart = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 24);
    //console.log("Today Start: " + todayStart);
    //console.log("Tomorrow Start: " + tomorrowStart);

    query.greaterThanOrEqualTo("updatedAt", todayStart);
    query.lessThan("updatedAt", tomorrowStart);

    return query.find()
  }
}

function runBillingForReservation(reservation, billMeNow) {
  if (billMeNow === true) {
    reservation['billMe'] = true;
  }
  return Billing.run([reservation], true).then(function() {
    if (reservation.skipProcessing) {
      return Parse.Promise.as();
    } else {
      var query = new Parse.Query('Billing');
      query.equalTo("reservationId", reservation.ReservationID);
      return query.first();
    }
  }).then(function(billing) {
    if (billing) {
      return module.exports.Reservations.notifyUsersOfPayment([billing]);
    } else {
      return Parse.Promise.as()
    }
  });
}

function updateReservation(reservationId, rateId, rateVehicleTypesId, days, start, end, expired, payNow) {
  var reservation;

  // Make it a boolean
  if (expired) {
    expired = true
  } else {
    expired = false
  }

  return RentCentric.Reservations.V2.retrieve(reservationId).then(function(_reservation) {
  reservation = _reservation;
  return RentCentric.Reservations.V2.update(reservationId, rateId, rateVehicleTypesId, days, start, end, reservation.ExpectedDateOut);
  }).then(function() {
    //;;!! can't RC return the reservation from the update call? Why call to get it again
    return RentCentric.Reservations.V2.retrieve(reservationId)
  }).then(function(_reservation) {
    reservation = _reservation;
    return Billing.get(reservationId).then(function(billing) {
      if (billing) {
        billing.set('pending', true);
        billing.set('expired', expired);
        return billing.save().always(function() {
          return Parse.Promise.as({ "reservationId" : reservationId});
          // if (payNow) {
          //   return runBillingForReservation(reservation, true).always(function() {
          //     return Parse.Promise.as({ "reservationId" : reservationId});
          //   })
          // } else {
          //   return Parse.Promise.as({ "reservationId" : reservationId});
          // }
        });
      } else {
        return Billing.createBilling(reservation, true, false, expired).always(function() {
          return Parse.Promise.as({ "reservationId" : reservationId});
          // if (payNow) {
          //   return Parse.Promise.as({ "reservationId" : reservationId});
          //   return Billing.run([reservation]).always(function() {
          //     return Parse.Promise.as({ "reservationId" : reservationId});
          //   })
          // } else {
          //   return Parse.Promise.as({ "reservationId" : reservationId});
          // }
        });
      }
    });
  }, function(error) {
    return Parse.Promise.error(error);
  });
}

function isSameDayReservation(reservation) {
  var today = new Date();
  var dateOut = new Date(parseInt(reservation.DateOut));
  var tz = reservation.Timezone;
  return Utils.isDateMonthYearEqual(dateOut, today, tz) == true;
}

function isExpiredReservation(reservation) {
  var now = new Date();
  var dateOut = new Date(parseInt(reservation.DateOut));
  return now.getTime() > dateOut.getTime()
}

function getNextBusinessDay(hoursOfOperation, holidays, tz) {
  if (!tz) {
    tz = Utils.easternTimezone;
  }
  var currentDay = Utils.getDateForTimezone(new Date(), tz);
  var nextDay;
  var holiday;
  var found = false;
  var count = 0;
  do {
    nextDay = new Date(currentDay.getFullYear(), currentDay.getMonth(), (currentDay.getDate() + 1));
    found = true;
    if (holidays) {
      for (var i = 0; i < holidays.length; ++i) {
        holiday = new Date(holidays[i]);
        if (nextDay.getFullYear() === holiday.getFullYear() && nextDay.getMonth() == holiday.getMonth() && nextDay.getDate() === holiday.getDate()) {
          found = false;
          break;
        }
      }
    }

    if (hoursOfOperation) {
      for (var i = 0; i < hoursOfOperation.length; ++i) {
        var dayNameAsDay = convertDayNameToDay(hoursOfOperation[i].DayOfWeek);
        if (dayNameAsDay == nextDay.getDay()) {
          if (hoursOfOperation[i].IsOpen == "False") {
            found = false;
            break;
          }
        }
      }
    }

    currentDay = nextDay;
    ++count;
  } while (!found && count <= 7);
  return Parse.Promise.as(nextDay);
}

function getTodayBusinessDay(hoursOfOperation, holidays, tz) {
  var currentDay = Utils.getDateForTimezone(new Date(), tz);
  var holiday;
  var found = false;

  found = true;
  if (holidays) {
    for (var i = 0; i < holidays.length; ++i) {
      holiday = new Date(holidays[i]);
      if (currentDay.getFullYear() === holiday.getFullYear() && currentDay.getMonth() == holiday.getMonth() && currentDay.getDate() === holiday.getDate()) {
        found = false;
        break;
      }
    }
  }

  if (hoursOfOperation) {
    for (var i = 0; i < hoursOfOperation.length; ++i) {
      var dayNameAsDay = convertDayNameToDay(hoursOfOperation[i].DayOfWeek);
      if (dayNameAsDay == currentDay.getDay()) {
        if (hoursOfOperation[i].IsOpen == "False") {
          found = false;
          break;
        }
      }
    }
  }

  if (found) {
    return Parse.Promise.as(currentDay);
  } else {
    return Parse.Promise.as(null);
  }
}

// Assumes the day is a business day or null
function getBusinessHours(day, tz, hoursOfOperation) {

  if (!day) {
    return Parse.Promise.as(null);
  }

  if (!hoursOfOperation) {
    return Parse.Promise.as(null);
  }

  var businessDay = Utils.getDateForTimezone(day, tz);

  var startHour;
  var endHour;

  for (var i = 0; i < hoursOfOperation.length; ++i) {
    if (hoursOfOperation[i].IsOpen == "True") {
      var dayNameAsDay = convertDayNameToDay(hoursOfOperation[i].DayOfWeek);
      if (dayNameAsDay == businessDay.getDay()) {
        if (hoursOfOperation[i].StartTime) {
          startHour = hoursOfOperation[i].StartTime;
        }
        if (hoursOfOperation[i].EndTime) {
          endHour = hoursOfOperation[i].EndTime;
        }
        break;
      }
    }
  }

  if (startHour && endHour) {
    return Parse.Promise.as({date:{day:businessDay.getDate(),month:(businessDay.getMonth()+1),year:businessDay.getFullYear()}, start:startHour, end:endHour});
  } else {
    return Parse.Promise.as(null);
  }
}

function convertDayNameToDay(dayName) {
  if (dayName == "Sunday") {
    return 0;
  } else if (dayName == "Monday") {
    return 1;
  } else if (dayName == "Tuesday") {
    return 2;
  } else if (dayName == "Wednesday") {
    return 3;
  } else if (dayName == "Thursday") {
    return 4;
  } else if (dayName == "Friday") {
    return 5;
  } else if (dayName == "Saturday") {
    return 6;
  } else {
    return 0;
  }
}

function lockedOut(user) {
  var now = new Date();
  var minutes;
  var attempts;

  return Utils.getConfigValue('lockedOutMinutes').then(function(mins) {
    minutes = mins;
    return Utils.getConfigValue('lockedOutAttempts');
  }).then(function(attempts) {
    // First check if user is already locked out
    var lockedAt = user.get('lockedAt');
    if (lockedAt) {
      // Is locked out
      if ( (now.getTime() - lockedAt.getTime()) > (minutes * 60 * 1000)) {
        user.set('failedLoginAttempts',[])
        user.set('lockedAt',null)
        return user.save().then(function(savedUser) {
          return Parse.Promise.as({user:savedUser,minutes:minutes})
        });
      } else {
        return Parse.Promise.as({user:user,minutes:minutes})
      }
    } else {
      // Not locked out. Check if should be locked out
      var failedAttempts = user.get('failedLoginAttempts')
      if (failedAttempts && failedAttempts.length >= attempts) {
        user.set('failedLoginAttempts',[])
        user.set('lockedAt',(new Date()))
        return user.save().then(function(savedUser) {
          return Parse.Promise.as({user:savedUser,minutes:minutes})
        });
      } else {
        return Parse.Promise.as({user:user,minutes:minutes})
      }
    }
  });

}
