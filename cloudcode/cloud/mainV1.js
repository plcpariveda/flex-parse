
var Data = require('./flexdrive-data.js');
var Environment = require('./flexdrive-environment.js');
var FlexDrive = require('./flexdrive.js');
var Error = require('./flexdrive-error.js');
var Log = require('./flexdrive-log.js');

// Markets
Parse.Cloud.define("Markets_list", function(request, response) {
  FlexDrive.Markets.list().then(function(markets) {
    response.success(markets);
  }, function(error) {
    response.error(error);
  });
});

// Vehicles

Parse.Cloud.define("Vehicles_Rentals_sendRenewalRequest", function(request, response) {
  var parseUser = request.user;
  if (!parseUser) {
    response.error("Please sign in!");
    return;
  }
  FlexDrive.Vehicles.Rentals.sendRenewalRequest(parseUser).then(function() {
    response.success('Success');
  }, function(error) {
    response.error(error);
  });
});

// ReservationRequests

Parse.Cloud.define('Vehicles_ReservationRequests_create', function(request, response) {
  var parseUser = request.user;
  var vehicleId = request.params.vehicleId;
  if (!parseUser) {
    response.error("Please sign in!");
    return;
  }
  var data = {
    user: parseUser,
    vehicleId: vehicleId
  };
  FlexDrive.Vehicles.ReservationRequests.create(data).then(function(reservationRequest) {
    response.success(reservationRequest);
  }, function(error) {
    response.error(error);
  });
});

Parse.Cloud.define('Vehicles_ReservationRequests_hasReservation', function(request, response) {
  var parseUser = request.user;
  var vehicleId = request.params.vehicleId;
  if (!parseUser) {
    response.error("Please sign in!");
    return;
  }
  FlexDrive.Vehicles.ReservationRequests.hasReservation(parseUser, vehicleId).then(function(hasReservation) {
    response.success( { hasReservation: hasReservation} );
  }, function(error) {
    response.error(error);
  });
});

// Users
Parse.Cloud.define("Users_existsWithUsernameEmail", function(request, response) {
  // TODO: Validate input.
  // TODO: In future release, add security feature that doesn't allow this method
  //  to be called over and over to figure out what all the usernames/emails are.
  var username = request.params.username;
  var email = request.params.email;
  FlexDrive.Users.existsWithUsernameEmail(username, email).then(function(data) {
    response.success(data);
  }, function(error) {
    respone.error(error);
  });
});

Parse.Cloud.define("Users_login", function(request, response) {
  var username = request.params.username;
  var password = request.params.password;

  FlexDrive.Users.login(username, password).then(function(result) {
    response.success({sessionToken: result.getSessionToken()});
  }, function(error) {
    response.error("Could not login.");
  });
});

Parse.Cloud.define("Users_createCustomerAccounts", function(request, response) {
  try {
    //TODO: Check Input
    var parseUser = request.user;
    if(!parseUser) {
      return Log.logFlexDriveError(Error.unauthorizedUserError(), request.params, "Users_createCustomerAccounts", null, null, null, null, null).then(function() {
        response.error("Please Sign in!");
      });
    }

    var data = {
      stripePaymentToken: request.params.paymentToken,
      customerData: request.params.customerInfo
    };
    FlexDrive.Users.createCustomerAccounts(parseUser, data).then(function() {
      response.success('Success');
    }, function(error) {
      var errorResponsePayload = createAccountErrorResponsePayloadForError(error);
      var logError = createAccountErrorForError(error, errorResponsePayload);

      return Log.logFlexDriveError(logError, request.params, "Users_createCustomerAccounts", null, null, null, null, null).then(function() {
        response.error(errorResponsePayload);
      });
    });
  } catch(error) {
    var errorResponsePayload = createAccountErrorResponsePayloadForError(error);
    var logError = createAccountErrorForError(error, errorResponsePayload);

    return Log.logFlexDriveError(logError, request.params, "Users_createCustomerAccounts", null, null, null, null, null).then(function() {
      response.error(errorResponsePayload);
    });
  }
});

function createAccountErrorForError(error, errorResponsePayload) {
  var logError = Error.webRegistrationError();
  Error.addObjectToError(logError, errorResponsePayload);

  if (error) {
    Error.addObjectToError(logError, error);
  }

  return logError;
}

function createAccountErrorResponsePayloadForError(error) {
  var errorResponsePayload = {};
  if (error && error.system && error.system == FlexDrive.errorSystems.Stripe && error.stripeError.type == 'card_error') {
    errorResponsePayload = {
      domain: 'com.flexdrive.errors.stripe',
      code: 0,
      message: error.stripeError.message
    };
  } else {
    errorResponsePayload = {
      domain: 'com.flexdrive.errors.generic',
      code: 0,
      message: 'An unexpected error has occured. Please try again later.'
    };
  }

  return errorResponsePayload;
}

Parse.Cloud.define("Users_retrieve", function(request, response) {
  var parseUser = request.user;
  if (!parseUser) {
    response.error("Please sign in!");
    return;
  }
  FlexDrive.Users.retrieve(parseUser).then(function(user) {
    response.success(user);
  }, function(error) { // TODO: Make sure error object kind is correct.
    response.error(error);
  });
});

Parse.Cloud.define("Users_updateEmail", function(request, response) {
  // TODO(RC): Protect endpoint uniformly.
  var parseUser = request.user;
  if (!parseUser) {
    response.error("Please sign in!");
    return;
  }
  var newEmail = request.params.email;
  if (!newEmail) {
    // TODO(RC): What if user sends empty string.
    response.error("No email value found. Must provide an email.");
    return;
  }
  FlexDrive.Users.updateEmail(parseUser, newEmail).then(function() {
    response.success("User's email updated successfully.");
  }, function(error) {
    response.error(error);
  });
});

Parse.Cloud.define("Users_sanitizeEmail", function(request, response) {
  Parse.Cloud.useMasterKey();

  var newEmail = request.params.email;
  if (!newEmail) {
    // TODO(RC): What if user sends empty string.
    response.error("No email value found. Must provide an email.");
    return;
  }

  var name = request.params.name;
  if (!name) {
    response.error("No name value found.");
    return;
  }

  var query = new Parse.Query(Parse.User);
  query.equalTo('username',name);
  query.first().then(function(parseUser) {
    return FlexDrive.Users.updateEmail(parseUser, newEmail);
  }).then(function() {
    response.success("User's email sanitized.");
  }, function(error) {
    response.error(error);
  });
});

// Swaps

Parse.Cloud.define("Vehicles_Swaps_create", function(request, response) {
  var parseUser = request.user;
  if (!parseUser) {
    response.error("Please sign in!");
    return;
  }
  // TODO: Check input.
  // if(!senderUser) {
  //     response.error("Please Sign in!");
  //     return;
  // }
  //
  // if(!recipient) {
  //     response.error("`recipientCustomerID` param is required");
  //     return;
  // }
  //
  // if (!senderVehicleID) {
  //   response.error("`senderVehicleID` param is required");
  //   return;
  // }
  //
  // if (!desiredVehicleID) {
  //   response.error("`desiredVehicleID` param is required");
  //   return;
  // }
  Parse.Cloud.useMasterKey();
  var receiverRentCentricCustomerId = request.params.recipientCustomerID;
  Data.Users.retrieveWithRentCentricCustomerId(receiverRentCentricCustomerId).then(function(receiver) {
    var data = {
      sender: request.user,
      senderVehicleId: request.params.senderVehicleID,
      receiver: receiver,
      receiverVehicleId: request.params.desiredVehicleID,
      message: request.params.message
    };
    return FlexDrive.Vehicles.Swaps.create(data);
  }).then(function(swapRequest) {
    response.success(swapRequest);
  }, function(error) {
    response.error(error);
  });
});

Parse.Cloud.define("Vehicles_Swaps_respond", function(request, response) {
  var parseUser = request.user;
  if (!parseUser) {
    response.error("Please sign in!");
    return;
  }
  // TODO: Add input check. if (token)
  Parse.Cloud.useMasterKey();
  var swapId = request.params.swapToken;
  var data = {
    responder: parseUser,
    response: request.params.responseStatus
  };
  FlexDrive.Vehicles.Swaps.respond(swapId, data).then(function(updatedSwapRequest) {
    response.success(updatedSwapRequest);
  }, function(error) {
    response.error(error);
  });
});

// Vehicles

Parse.Cloud.define("Vehicles_list", function(request, response) {
  var parseUser = request.user;
  if (!parseUser) {
    FlexDrive.Vehicles.anonymousList().then(function(vehicles) {
      response.success(vehicles);
    }, function(error) {
      response.error(error);
    });
  } else {
    FlexDrive.Vehicles.list().then(function(vehicles) {
      response.success(vehicles);
    }, function(error) {
      response.error(error);
    });
  }
});

Parse.Cloud.define("Vehicles_list_v2", function(request, response) {
  var parseUser = request.user;
  var marketId = request.params.MarketID;
  var index = request.params.index;
  var size = request.params.size;
  if (!parseUser) {
    FlexDrive.Vehicles.anonymousList(marketId,2,index,size).then(function(obj) {
      response.success(obj);
    }, function(error) {
      response.error(error);
    });
  } else {
    FlexDrive.Vehicles.list(marketId,2,index,size).then(function(obj) {
      response.success(obj);
    }, function(error) {
      response.error(error);
    });
  }
});


Parse.Cloud.define("Vehicles_retrieve", function(request, response) {
  Parse.Cloud.useMasterKey();
  var vehicleId = request.params.VehicleID;
  var parseUser = request.user;

  var deviceType = "android";

  var query = new Parse.Query(Parse.Installation);
  console.log("installationId=" + request.installationId);
  query.equalTo("installationId", request.installationId);
  return query.first().then(function(installation) {
    if (installation) {
      deviceType = installation.get('deviceType');
      console.log('found installation, deviceType=' + deviceType);
    } else {
      console.log('no installation found, deviceType=' + deviceType);
    }

    if (!parseUser) {
      return FlexDrive.Vehicles.anonymousRetrieve(vehicleId,  deviceType).then(function(vehicle) {
        response.success(vehicle);
      }, function(error) {
        response.error(error);
      });
    } else {
      return FlexDrive.Vehicles.retrieve(vehicleId, parseUser, deviceType).then(function(vehicle) {
        response.success(vehicle);
      }, function(error) {
        response.error(error);
      });
    }
  });
});


Parse.Cloud.define("Vehicles_Rentals_retrieveActiveVehicleForUser", function(request, response) {
  var parseUser = request.user;
  if (!parseUser) {
    response.error("Must be authenticated to make this API call.");
    return;
  }
  FlexDrive.Vehicles.Rentals.retrieveActiveVehicleForUser(parseUser).then(function(vehicle) {
    response.success(vehicle);
  }, function(error) {
    response.error(error);
  });
});

// Automated test endpoint
Parse.Cloud.define("Users_createTestUser", function(request, response) {
  try {
    stripeUserId = request.params.stripeUserId;
    rcUserId = request.params.rcUserId;
    email = request.params.email;
    password = request.params.password;

    FlexDrive.Users.V2.createTestUser(stripeUserId, rcUserId, email, password).then(function(parseUser) {
      response.success({
        data: parseUser,
        error: null
      });
    }, function(error) {
      console.error(JSON.stringify(error));
      response.success({
        data: null,
        error: error
      });
    });
  } catch(err) {
    console.error(err.message);
    response.success({
      data: null,
      error: Error.systemError()
    });
  }
});
