/**
* FlexDrive Email
* Parse Cloud Module
* 1.0.0
*/

var Environment = require('./flexdrive-environment.js');
var RentCentric = require("./rentcentric.js");
var Error = require('./flexdrive-error.js');

var MailgunEnvironment = Environment.Mailgun.getEnvironment();
var mailgun = require('./mailgun-wrapper')

mailgun.initialize(MailgunEnvironment.domainName, MailgunEnvironment.apiKey);
var flexDriveAdminEmailAddress = MailgunEnvironment.flexDriveAdminEmailAddress;
var flexDriveBillingEmailAddress = MailgunEnvironment.flexDriveBillingEmailAddress;
var flexDriveITEmail = MailgunEnvironment.flexDriveITEmail;

var Data = require('./flexdrive-data.js');
var Utils = require('./flexdrive-utils.js');

// Values:

// Copy
var customerNameCopy = function(data) {
  return data.firstName + ' ' + data.lastName + ' (' + data.id + ')';
};
var newUserRegistrationApplicationEmailSubjectCopy = function(data) {
  return 'New Customer Application - ' + customerNameCopy(data);
};
var newUserRegistrationApplicationEmailBodyCopy = function(data) {
  return customerNameCopy(data) + ' registered and needs drivers license validation to complete registration.';
};
var reservationRequestEmailSubjectCopy = function(data) {
  return 'Reservation Request - ' + data.vehicle.id + ' by ' + customerNameCopy(data.customer);
};
var reservationRequestEmailBodyCopy = function(data) {
  return customerNameCopy(data.customer) + ' is interested in reserving vehicle:\n' + data.vehicle.id + '    ' + data.vehicle.color + '   ' + data.vehicle.year + ' ' + data.vehicle.make + ' ' + data.vehicle.model;
};
var swapAgreementEmailSubjectCopy = function(data) {
  return 'Swap Request - From: ' + customerNameCopy(data.senderData.customer) + ' To: ' + customerNameCopy(data.receiverData.customer);
};
var swapAgreementEmailBodyCopy = function(data) {
  return customerNameCopy(data.receiverData.customer) + ', Rental# ' + data.receiverData.rentalNumber + ' has agreed to swap vehicles with\n' + customerNameCopy(data.senderData.customer) + ', Rental# ' + data.senderData.rentalNumber;
};
var rentalRenewalRequestEmailSubjectCopy = function(data) {
  return 'Rental Renewal - ' + customerNameCopy(data);
};
var rentalRenewalRequestEmailBodyCopy = function(data) {
  return customerNameCopy(data.customerData) + ' would like to renew rental# ' + data.rentalData.rentalNumber;
};

// Cloud Module Public API:

module.exports = {
  /**
  * Get the version of the module.
  * @return {String}
  */
  version: '1.0.0',

  sendNewUserRegistrationApplication: function(user, customerData, options) {
    var promise = new Parse.Promise();
    var fromAddress = user.getEmail();
    var subject = newUserRegistrationApplicationEmailSubjectCopy(customerData);
    var body = newUserRegistrationApplicationEmailBodyCopy(customerData);
    sendEmail(flexDriveAdminEmailAddress, fromAddress, subject, body).then(function() {
      Utils.reportSuccess(null, options, promise);
    }, function(error) {
      Utils.reportFailure(error, options, promise);
    });
    return promise;
  },

  sendSwapAgreement: function(swapRequest, senderData, receiverData, options) {
    Parse.Cloud.useMasterKey();
    var promise = new Parse.Promise();
    Data.SwapRequests.fetchRelatedObjects(swapRequest).then(function() {
      var sender = swapRequest.get(Data.SwapRequests.keys.sender);
      var fromAddress = sender.getEmail();
      var subject = swapAgreementEmailSubjectCopy({
        senderData: senderData,
        receiverData: receiverData
      });
      var body = swapAgreementEmailBodyCopy({
        senderData: senderData,
        receiverData: receiverData
      });
      return sendEmail(flexDriveAdminEmailAddress, fromAddress, subject, body);
    }).then(function() {
      Utils.reportSuccess(swapRequest, options, promise);
    }, function(error) {
      Utils.reportFailure(error, options, promise);
    });
    return promise;
  },

  sendReservationRequest: function(reservationRequest, vehicleData, customerData, options) {
    var promise = new Parse.Promise();
    Data.ReservationRequests.fetchRelatedObjects(reservationRequest).then(function() {
      var user = reservationRequest.get(Data.ReservationRequests.keys.user);
      var fromAddress = user.getEmail();
      var subject = reservationRequestEmailSubjectCopy({
        customer: customerData,
        vehicle: vehicleData
      });
      var body = reservationRequestEmailBodyCopy({
        customer: customerData,
        vehicle: vehicleData
      });
      return sendEmail(flexDriveAdminEmailAddress, fromAddress, subject, body);
    }).then(function() {
      Utils.reportSuccess(reservationRequest, options, promise);
    }, function(error) {
      Utils.reportFailure(error, options, promise);
    });
    return promise;
  },

  sendRentalRenewalRequest: function(user, data, options) {
    var promise = new Parse.Promise();
    var fromAddress = user.getEmail();
    var subject = rentalRenewalRequestEmailSubjectCopy(data.customerData);
    var body = rentalRenewalRequestEmailBodyCopy(data);
    sendEmail(flexDriveAdminEmailAddress, fromAddress, subject, body).then(function() {
      Utils.reportSuccess(null, options, promise);
    }, function(error) {
      Utils.reportFailure(error, options, promise);
    });
    return promise;
  },

  sendTemplateEmail: function(user, emailTemplateName, subject) {
    var userKeys = Data.Users.keys;
    var rentCentricCustomerId = user.get(userKeys.rentCentricCustomerId);

    return getRenderedTemplate(emailTemplateName, rentCentricCustomerId).then(function(renderedEmail) {
      var fromAddress = "Flexdrive Member Services<" + flexDriveAdminEmailAddress +">";
      var toAddress = user.getEmail();
      return sendHtmlEmail(toAddress, fromAddress, subject, renderedEmail);
    }, function(error) {
      console.error('Problem sending email ' + JSON.stringify(error));
      var emailError = emailErrorForError(error);
      return Parse.Promise.error(emailError);
    });
  },

  sendReservationNotification: function(to, user, reservation) {
    var fromAddress = flexDriveITEmail;
    var subject = 'Reservation - ' + reservation.LocationName + ' by ' + user.get('email');
    var body = user.get('email') + ' has reserved vehicle:\nVehicle ID: ' + reservation.VehicleID + '\nAssigned ID: ' + reservation.AssignedID + '\nColor: ' + reservation.ExteriorColor + '\nYear: ' + reservation.YearMade + '\nMake: ' + reservation.MakeName + '\nModel: ' + reservation.ModelName + '\n\nAt ' + reservation.LocationName + '\n\nReservation ID: ' + reservation.ReservationID;
    return sendEmail(to, fromAddress, subject, body);
  },

  sendExcessiveBillingsEmail: function(numberOfBillings) {
    console.log("Sending Excessive Billings Email: " + numberOfBillings);
    var fromAddress = 'noreply@mutualmobile.com';
    var toAddress = "Flexdrive IT<" + MailgunEnvironment.flexDriveITEmail +">";
    var subject = "Excessive Billing Objects (" + MailgunEnvironment.env + ")"
    var body = "The last query for billings found " + numberOfBillings + " billings."

    return sendHtmlEmail(toAddress, fromAddress, subject, body).fail(function(error) {
      console.log("Email Failed");
      var emailError = emailErrorForError(error);
      return Parse.Promise.error(emailError);
    });
  },

  sendBillingReport: function(body) {
    var fromAddress = 'noreply@mutualmobile.com';
    var subject = 'Billing report';
    if (MailgunEnvironment.env) {
      subject += " (" + MailgunEnvironment.env + " env)";
    }
    return sendHtmlEmail(flexDriveBillingEmailAddress, fromAddress, subject, body).then(function() {
      return Parse.Promise.as('email sent');
    }, function(error) {
      console.error('Problem sending email ' + error.message);
      return Parse.Promise.error(error);
    });
  },

  //;;!! working on this
  sendErrorNotification: function(body) {
    var fromAddress = 'noreply@mutualmobile.com';
    var subject = 'Error Notification';
    if (MailgunEnvironment.env) {
      subject += " (" + MailgunEnvironment.env + " env)";
    }
    return sendEmail(flexDriveAdminEmailAddress, fromAddress, subject, body).then(function() {
      return Parse.Promise.as('email sent');
    }, function(error) {
      console.error('Problem sending email ' + error.message);
      return Parse.Promise.error(error);
    });
  },
}

// Private API:

function sendEmail(to, from, subject, body) {
  var email = {
    to: to,
    from: from,
    subject: subject,
    text: body
  };
  return mailgun.sendEmail(email);
};

function sendHtmlEmail(to, from, subject, body) {
  var email = {
    to: to,
    from: from,
    subject: subject,
    html: body
  };
  return mailgun.sendEmail(email);
};

function getRenderedTemplate(templateEmailName, rentCentricCustomerId) {
  var emailTemplate;

  var url = Environment.FlexDriveWebsite.getURL() + templateEmailName;
  return Parse.Cloud.httpRequest({
    url: url
  }).then(function(httpResponse) {
    if (httpResponse.status != 200) {
      console.log('Getting email template failed with response code ' + httpResponse.status);
      return Parse.Promise.error();
    } else {
      emailTemplate = httpResponse.text;
      return RentCentric.Customers.V2.retrieveConglomerateInfo(rentCentricCustomerId);
    }
  }).then(function(conglomerateInfo) {
    var customerRunningInfo = conglomerateInfo.CustomerRunningInfo;
    var customerInfo = customerRunningInfo.CustomerInfo;
    var firstName = customerInfo.FirstName;
    var substitutionValues = {};

    if (firstName) {
      substitutionValues[Data.EmailTemplate.keys.firstName] = firstName;
    }
    substitutionValues[Data.EmailTemplate.keys.adminEmail] = flexDriveAdminEmailAddress;

    var emailBody = renderEmailTemplate(emailTemplate, substitutionValues);
    return Parse.Promise.as(emailBody)
  }, function(error) {
    console.log("Error rendering email template: " + JSON.stringify(error));
    var emailError = emailErrorForError(error);
    return Parse.Promise.error(emailError);
  });
};

function renderEmailTemplate(emailTemplate, substitutionValues) {
  var renderedTemplate = emailTemplate;
  var keys = Data.EmailTemplate.keys;

  for (key in substitutionValues) {
    var value = substitutionValues[key];
    if (key == keys.firstName && value.length == 0) {
      renderedTemplate = renderedTemplate.replace(key, "Flexdrive Member");
    } else if (value.length == 0) {
      renderedTemplate = renderedTemplate.replace(key, "");
    } else {
      renderedTemplate = renderedTemplate.replace(key, value);
    }
  }

  return renderedTemplate
};

function emailErrorForError(error) {
  var emailError = Error.emailFailedError();
  Error.addObjectToError(emailError, error);

    return emailError;
};
