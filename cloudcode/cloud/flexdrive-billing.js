/**
* Billing is all V2 compatible
*/

var Environment = require('./flexdrive-environment.js');
var Error = require('./flexdrive-error.js');
var Mail = require('./flexdrive-email.js');
var Log = require('./flexdrive-log.js');
var Data = require('./flexdrive-data.js');

var RentCentric = require("./rentcentric.js");
var RentCentricEnvironment = Environment.RentCentric.getEnvironment();
RentCentric.initialize(RentCentricEnvironment.url, RentCentricEnvironment.clientID, RentCentricEnvironment.serviceUserName, RentCentricEnvironment.servicePassword);

var FlexdriveStripe = require("./flexdrive-stripe.js");

var Utils = require('./flexdrive-utils.js');
var Mail = require('./flexdrive-email.js');
var _ = require('underscore');

var twelveMinutesMilliseconds = 720000;
var reservationPageSize = 'reservationPageSize';

var skippingReservationText = 'Skipping reservation because ';

module.exports = {
  /**
  * Get the version of the module.
  * @return {String}
  */
  version: '1.0.0',

  run: function(reservations, individual) {
    var billingStats;
    return Billing_Internal.getReservations(reservations).then(function(reservationInfo) {
      var BillingStats = Parse.Object.extend("BillingStats");
      var stats = new BillingStats();
      if (reservationInfo.reservations && reservationInfo.reservations.length > 0 && individual) {
        stats.set('reservationId', reservationInfo.reservations[0].ReservationID);
      }
      stats.set('numProcessed',0);
      stats.set('index',reservationInfo.index);
      stats.set('totalToProcess',reservationInfo.total);
      return stats.save().then(function(statsSaved) {
        billingStats = statsSaved;
        return Parse.Promise.as(reservationInfo);
      });
    }).then(function(reservationInfo) {
      billingStats.set('numToProcess',reservationInfo.reservations.length);
      return billingStats.save().then(function(billStats) {
        billingStats = billStats;
        return Parse.Promise.as(reservationInfo);
      })
    }).then(function(reservationInfo) {
      console.log('' + reservationInfo.reservations.length + ' reservation to process');
      var chainedPromise = Parse.Promise.as();
      _.each(reservationInfo.reservations, function(reservation) {
        chainedPromise = chainedPromise.then(function() {
          // If getting close to 15 minutes of run time then
          // shut it down by not processing any more reservations
          var now = new Date();
          var runTimeMillis = now.getTime() - billingStats.createdAt.getTime();
          if (runTimeMillis > twelveMinutesMilliseconds) {
            runTimeMillis = now.getTime() - billingStats.createdAt.getTime();
            console.log('more than 12 minutes, skipping ');
            return Parse.Promise.as();
          }

          // Lots of error handling here. Want the log the error but
          // continue on to the next reservation.
          try {
            var numProcessed = billingStats.get('numProcessed');
            billingStats.set('numProcessed',++numProcessed);
            var elapsedTime = Utils.elapsedTime(billingStats.createdAt, now);
            var elapsedMinutes = '' + elapsedTime.minutes;
            if (elapsedMinutes.length > 4) {
              elapsedMinutes = elapsedMinutes.substring(0,4);
            }
            billingStats.set('elapsedTime',('' + elapsedMinutes + ' minutes'));
            return billingStats.save().then(function() {
              return Billing_Internal.processReservation(reservation).fail(function(error) {
                // Log it and then keep processing
                return Billing_Internal.logErrorToBilling(reservation.ReservationID, error).always(function() {
                  return Parse.Promise.as();
                });
              });
            }, function(error) {
              // Log it and then keep processing
              return Billing_Internal.logErrorToBilling(reservation.ReservationID, error).always(function() {
                return Parse.Promise.as();
              });
            });
          } catch(err) {
            // Log it and then keep processing
            return Billing_Internal.logErrorToBilling(reservation.ReservationID, err).always(function() {
              return Parse.Promise.as();
            });
          }
        });
      });
      return chainedPromise;
  }).then(function() {
      billingStats.set('endedAt',(new Date()));
      return billingStats.save();
    }, function(error) {
      // bubble up
      return Parse.Promise.error(error);
    });
  },

  get: function(reservationId) {
    var query = new Parse.Query('Billing');
    query.equalTo('reservationId', reservationId);
    return query.first();
  },

  getEstimatedTaxes: function(locationId, amount) {
    return Billing_Internal.calculateTaxes(locationId, amount).then(function(taxes) {
      var taxList = [];
      _.each(taxes, function(taxInfo) {
        taxList.push({taxAmount: taxInfo.amount,
          description: taxInfo.name,
          percentage: taxInfo.percentage});
      });
      return Parse.Promise.as(taxList);
    });
  },
  createBilling: function(reservation, pending, odrNotify, expired) {
    return Billing_Internal.createBilling(reservation, pending, odrNotify, expired);
  },

  getPendingBillings: function() {
    var now = new Date();
    var sevenDaysAgo = new Date(now.getFullYear(), now.getMonth(), (now.getDate() - 7));
    var query = new Parse.Query('Billing');
    query.greaterThanOrEqualTo("createdAt", sevenDaysAgo);
    query.equalTo(Data.Billing.keys.pending, true);
    query.limit(1000);
    return query.find();
  },

};


var Billing_Internal = {

    // If there is reservation(s) passed into the call then return them.
    // Get the last billing run.
    // If there was no last billing run then start at the beginning
    // If there was a last billing then.
    //   If it is a new day then start at the beginning
    //   If the last run still has more to process then begin at the same page as the last run
    //   If the last run completed see of there are more pages to process
    //   If all pages have processed see of there are new reservations
    //   If all pages have processed and no new reservations start over
    getReservations: function(reservations) {
      if (reservations) {
        return Parse.Promise.as({reservations:reservations,total:1,index:1});
      } else {
        var stat;
        var index = 0;
        var chunk;
        var total;
        var logText = '';

        // Check the last billing stats
        var query = new Parse.Query('BillingStats');
        query.descending("createdAt");
        query.doesNotExist("reservationId");
        return query.first().then(function(billingStat) {
          stat = billingStat;
          logText += '\nbillingStat=' + JSON.stringify(billingStat);
          return RentCentric.Reservations.countStartTodayUnPaid();
        }).then(function(reservationCount) {
          total = reservationCount;
          logText += '\nreservation count=' + reservationCount;
          var query = new Parse.Query('Config');
          query.equalTo('key',reservationPageSize);
          return query.first();
        }).then(function(pageSize) {
          chunk = parseInt(pageSize.get('value'));
          logText += '\npageSize=' + chunk;

          if (stat) {
            // Check if it is a new day eastern time zone since the call to get the reservations is for eastern time zone.
            var lastRunDate = stat.createdAt;
            var today = new Date();
            logText += '\nlastRunDate=' + lastRunDate + ' today='+today + ' equal=' + (Utils.isDateMonthYearEqual(lastRunDate, today, Utils.easternTimezone));
            if (Utils.isDateMonthYearEqual(lastRunDate, today, Utils.easternTimezone) == false) {
              logText += '\nNew day';
              // Tis a new day to get the first page
              index = 1;
            } else {
              // Not a new day see if the last page was completely processed
              index = stat.get('index');
              var totalToProcess = stat.get('totalToProcess');
              var numToProcess = stat.get('numToProcess');
              var numProcessed = stat.get('numProcessed');
              logText += '\ntotalToProcess=' + totalToProcess + ' numToProcess='+numToProcess + ' numProcessed='+numProcessed;
              if (isNaN(totalToProcess) || isNaN(index) || isNaN(numToProcess) || isNaN(numProcessed)) {
                logText += '\ntotalToProcess or index or numToProcess or numProcessed not set';
                // Should not get here
                if (!isNaN(totalToProcess)) {
                  total = totalToProcess
                }
                index = 1;
              } else {
                // stats has number in all the fields
                var moreInPage = false;
                var nextPage = false;

                if (numToProcess !== 0 && numProcessed !== 0) {
                  if (numToProcess !== numProcessed) {
                    // Rerun from the same point
                    moreInPage = true;
                    total = totalToProcess;
                    logText += '\nNot equal so continue processing that page';
                  } else if ((index + numToProcess -1) !== totalToProcess) {
                    // Total number of reservations were not processed
                    nextPage = true;
                    index = index + chunk;
                    total = totalToProcess;
                    logText += '\nmore to process so get the next page';
                  }
                }

                if (!moreInPage && !nextPage) {
                  logText += '\nNo more in page and not next page';
                  // All were processed. Now see if the total changed
                  if (total !== totalToProcess) {
                    logText += '\nTotal was ' + totalToProcess + ' but now is ' + total;
                    // The number of reservations has changed so start over
                    logText += '\nNew reservations start over';
                    index = 1;
                  } else {
                    logText += '\nStart over';
                    // Nothing to do so start over
                    index = 1;
                  }
                } else {
                  logText += '\nmoreInPage=' + moreInPage + ', nextPage=' + nextPage;
                }
              }
            }
          } else {
            logText += '\nNo billing stat so start new';
            // Should not get here
            index = 1;
          }

          logText += '\nResult is index='+index + " total="+total;
          // uncomment log statement to debug
          //return Log.info(logText).always(function() {
            if (index === 0) {
              return Parse.Promise.as([]);
            } else {
              return RentCentric.Reservations.listStartTodayUnPaid(index, chunk);
            }
          //});
        }).then(function(reservations) {
          return Parse.Promise.as({reservations:reservations, total:total, index:index});
        });
      }
    },

    isReservationOK: function(reservation) {
      var dateOut = new Date(parseInt(reservation.DateOut));
      var today = new Date();
      var tz = reservation.Timezone;

      if (!reservation.ReservationStatus.Confirmed || reservation.ReservationStatus.Confirmed !== "Yes") {
        return 'reservation status is not confirmed';
      }

      var numberOfYesProps = 0;
      for (var prop in reservation.ReservationStatus) {
        if (reservation.ReservationStatus[prop] === "Yes") {
          ++numberOfYesProps;
        }
      }

      if (numberOfYesProps > 1) {
        return 'reservation has statuses other than confirmed';
      }

      if (reservation.BalanceDue) {
        // Check if the reservation has been paid
        if (reservation.BalanceDue == 0) {
          return 'reservation balance is zero';
        }
      } else {
        return 'reservation balance is not set';
      }

      if (reservation.NetRate) {
        if (reservation.NetRate == 0) {
          return 'reservation net rate is zero';
        }
      } else {
        return 'reservation net rate is not set';
      }

      if (reservation.BalanceDue != reservation.NetRate) {
        return 'reservation balance due is not the same as net rate';
      }

      if (!reservation.ReservationType || reservation.ReservationType !== "Billable") {
        return 'reservation is not billable';
      }

      return 'ok';
    },

    processReservation: function(reservation) {
      console.log('\n-----> processing reservation id=' +  reservation.ReservationID);
      var skipPayment = false;
      var skipProcessing = false;
      var billing;
      var rentalCount;
      var query = Billing_Internal.getBillingQuery(reservation.ReservationID);
      return query.first().then(function(_billing) {
        billing = _billing;
        if (billing) {
          console.log('billing found so no need to create a new one ' + billing.id);

          // Do not try to pay a billing that has already been paid
          if (billing.get('paid')) {
            console.log('Skipping payment');
            skipPayment = true;
            return new Parse.Promise.as(billing);
          }

          // Skip the billing process if the billing has a payment error and the processPaymentError flag is not set
          if (billing.get('paymentError') && reservation.billMe !== true) {
            console.log('Skipping payment');
            skipPayment = true;
            return new Parse.Promise.as(billing);
          }

          return Utils.getConfigValue('processingPeriod').then(function(period) {
            // Avoid double processing
            if (billing.get('startProcessing')) {
              var now = new Date();
              // Check if start processing was more than two minutes ago
              if (now.getTime() - billing.get('startProcessing') < parseInt(period)) {
                console.log('Skipping processing');
                skipProcessing = true;
                reservation['skipProcessing'] = true;
                return new Parse.Promise.as(billing);
              }
            }

            // If a billing with a previous paymentError is being reprocessed, push and email
            // notifications should be resent.
            if (billing.get('paymentError')) {
              billing.set('resendNotifications', true);
            }
            billing.set('paymentError', null);
            billing.set('lastError', null);
            billing.set('balanceDue',reservation.BalanceDue);
            billing.set('netRate',reservation.NetRate);
            billing.set('resStart',new Date(parseInt(reservation.DateOut)));
            billing.set('reservation', JSON.stringify(reservation));
            billing.set('startProcessing', new Date());
            return billing.save();
          });
        } else {
          console.log('new billing ' + reservation.CustomerID);
          return Billing_Internal.createBilling(reservation, false, false, false).then(function(billing) {
            billing.set('startProcessing', new Date());
            return billing.save();
          });
        }
      }).then(function(_billing) {
        billing = _billing;
        if (skipPayment || skipProcessing) {
          return Parse.Promise.as(billing);
        }

        // Check if the reservation is elegible to be paid
        var result = Billing_Internal.isReservationOK(reservation);
        if (result == "ok") {
          return Parse.Promise.as();
        } else {
          // Record why the reservation has been skipped
          return Parse.Promise.error(skippingReservationText + result);
        }
      }).then(function() {
        if (skipPayment || skipProcessing) {
          return Parse.Promise.as();
        }

        return RentCentric.Rentals.V2.count(billing.get('customerIDRentCentric'));
      }).then(function(_rentalCount) {
        if (skipPayment || skipProcessing) {
          return Parse.Promise.as();
        }

        rentalCount = _rentalCount;
        var query = new Parse.Query('Markets');
        query.equalTo('MarketID', billing.get('locationId'));
        return query.first();
      }).then(function(market) {
        if (skipPayment || skipProcessing) {
          return Parse.Promise.as(billing);
        }

        if (billing.get('fees')) {
          // Use the original fees that were set when the billing was created
          return Parse.Promise.as(billing)
        } else {
          // New billing so get the fees
          var fees = [];
          if (rentalCount == 0) {
            fees = Data.Markets.getMembershipFees(market);
          }
          // Charge the deposit if not a renewal
          if (!reservation.RentalID || reservation.RentalID == 0) {
            var moreFees = Data.Markets.getDeposits(market,reservation.VehicleType);
            for (var i = 0; i < moreFees.length; ++i) {
              fees.push(moreFees[i]);
            }
          }
          // Charge the late fee if expired
          if (billing.get('expired')) {
            var moreFees = Data.Markets.getLateFees(market);
            for (var i = 0; i < moreFees.length; ++i) {
              fees.push(moreFees[i]);
            }
          }
          billing.set('fees',fees);
          return billing.save();
        }
      }).then(function(_billing) {
        billing = _billing;
        if (skipPayment || skipProcessing) {
          return Parse.Promise.as(billing);
        }

        var amount = 0;
        if (billing.get('netRate')) {
          amount = parseInt(Utils.convertToPennies(billing.get('netRate')));
        } else {
          amount = parseInt(Utils.convertToPennies(billing.get('balanceDue')));
        }

        if (billing.get('fees')) {
          for (var i = 0; i < billing.get('fees').length; ++i) {
            if (billing.get('fees')[i].type != "Deposit") {
              amount += parseInt(Utils.convertToPennies(billing.get('fees')[i].amount));
            }
          }
        }
        amount = Utils.convertToTwoDecimalNumber(amount);

        var locationId = billing.get('locationId');
        return Billing_Internal.calculateTaxes(locationId, amount).then(function(taxes) {
          billing.set('taxes',JSON.stringify(taxes));
          return billing.save();
        });
      }).then(function(_billing) {
        billing = _billing;
        if (skipPayment || skipProcessing) {
          return Parse.Promise.as(billing);
        }

        // Try to pay it
        return FlexdriveStripe.charge(billing);
      }).then(function(billing) {
        return finishProcessingReservation(reservation.ReservationID, skipProcessing, null)
      }, function(error) {
        return finishProcessingReservation(reservation.ReservationID, skipProcessing, error)
      });
    },

    getTaxInfo: function(locationId) {
      var query = new Parse.Query('Markets');
      query.equalTo('MarketID', locationId);
      return query.first().then(function(market) {
        return Parse.Promise.as(market.get('taxes'));
      });
    },

    // returns an array of taxes
    calculateTaxes: function(locationId, amount) {
      var taxList = [];
      return Billing_Internal.getTaxInfo(locationId).then(function(taxes) {
        var chainedPromise = Parse.Promise.as();
        _.each(taxes, function(taxInfo) {
          chainedPromise = chainedPromise.then(function() {
            if (('' + locationId) == ('' + taxInfo.LocationID)) {
              var taxPercentage = parseFloat(taxInfo.TaxRate);
              taxPercentage = taxPercentage / 100;
              var calculatedTax = amount * taxPercentage;
              var calculatedTaxRounded = calculatedTax.toFixed(2);

              newTaxInfo = new Object();
              newTaxInfo.code = taxInfo.TaxCode;
              newTaxInfo.name = taxInfo.TaxName;
              newTaxInfo.percentage = taxInfo.TaxRate;
              newTaxInfo.amount = calculatedTaxRounded;
              taxList.push(newTaxInfo);
            }
            return Parse.Promise.as();
          });
        });
        return chainedPromise;
      }).then(function() {
        return Parse.Promise.as(taxList);
      });
    },

    getBillingQuery: function(reservationId) {
      var query = new Parse.Query('Billing');
      query.equalTo("reservationId", reservationId);
      return query;
    },

    logErrorToBilling: function(reservationId, error) {
      var logError;
      var query = Billing_Internal.getBillingQuery(reservationId);
      return query.first().then(function(billing) {
        if (billing) {
          var errorInformation;
          if (error.message) {
            errorInformation = error.message;
          } else {
            errorInformation = error.toString();
          }

          billing.set('lastError', errorInformation);
          logError = Billing_Internal.billingErrorForError(errorInformation);
          return billing.save();
        } else {
          logError = Error.billingSystemError();
          Error.addObjectToError(logError, {"billing" : "No billing for Reservation Id: " + reservationId + " billing Id: " + billing.id});
          return Parse.Promise.as();
        }
      }).then(function() {
        if (logError) {
          if (logError.internalMessages && logError.internalMessages.length > 0 && (typeof logError.internalMessages[0] === 'string') && Billing_Internal.doNotLogToLog(logError.internalMessages[0]) ) {
            return Parse.Promise.as();
          } else {
            console.log("Billing Error: " + JSON.stringify(logError));
            return Log.logFlexDriveError(logError, null, null, null, null, null, null, null);
          }
        } else {
          return Parse.Promise.as();
        }
      });
    },

    processReport: function(billing, reports, bgcolor) {
      var paidDate = billing.get('paid');
      var locationId = billing.get('locationId');
      var tz = JSON.parse(billing.get('timezone'));
      var paidDateAsText = Utils.getDateTimezoneAsText(paidDate, tz);

      reports.push('<tr bgcolor="' + bgcolor + '">' +
      '<td align="center">' + billing.get('reservationId')  + '</td>' +
      '<td align="center">' + paidDateAsText + '</td>' +
      '<td align="center">' + (billing.get('reservationUpdated') ? billing.get('reservationUpdated') : '') + '</td>' +
      '<td>' + (billing.get('lastError') ? billing.get('lastError') : '') + '</td>' +
      '<td align="center">' + (billing.get('locationId') ? billing.get('locationId') : '') + '</td>' +
      '<td align="center">' + (billing.get('customerIDRentCentric') ? billing.get('customerIDRentCentric') : '') + '</td>' +
      '<td align="center">' + (billing.get('customerIDStripe') ? billing.get('customerIDStripe') : '') + '</td>' +
      '</tr>');
    },

    billingErrorForError: function(error) {
      var billingError = Error.billingSystemError();
      Error.addObjectToError(billingError, error);
      return billingError;
    },

    // If this is a skipping reservation error or a stripe warning then
    // return true
    doNotLogToLog: function(text) {
      if (text.indexOf(skippingReservationText) === 0 || text.indexOf('Your card was declined') === 0 || text.indexOf('Your card number is incorrect') === 0 || text.indexOf('Your card has expired') === 0) {
        return true;
      } else {
        return false;
      }
    },

    createBilling: function(reservation, pending, odrNotify, expired) {
      // Begin persisting the billing information
      var Billing = Parse.Object.extend("Billing");
      var billing = new Billing();
      billing.set('balanceDue',reservation.BalanceDue);
      billing.set('netRate',reservation.NetRate);
      billing.set('customerIDRentCentric', reservation.CustomerID);
      billing.set('reservationId',reservation.ReservationID);
      billing.set('locationId',reservation.LocationID);
      billing.set('timezone',JSON.stringify(reservation.Timezone));
      billing.set('resStart',new Date(parseInt(reservation.DateOut)));
      billing.set('reservation', JSON.stringify(reservation));
      if (odrNotify) {
        billing.set('odrNotify', true);
      } else {
        billing.set('odrNotify', false);
      }
      if (expired) {
        billing.set('expired', true);
      } else {
        billing.set('expired', false);
      }
      if (pending) {
        billing.set(Data.Billing.keys.pending, true);
      }

      return billing.save().then(function(bill) {
        billing = bill;
        var userQuery = new Parse.Query(Parse.User);
        userQuery.equalTo("customerIDRentCentric",billing.get("customerIDRentCentric"));
        return userQuery.first();
      }).then(function(user) {
        if (user) {
          billing.set('customerIDStripe',user.get("customerIDStripe"));
          billing.set('email',user.get("email"));
          return billing.save();
        } else {
          // Should never get here
          return Parse.Promise.error("Could not find rent centric user with ID=" + billing.get("customerIDRentCentric"));
        }
      }, function(error) {
        console.error(error.message);
        var billingError = Billing_Internal.billingErrorForError(error);
        return Parse.Promise.error(billingError);
      });
    }
  }

function finishProcessingReservation(reservationId, skipProcessing, error) {
  var query = new Parse.Query('Billing');
  query.equalTo('reservationId', reservationId);
  return query.first().then(function(billing) {
    return updateRentCentricReservation(billing).always(function() {
      if (skipProcessing) {
        if (error) {
          return Parse.Promise.error(error)
        } else {
          return Parse.Promise.as();
        }
      } else {
        billing.set('startProcessing', null);
        return billing.save().always(function() {
          if (error) {
            return Parse.Promise.error(error)
          } else {
            return Parse.Promise.as();
          }
        });
      }
    })
  });
}

// Never returns an error
function updateRentCentricReservation(billing) {
  var updateRCPaid = !billing.get('reservationUpdated') && billing.get('paid');

  //;;!! once RC accepts the json this can be uncommented
  var updateRCPaymentError = false;
  //var updateRCPaymentError = !billing.get('reservationPaymentErrorUpdated') && billing.get('paymentError')
  if (updateRCPaid || updateRCPaymentError) {
    return RentCentric.Reservations.updatePaid(billing).then(function() {
      if (updateRCPaid) {
        billing.set('reservationPaymentErrorUpdated', false);
        billing.set('reservationUpdated', true);
        return billing.save();
      } else {
        billing.set('reservationPaymentErrorUpdated', true);
        return billing.save();
      }
    }, function(error) {
      return Log.warn(JSON.stringify(error))
    });
  } else {
    return Parse.Promise.as();
  }
}
