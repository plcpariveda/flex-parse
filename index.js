require('dotenv').config();
var express = require('express');
var ParseServer = require('parse-server').ParseServer;
var path = require('path');
var FlexdrivePush = require('./flexdrive-push.js');

var databaseUri = process.env.DATABASE_URI;

if (!databaseUri) {
  console.log('DATABASE_URI not specified, falling back to localhost.');
}

//Create the app before our ParseServer and expose it via exports
var app = express();
module.exports = app;

var api = new ParseServer({
  databaseURI: databaseUri || 'mongodb://localhost:27017/dev',
  cloud: process.env.CLOUD_CODE_MAIN || __dirname + '/cloudcode/cloud/main.js',
  appId: process.env.APP_ID || 'myAppId',
  masterKey: process.env.MASTER_KEY || '', //Add your master key here. Keep it secret!
  serverURL: process.env.SERVER_URL || 'http://localhost:1337/parse',  // Don't forget to change to https if needed
  push: FlexdrivePush.getPushConfig()
});

// Serve the Parse API on the /parse URL prefix
var mountPath = process.env.PARSE_MOUNT || '/parse';
app.use(mountPath, api);

var staticPath = __dirname + '/cloudcode/public';
console.log("Parse static assets at", staticPath);
app.use(express.static(staticPath));

app.get('/ping', function(req, res) {
  res.status(200).send('Parse Server is running');
});

var port = process.env.PORT || 1337;
var httpServer = require('http').createServer(app);
httpServer.listen(port, function() {
    console.log('flex parse-server running on port ' + port + '.');
});

// This will enable the Live Query real-time server
ParseServer.createLiveQueryServer(httpServer);
